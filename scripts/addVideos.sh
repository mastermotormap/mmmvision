#!/bin/bash
# Script to add Video
#$1 = path to folder with xml    
#$2 = subject id
#$3 = delta timestep				
#subjectdir="/home/krebs/mounts/share/Vicon_Data/Vicon/OML/Bimanual_Manipulation_Dataset_Motion_Primitives/1723"
#subjectdir="/home/krebs/Desktop/Testdata"
#bash addVideos.sh $subjectdir/conversion_data/segmented
#./VideoImporter --inputMotion /home/krebs/Desktop/Testdata/segemented/Roll1_dough_start_05.xml --dataFile /home/krebs/Desktop/Testdata/gopro/Roll1_dough_start_0005_gopro.MP4 --motionName 1723 --outputMotion /home/krebs/Desktop/Testdata/segemented/Roll1_dough_start_05_new.xml
 


addVideo()
{
	echo "in add video"
	filename=$1
	motionname=$2
	delta=0.32#$3
	purename=$(basename "${filename%.*}")
	videodata="$(dirname "$filename")/../../GoPro/"$purename"_gopro.MP4"
	#videodata="$(dirname "$subjectdir")/gopro/"$purename"_gopro.MP4"
	outputname="$(dirname "$filename")/../TestAddedGoPro/"$purename".xml"
	#outputname="$(dirname "$filename")/"$purename"_withgopro.xml"

	../build/bin/VideoImporter \
				--inputMotion $filename     \
				--motionName $motionname     \
				--dataFile $videodata      \   
				--outputMotion $outputname                \
				--timestepDelta $delta			
}
export -f addVideo

if ! [ -d "$1" ]; then
        echo "[ERROR] in required argument 1: $1 is no valid directory!"
	exit
fi

arg1="$1"
arg2="$2"
if [ -z "$2"]; then
	echo "[WARNING] Subject id was not set!"
        arg2="1723"
fi
arg3="$3"
if [ -z "$3"]; then
	echo "[WARNING] Timestep delta was not set!"
        arg3=0
fi

#bash -c "addIMU \"$arg1\" \"$arg2\" \"$arg3\""
echo "here"
find $1 -name "*.xml" -type f | xargs -I% -P8 bash -c "addVideo \"%\" \"$arg2\" \"$arg3\""
exit

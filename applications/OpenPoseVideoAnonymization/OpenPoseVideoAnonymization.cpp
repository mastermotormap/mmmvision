
#include <MMMVision/Tools/Video.h>
#include <MMMVision/Tools/VideoTools.h>
#include <MMMVision/Tools/OpenPose/OpenPoseTools.h>
#include <MMMVision/Tools/Errors.h>
#include <regex>

std::vector<std::filesystem::path> getVideosFromDirectory(const std::filesystem::path &directoryPath, bool recursive, const std::string &regex) {
    if (!std::filesystem::exists(directoryPath))
        throw MMMVision::MMMVisionError("");

    std::regex r = std::regex(regex);
    std::vector<std::filesystem::path> paths;
    if (recursive) {
        for(auto &file : std::filesystem::recursive_directory_iterator(directoryPath))
            if (std::regex_search(file.path().c_str(), r))
                paths.push_back(file.path());
    }
    else {
        for(auto &file : std::filesystem::directory_iterator(directoryPath))
            if (std::regex_search(file.path().c_str(), r))
                paths.push_back(file.path());
    }
    return paths;
}

int main(int argc, const char* argv[])
{
    if (argc < 2) {
        std::cout << "No filename as command line parameter!" << std::endl;
        return -1;
    }

    MMMVision::OpenPoseWrapper w;

    int netHeight = 160;
    int netWidth = -1;
    int padding = 150;
    int gaussKernel = 351;
    std::cout << "Using net size of " << netWidth << "x" << netHeight << std::endl;
    op::Point<int> netSize(netWidth, netHeight);
    op::Wrapper opWrapper{op::ThreadManagerMode::Asynchronous};
    w.configure(opWrapper, netSize);

    bool override = false;
    bool recursive = true;
    std::string suffix = "_anonymized";
    std::string ignoreParentFolder = "GoPro";

    std::filesystem::path searchPath = argv[1];
    std::vector<std::filesystem::path> paths;
    if (std::filesystem::is_directory(searchPath)) {
        paths = getVideosFromDirectory(argv[1], recursive, "^((?!_anonymized).)*\\.(MP4|mp4)$");
        std::cout << "Found the following video files to anonymize" << std::endl;
        for (unsigned int i = 0; i < paths.size(); i++)
            std::cout << i << " " << paths[i] << std::endl;
    }
    else paths.push_back(argv[1]);

    for (const auto &path : paths) {
        auto filePath = MMMVision::Video::getFilePathSuffix(path, suffix);
        if ((!ignoreParentFolder.empty() && path.parent_path().stem() == ignoreParentFolder) || (!override && std::filesystem::exists(filePath))) {
            std::cout << "Ignoring " << path << std::endl;
            continue;
        }
        try {
            MMMVision::VideoPtr video(new MMMVision::Video(path));

            cv::Rect2i upperRegion(0,0,video->getWidth(), video->getHeight() / 2); // only do it in the upper region

            std::cout << "Anonymizing file " << video->getFilePath() << "..." << std::endl;

            auto rois = w.createBoundingBoxes(opWrapper, video->getFrames());
            auto updatedRois = MMMVision::Tools::groupAndExtendBoundingBoxes(rois, video->getTimesteps());

            for (const auto &roisByTime : updatedRois) {
                for (const auto &roi : roisByTime.second) {
                    video->anonymize(roi.first, roi.second, padding, gaussKernel, upperRegion);
                }
            }
            video->write(filePath);
        }
        catch (MMMVision::MMMVisionError &e) {
            std::cout << "Error when trying to anonymize file " << path << ": " << e.what() << std::endl;
        }
    }

    return 0;
}



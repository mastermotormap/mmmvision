/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Franziska Krebs
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Exceptions.h>
#include <MMM/Motion/MotionReaderC3D.h>
#include <MMM/Model/LoadModelStrategy.h>
//#include <MMMVision/Sensor/Plugin/VideoCameraSensor/VideoCameraSensor.h>
#include <MMMVision/Sensor/Plugin/AzureKinectDKSensor/AzureKinectDKSensor.h>
#include <MMMVision/Sensor/Plugin/AzureKinectDKSensor/AzureKinectDKSensorMeasurement.h>
#include <MMM/Viewer/MotionHandler.h>
#include "MMMAzureKinectImporterConfiguration.h"
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Model/ModelReaderXML.h>
#include <VirtualRobot/Robot.h>
//#include "VideoImporter.h"

#include <string>
#include <vector>
#include <tuple>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMAzureKinectImporter --- " << std::endl;
    // example: ./AzureKinectImporter --inputMotion "/.../kinect/Peel1_cucumber_cb_04.xml" --outputMotion "/.../kinect/Peel1_cucumber_cb_04_kinectfront.xml" --dataFilePath "/..../kinect/kinectfront_Peel1_cucumber_cb_04.mkv" --motionName "kinect_azure_front" --timestepDelta 0
    MMMAzureKinectImporterConfiguration* configuration = new MMMAzureKinectImporterConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    try {
        MMM_INFO << "Loading output model file" << std::endl;

        MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(true, false, configuration->sensorPluginPaths));
        MMM_INFO << "Reading motion file '" << configuration->inputMotionPath << "'!" << std::endl;
        MMM::MotionRecordingPtr motions = motionReader->loadMotionRecording(configuration->inputMotionPath);
        MMM::MotionPtr motion = motions->getMotion(configuration->motionName);

        std::cout << "Adding azure kinect dk recording from " << configuration->dataFilePath << std::endl;
        MMM::AzureKinectDKSensorPtr sensor = std::make_shared<MMM::AzureKinectDKSensor>(configuration->dataFilePath);
        auto model = motion->getModel();
        if (model) {
            auto rgbSensor = model->getSensor("rgb");
            if (rgbSensor)
                sensor->setTransformation(rgbSensor->getParentNodeToSensorTransformation());
        }
        motion->addSensor(sensor, configuration->timestepDelta, motion->getModel() != nullptr);
        MMM_INFO << "Writing motions to " << configuration->outputMotionPath << std::endl;
        motions->saveXML(configuration->outputMotionPath);
    }
    catch (MMM::Exception::MMMException &e) {
        std::string error_message = std::string("Could not add AzureKinect sensor! ") + e.what();
        MMM_ERROR << error_message << std::endl;
    }


}

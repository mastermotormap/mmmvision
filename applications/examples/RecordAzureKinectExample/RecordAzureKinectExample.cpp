
#include <iostream>
#include <opencv2/opencv.hpp>
#include <unistd.h>
#include <chrono>
#include <thread>

#include <SensorLibraries/AzureKinectDKWrapper/AzureKinect.h>

int main(int argc, const char* argv[])
{
    try {
        MMMVision::AzureKinectPtr azureKinect(new MMMVision::AzureKinect(MMMVision::AzureKinectConfig::DEFAULT_RECORDING()));

        azureKinect->startCamera();

        // Wait to be initialized
        std::this_thread::sleep_for(std::chrono::seconds(1));

        /*std::vector<int> compression_params;
        compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
        compression_params.push_back(9);*/

        cv::Mat colorImage;
        cv::Mat depthImage;
        std::cout << "Capturing image" << std::endl;
        if (!azureKinect->getImages(colorImage, depthImage, true, true)) {
            return -1;
        }
        std::cout << "Writing color image" << std::endl;
        cv::imwrite("color.png", colorImage);
        cv::waitKey();
        std::cout << "Writing depth image" << std::endl;
        cv::imwrite("depth.png", depthImage);
        cv::waitKey();

        azureKinect->close();
    }
    catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        return -1;
    }
    catch (...) {
        std::cout << "Unknown error" << std::endl;
        return -1;
    }

    return 0;
}



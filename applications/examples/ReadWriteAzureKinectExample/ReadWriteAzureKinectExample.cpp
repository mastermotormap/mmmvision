#include <iostream>
#include <opencv2/opencv.hpp>
#include <unistd.h>
#include <chrono>
#include <thread>

#include <SensorLibraries/AzureKinectDKWrapper/AzureKinectPlayback.h>

int main(int argc, const char* argv[])
{
    if (argc < 2) {
        std::cout << "No filename as command line parameter!" << std::endl;
        return -1;
    }
    std::cout << "filename: " << argv[1] << std::endl;
    MMMVision::AzureKinectPlaybackPtr azureKinectRecording(new MMMVision::AzureKinectPlayback(argv[1], 0));

    int timestep = 0;
    std::this_thread::sleep_for(std::chrono::seconds(1));

    cv::Mat colorImage;
    cv::Mat depthImage;
    cv::Mat irImage;

    std::cout << "Start timestep   " << azureKinectRecording->getStartTimestep() << "sec" << std::endl;
    std::cout << "End timestep     " << azureKinectRecording->getEndTimestep() << "sec" << std::endl;
    std::cout << "Recording length " << azureKinectRecording->getRecordingLength() << "sec" << std::endl;
    std::vector<float> timesteps = azureKinectRecording->getTimesteps();
    std::cout << "Timesteps: " << std::endl;
    int i = 0;
    for (float timestep : timesteps) {
        std::cout << timestep << "\t";
        if (i++ == 5) {
            std::cout << std::endl;
        }
    }

    while(false){
        try {
            std::cout << "Capturing image" << std::endl;
            if (!azureKinectRecording->getImages(colorImage, depthImage, irImage, true, true)) {
                return -1;
            }

            std::cout << "Got image" << std::endl;

            std::cout << "Writing color image" << std::endl;
            cv::imwrite("color" + std::to_string(timestep) + ".png", colorImage);

            std::cout << "Writing depth image" << std::endl;
            cv::imwrite("depth" + std::to_string(timestep) + ".png", depthImage);

            std::cout << "Writing ir image" << std::endl;
            cv::imwrite("ir" + std::to_string(timestep) + ".png", irImage);
            std::cout << "dataExport" << std::endl;
            azureKinectRecording->exportMKVFile(colorImage, depthImage, irImage, "test");

            timestep++;
        }
        catch (std::runtime_error &e) {
            std::cout << e.what() << std::endl;
            return -1;
        }
        catch (...) {
            std::cout << "Unknown error" << std::endl;
            return -1;
        }
    }
    return 0;
}




#include <MMM/MMMCore.h>
#include <MMM/Motion/XMLTools.h>
#include <MMMVision/Tools/Video.h>
#include <MMMVision/Tools/VideoTools.h>
#include <MMMVision/Tools/OpenPose/OpenPoseTools.h>
#include <MMMVision/Tools/Errors.h>
#include <regex>
#include "FaceAnonymizationConfiguration.h"
#include <chrono>

std::vector<std::filesystem::path> getVideosFromDirectory(const std::filesystem::path &directoryPath, bool recursive = true, const std::string &regex = std::string()) {
    if (!std::filesystem::exists(directoryPath))
        throw MMMVision::MMMVisionError("");

    std::regex r = std::regex(regex);
    std::vector<std::filesystem::path> paths;
    if (recursive) {
        for(auto &file : std::filesystem::recursive_directory_iterator(directoryPath))
            if (!regex.empty() || std::regex_search(file.path().c_str(), r))
                paths.push_back(file.path());
    }
    else {
        for(auto &file : std::filesystem::directory_iterator(directoryPath))
            if (!regex.empty() || std::regex_search(file.path().c_str(), r))
                paths.push_back(file.path());
    }
    return paths;
}

cv::Rect2f getROI(const std::vector<cv::Point2f> &points, float height, float width, float paddingL = 35, float paddingR = 35, float paddingB = 100, float paddingT = 50) {
    float l = 100000.0f;
    float t = 100000.0f;
    float r = 0;
    float b = 0;

    for (const cv::Point2f &p : points) {
        if (p.x < l) l = p.x;
        if (p.x > r) r = p.x;
        if (p.y < t) t = p.y;
        if (p.y > b) b = p.y;
    }
    return cv::Rect2f(cv::Point2f(std::max(0.0f,l - paddingL), std::max(0.0f,t - paddingT)),
                      cv::Point2f(std::min(width, r + paddingR), std::min(height, b + paddingB)));
}


int main(int argc, char *argv[])
{
    MMM_INFO << " --- FaceAnonymization --- " << std::endl;
    FaceAnonymizationConfiguration* configuration = new FaceAnonymizationConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    int paddingX = configuration->paddingX;
    int paddingY = configuration->paddingY;
    int gaussKernel = configuration->gaussKernel;
    int rotateCode = configuration->rotateCode;
    float threshold = 0.3f;

    MMMVision::OpenPoseWrapper w;

    op::Point<int> netSize(configuration->netWidth, configuration->netHeight);
    op::Wrapper opWrapper{op::ThreadManagerMode::Asynchronous};
    w.configure(opWrapper, netSize);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    opWrapper.start();

    std::vector<std::pair<std::string, std::string>> VideoRecordingPaths;
    if (!configuration->inputVideoPath.empty()) {
        if (!configuration->overwrite && MMM::xml::isValid(configuration->outputVideoPath))
            MMM_INFO << configuration->outputVideoPath << " already exists!" << std::endl;
        else VideoRecordingPaths.push_back(std::make_pair(configuration->inputVideoPath, configuration->outputVideoPath));
    }
    if (!configuration->inputVideoDir.empty()) {
        std::filesystem::create_directory(configuration->outputVideoDir);
        for (auto inputVideoPath : getVideosFromDirectory(configuration->inputVideoDir, false)) {
            std::filesystem::path outputVideoPath = configuration->outputVideoDir / inputVideoPath.filename();
            if (!configuration->overwrite && MMM::xml::isValid(outputVideoPath))
                MMM_INFO << outputVideoPath << " already exists!" << std::endl;
            else VideoRecordingPaths.push_back(std::make_pair(inputVideoPath, outputVideoPath));
        }
    }

    for (const auto &VideoRecordingPath : VideoRecordingPaths) {
        std::cout << "Anonymizing file " << VideoRecordingPath.first << "..." << std::endl;
        cv::VideoCapture cap(cv::String(VideoRecordingPath.first.c_str()));
        if(!cap.isOpened()){
            std::cout << "Error opening video file" << std::endl;
            continue;
        }

        int fps = cap.get(cv::CAP_PROP_FPS) + 0.1f;
        auto codec = cap.get(cv::CAP_PROP_FOURCC);
        auto size = cv::Size(cap.get(cv::CAP_PROP_FRAME_WIDTH), cap.get(cv::CAP_PROP_FRAME_HEIGHT));
        if (rotateCode == cv::ROTATE_90_CLOCKWISE || rotateCode == cv::ROTATE_90_COUNTERCLOCKWISE) {
            int height = size.height;
            size.height = size.width;
            size.width = height;
        }
        auto startTimestep = cap.get(cv::CAP_PROP_POS_MSEC) / 1000.0f;
        auto frameCount = cap.get(cv::CAP_PROP_FRAME_COUNT);
        cv::Rect2i region(0,0,size.width,size.height);

        cv::VideoWriter writer(cv::String(VideoRecordingPath.second.c_str()), codec, fps, size);

        if (!writer.isOpened()) {
            std::cout << "Error while opening!" << std::endl;
            continue;
        }

        int failed = 0;
        int counter = 0;

        std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
        while(true) {
            auto timestep = cap.get(cv::CAP_PROP_POS_MSEC) / 1000.0f - startTimestep;

            cv::Mat frame;
            if(!cap.read(frame)) {
                failed++;
            }
            if (!frame.empty()) {
                failed = 0;
            }

            if (failed > 5)
                break;

            if (rotateCode >= 0)
                cv::rotate(frame, frame, rotateCode);

            op::Matrix imageToProcess = OP_CV2OPCONSTMAT(frame);

            auto datumsPtr = opWrapper.emplaceAndPop(imageToProcess);
            if (datumsPtr != nullptr && !datumsPtr->empty()) {
                for (auto s : *datumsPtr) {
                    const auto keypoints = s->poseKeypoints;
                    for (auto person = 0; person < keypoints.getSize(0); person++) {
                        std::vector<cv::Point2f> points;
                        for (auto bodyPart = 0; bodyPart < keypoints.getSize(1); bodyPart++) {
                            auto keyPoint_x = keypoints[{person, bodyPart, 0}];
                            auto keyPoint_y = keypoints[{person, bodyPart, 1}];
                            auto keyPoint_score = keypoints[{person, bodyPart, 2}];
                            if (keyPoint_score < threshold) continue;
                            if (bodyPart == 0 || bodyPart == 15 || bodyPart == 16 || bodyPart == 17 || bodyPart == 18) {
                                points.push_back(cv::Point2f(keyPoint_x, keyPoint_y));
                            }
                        }
                        if (points.size() > 1) {
                            auto faceROI = getROI(points, size.height, size.width, paddingX,paddingX,paddingY,paddingY);
                            MMMVision::Tools::anonymize(frame, faceROI, 0, gaussKernel, region, false);
                        }

                    }
                }
            }

            writer.write(frame);

            if (configuration->log && counter % fps == 0) {
                std::chrono::steady_clock::time_point current = std::chrono::steady_clock::now();
                float differenceS = std::chrono::duration_cast<std::chrono::seconds>(current - start).count();
                float estimatedS = (differenceS / counter * frameCount) - differenceS;

                std::cout << VideoRecordingPath.first << " anonymized " << counter << "/" << frameCount << " at timestep " <<
                             ((int)timestep / 60) << ":" << ((int)timestep % 60) << ". Estimated time " <<
                             ((int)estimatedS / 60) << ":" << ((int)estimatedS % 60) <<std::endl;
            }

            counter++;
        }

        cap.release();
        writer.release();
    }

    opWrapper.stop();


    return 0;
}



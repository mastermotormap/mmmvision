set(TARGET_NAME "FaceAnonymization")

if(NOT(OpenCV_FOUND AND OpenPose_DIR))
    message(STATUS "Required libraries not found for ${TARGET_NAME}")
    return()
endif()

set(LIBRARIES
    ${OpenCV_LIBS}
    MMMCore
    MMMVisionTools
    MMMVisionOpenPoseTools
    stdc++fs
    MMMViewerLib
    SimoxUtility
)

set(SOURCES
    FaceAnonymization.cpp
)

set(HEADERS
    FaceAnonymizationConfiguration.h
)

add_executable(${TARGET_NAME} ${SOURCES} ${HEADERS})
target_link_libraries(${TARGET_NAME} PRIVATE ${LIBRARIES})


/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Franziska Krebs
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <filesystem>
#include <string>
#include <SimoxUtility/algorithm/string.h>

#include <VirtualRobot/RuntimeEnvironment.h>
#include "../common/ApplicationBaseConfiguration.h"

class FaceAnonymizationConfiguration : public ApplicationBaseConfiguration
{

public:
    std::filesystem::path inputVideoPath;
    std::filesystem::path inputVideoDir;
    std::filesystem::path outputVideoPath;
    std::filesystem::path outputVideoDir;
    bool overwrite;
    int netWidth;
    int netHeight;
    int paddingX = 25;
    int paddingY = 50;
    int gaussKernel = 351;
    int rotateCode = -1;
    bool log;

    FaceAnonymizationConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputVideo", "Video file");
        VirtualRobot::RuntimeEnvironment::considerKey("inputVideoDir", "Directory of video files");
        VirtualRobot::RuntimeEnvironment::considerKey("outputVideo", "Output video file path: Default (if not overwrite): {INPUTVIDEO}_anonymized.*");
        VirtualRobot::RuntimeEnvironment::considerKey("outputVideoDir", "Output video directory: Default (if not overwrite): {INPUTVIDEO}/anonymized/");
        VirtualRobot::RuntimeEnvironment::considerFlag("overwrite", "Flag to overwrite Video files.");
        VirtualRobot::RuntimeEnvironment::considerKey("netWidth", "Network width for OpenPose. Multiple of 16. Default: -1");
        VirtualRobot::RuntimeEnvironment::considerKey("netHeight", "Network height for OpenPose. Multiple of 16. Default: 160");
        VirtualRobot::RuntimeEnvironment::considerKey("paddingX");
        VirtualRobot::RuntimeEnvironment::considerKey("paddingY");
        VirtualRobot::RuntimeEnvironment::considerKey("gaussKernel");
        VirtualRobot::RuntimeEnvironment::considerKey("rotateCode", "e.g. 0=RotateClockWise90°");
        VirtualRobot::RuntimeEnvironment::considerFlag("log");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputVideoPath = getParameter("inputVideo", false, true);
        inputVideoDir = getParameter("inputVideoDir", false, false);

        if ((inputVideoDir.empty() && inputVideoPath.empty()) || (!inputVideoDir.empty() && !inputVideoPath.empty())) {
            valid = false;
        }

        overwrite = VirtualRobot::RuntimeEnvironment::hasFlag("overwrite");

        outputVideoPath = getParameter("outputVideo", false, false);
        if (!inputVideoPath.empty() && outputVideoPath.empty()) {
            if (overwrite) outputVideoPath = inputVideoPath;
            else outputVideoPath = inputVideoPath.stem().generic_string() + "_anonymized.mp4";
        }
        outputVideoDir = getParameter("outputVideoDir", false, false);
        if (!inputVideoDir.empty() && outputVideoDir.empty()) {
            if (overwrite) outputVideoDir = inputVideoDir;
            else outputVideoDir = inputVideoDir / std::filesystem::path("anonymized");
        }

        if (VirtualRobot::RuntimeEnvironment::hasValue("netWidth")) {
            netWidth = simox::alg::to_<int>(getParameter("netWidth"));
        }
        else netWidth = -1;

        if (VirtualRobot::RuntimeEnvironment::hasValue("netHeight")) {
            netHeight = simox::alg::to_<int>(getParameter("netHeight"));
        }
        else netHeight = 368;

        if (VirtualRobot::RuntimeEnvironment::hasValue("paddingX")) {
            paddingX = simox::alg::to_<int>(getParameter("paddingX"));
        }

        if (VirtualRobot::RuntimeEnvironment::hasValue("paddingY")) {
            paddingY = simox::alg::to_<int>(getParameter("paddingY"));
        }

        if (VirtualRobot::RuntimeEnvironment::hasValue("gaussKernel")) {
            gaussKernel = simox::alg::to_<int>(getParameter("gaussKernel"));
        }

        if (VirtualRobot::RuntimeEnvironment::hasValue("rotateCode")) {
            rotateCode = simox::alg::to_<int>(getParameter("rotateCode"));
        }

        log = VirtualRobot::RuntimeEnvironment::hasFlag("log");

        print();
        return valid;
    }

    void print()
    {
        std::cout << "*** AddBody25 Configuration ***\n";
        if (!inputVideoPath.empty()) {
            std::cout << "Input Video: " << inputVideoPath << "\n";
            std::cout << "Output Video: " << outputVideoPath << "\n";
        }
        else if (!inputVideoDir.empty()){
            std::cout << "Input Video directory: " << inputVideoDir << "\n";
            std::cout << "Output Video directory: " << outputVideoDir << "\n";
        }
        else {
            std::cout << "Either inputVideo or inputVideoDirectory has to be specified!" << std::endl;
        }
        std::cout << std::endl;
    }
};

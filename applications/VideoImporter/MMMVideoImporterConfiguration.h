/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Franziska Krebs
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_VIDEOIMPORTERCONFIGURATION_H_
#define __MMM_VIDEOIMPORTERCONFIGURATION_H_

#include <MMM/MMMCore.h>
#include <filesystem>
#include <string>
#include <SimoxUtility/algorithm/string/string_tools.h>

#include <VirtualRobot/RuntimeEnvironment.h>
#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMMVIDEOIMPORTER.
*/
class MMMVideoImporterConfiguration : public ApplicationBaseConfiguration
{

public:
    std::filesystem::path inputMotionPath;
    std::filesystem::path outputMotionPath;
    std::vector<std::filesystem::path> sensorPluginPaths;
    std::filesystem::path dataFilePath;
    std::string motionName;
    float timestepDelta;

    MMMVideoImporterConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPluginPaths");
        VirtualRobot::RuntimeEnvironment::considerKey("dataFilePath");
        VirtualRobot::RuntimeEnvironment::considerKey("motionName");
        VirtualRobot::RuntimeEnvironment::considerKey("timestepDelta");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath = getParameter("inputMotion", true, true);
        outputMotionPath = getParameter("outputMotion", false, false);
        if (outputMotionPath.empty())
            outputMotionPath = inputMotionPath.stem().generic_string() + ".xml";
        dataFilePath = getParameter("dataFilePath", true, true);
        motionName = getParameter("motionName", false, false);
        sensorPluginPaths = getPaths("sensorPlugins", MMMVISION_SENSOR_PLUGIN_LIB_DIR);
        std::string timestepDeltaStr = getParameter("timestepDelta");
        if (!timestepDeltaStr.empty()) timestepDelta = simox::alg::to_<float>(timestepDeltaStr.c_str());
        else timestepDelta = 0.0f;

        return true;
    }

    std::vector<std::string> getParameters(const std::string &parameterName, bool emptyAllowed)
    {
        std::vector<std::string> parameters = simox::alg::split(getParameter(parameterName), ";,");

        parameters.erase(std::unique(parameters.begin(), parameters.end()), parameters.end());
        if (!emptyAllowed) parameters.erase(std::remove_if(parameters.begin(), parameters.end(), std::mem_fun_ref(&std::string::empty)), parameters.end());

        return parameters;
    }

    void print()
    {
        MMM_INFO << "*** MMMVideoImporter Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Output motion: " << outputMotionPath << std::endl;
    }
};


#endif

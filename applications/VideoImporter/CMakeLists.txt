cmake_minimum_required(VERSION 3.10.2)

set(TARGET_NAME "VideoImporter")

if(NOT(MMMCore_FOUND AND OpenCV_FOUND AND PCL_FOUND))
    message(STATUS "Required libraries not found for ${TARGET_NAME}")
    return()
endif()

set(LIBRARIES
    MMMCore
    SimoxUtility
    stdc++fs
    MMMVisionSensor
    MMMVisionSensorVisualisation
    MMMViewerLib
    VideoCameraSensor
)

set(SOURCES
    MMMVideoImporter.cpp
)

set(HEADERS
    MMMVideoImporterConfiguration.h
)

add_executable(${TARGET_NAME} ${SOURCES} ${HEADERS})
target_link_libraries(${TARGET_NAME} PRIVATE ${LIBRARIES})




/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Franziska Krebs
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_VIDEOIMPORTERCONFIGURATION_H_
#define __MMM_VIDEOIMPORTERCONFIGURATION_H_

#include <MMM/MMMCore.h>
#include <filesystem>
#include <string>
#include <SimoxUtility/algorithm/string/string_tools.h>

#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMMVision/Sensor/Plugin/Body25Sensor/Body25Source.h>
#include "../common/ApplicationBaseConfiguration.h"
//#include <boost/math/special_functions/binomial.hpp>

/*!
    Configuration of MMMVIDEOIMPORTER.
*/
class AddBody25Configuration : public ApplicationBaseConfiguration
{

public:
    std::filesystem::path inputMotionPath;
    std::filesystem::path inputMotionDir;
    std::filesystem::path outputMotionPath;
    std::filesystem::path outputMotionDir;
    bool overwrite;
    std::vector<std::filesystem::path> sensorPluginPaths;
    int netWidth;
    int netHeight;
    bool anonymize;
    float depthMin;
    float depthMax;
    float depthSigma;
    unsigned int depthRadius;
    std::string depthMethod;
    bool log;

    AddBody25Configuration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion", "Motion file xml");
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotionDir", "Directory of motion files");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion", "Output motion file path: Default (if not overwrite): {INPUTMOTION}_body25.xml");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotionDir", "Output motion directory: Default (if not overwrite): {INPUTMOTIONDIR}/body25/");
        VirtualRobot::RuntimeEnvironment::considerFlag("overwrite", "Flag to overwrite motion files.");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPluginPaths", "Additional sensor plugin paths to load. Default: MMMVision sensor plugins");
        VirtualRobot::RuntimeEnvironment::considerKey("netWidth", "Network width for OpenPose. Multiple of 16. Default: -1");
        VirtualRobot::RuntimeEnvironment::considerKey("netHeight", "Network height for OpenPose. Multiple of 16. Default: 160");
        VirtualRobot::RuntimeEnvironment::considerKey("depthSigma", "Sigma when using Gaussian method Default: depthRadius / 2");
        VirtualRobot::RuntimeEnvironment::considerKey("depthMin", "Minimum depth value in MM to be considered. Default: 100mm");
        VirtualRobot::RuntimeEnvironment::considerKey("depthMax", "Maximum depth value in MM  to be considered. Default: 4000mm");
        VirtualRobot::RuntimeEnvironment::considerKey("depthRadius", "Radius in pixels around the body keypoint to extract depth information. Default: 4px");
        VirtualRobot::RuntimeEnvironment::considerKey("depthMethod", "Depth extration method. Choose from ['Median', 'Gaussian']. Default: Gaussian");
        VirtualRobot::RuntimeEnvironment::considerFlag("anonymize", "Flag to indicate wheter the motions should be anonymized afterwards.");
        VirtualRobot::RuntimeEnvironment::considerFlag("log");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath = getParameter("inputMotion", false, true);
        inputMotionDir = getParameter("inputMotionDir", false, false);

        if ((inputMotionDir.empty() && inputMotionPath.empty()) || (!inputMotionDir.empty() && !inputMotionPath.empty())) {
            valid = false;
        }

        overwrite = VirtualRobot::RuntimeEnvironment::hasFlag("overwrite");

        outputMotionPath = getParameter("outputMotion", false, false);
        if (!inputMotionPath.empty() && outputMotionPath.empty()) {
            if (overwrite) outputMotionPath = inputMotionPath;
            else outputMotionPath = inputMotionPath.stem().generic_string() + "_body25.xml";
        }
        outputMotionDir = getParameter("outputMotionDir", false, false);
        if (!inputMotionDir.empty() && outputMotionDir.empty()) {
            if (overwrite) outputMotionDir = inputMotionDir;
            else outputMotionDir = inputMotionDir / std::filesystem::path("body25");
        }

        sensorPluginPaths = getPaths("sensorPlugins", MMMVISION_SENSOR_PLUGIN_LIB_DIR);

        if (VirtualRobot::RuntimeEnvironment::hasValue("netWidth")) {
            netWidth = simox::alg::to_<int>(getParameter("netWidth"));
        }
        else netWidth = -1;

        if (VirtualRobot::RuntimeEnvironment::hasValue("netHeight")) {
            netHeight = simox::alg::to_<int>(getParameter("netHeight"));
        }
        else netHeight = 160;

        if (VirtualRobot::RuntimeEnvironment::hasValue("depthMin")) {
            depthMin = simox::alg::to_<float>(getParameter("depthMin"));
        }
        else depthMin = 100;

        if (VirtualRobot::RuntimeEnvironment::hasValue("depthMax")) {
            depthMax = simox::alg::to_<float>(getParameter("depthMax"));
        }
        else depthMax = 4000;

        if (VirtualRobot::RuntimeEnvironment::hasValue("depthRadius")) {
            depthRadius = simox::alg::to_<unsigned int>(getParameter("depthRadius"));
        }
        else depthRadius = 4;

        if (VirtualRobot::RuntimeEnvironment::hasValue("depthSigma")) {
            depthSigma = simox::alg::to_<float>(getParameter("depthSigma"));
        }
        else depthSigma = depthRadius / 2.0f;
        //else depthSigma = std::pow(depthRadius * 2 + 1, 2) / (boost::math::binomial_coefficient<float>(depthRadius * 2 + 1, depthRadius) * std::sqrt(2 * M_PI));


        depthMethod = getParameter("depthMethod");
        if (depthMethod.empty()) depthMethod = "Gaussian";

        anonymize = VirtualRobot::RuntimeEnvironment::hasFlag("anonymize");

        log = VirtualRobot::RuntimeEnvironment::hasFlag("log");

        print();
        return valid;
    }

    void print()
    {
        MMM_INFO << "*** AddBody25 Configuration ***\n";
        if (!inputMotionPath.empty()) {
            std::cout << "Input motion: " << inputMotionPath << "\n";
            std::cout << "Output motion: " << outputMotionPath << "\n";
        }
        else if (!inputMotionDir.empty()){
            std::cout << "Input motion directory: " << inputMotionDir << "\n";
            std::cout << "Output motion directory: " << outputMotionDir << "\n";
        }
        else {
            MMM_ERROR << "Either inputMotion or inputMotionDirectory has to be specified!" << std::endl;
        }
        std::cout << "Depth extraction method: " << depthMethod << "\n";
        std::cout << "Depth radius: " << depthRadius << "\n";
        if (depthMethod == "Gaussian") {
            std::cout << "Gaussian sigma: " << depthSigma << "\n";
        }
        std::cout << "Depth minimum: " << depthMin << "\n";
        std::cout << "Depth maximum: " << depthMax << "\n";
        std::cout << std::endl;
    }
};


#endif

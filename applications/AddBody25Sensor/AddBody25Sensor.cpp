
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Motion.h>
#include <MMMVision/Tools/Video.h>
#include <MMMVision/Tools/VideoTools.h>
#include <MMMVision/Tools/OpenPose/OpenPoseTools.h>
#include <MMMVision/Tools/Errors.h>
#include <SensorLibraries/AzureKinectDKWrapper/AzureKinectPlayback.h>
#include <MMMVision/Sensor/Plugin/AzureKinectDKSensor/AzureKinectDKSensor.h>
#include <MMMVision/Sensor/Plugin/AzureKinectDKSensor/AzureKinectDKSensorMeasurement.h>
#include <MMMVision/Sensor/Plugin/Body25Sensor/Body25Sensor.h>
#include <MMMVision/Sensor/Plugin/Body25Sensor/Body25SensorMeasurement.h>
#include <MMM/Model/LoadModelStrategy.h>
#include "AddBody25Configuration.h"
#include <regex>

cv::Rect2f getROI(const std::vector<cv::Point2f> &points, float paddingL = 35, float paddingR = 35, float paddingB = 100, float paddingT = 50) {
    float l = 10000.0f;
    float t = 10000.0f;
    float r = 0;
    float b = 0;

    for (const cv::Point2f p : points) {
        std::cout << l << " " << p << std::endl;
        if (p.x < l) l = p.x;
        if (p.x > r) r = p.x;
        if (p.y < t) t = p.y;
        if (p.y > b) b = p.y;
    }
    return cv::Rect2f(cv::Point2f(l - paddingL, t - paddingT), cv::Point2f(r + paddingR, b + paddingB));
}


int main(int argc, char *argv[])
{
    MMM_INFO << " --- AddBody25 --- " << std::endl;
    AddBody25Configuration* configuration = new AddBody25Configuration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    MMMVision::OpenPoseWrapper w;

    op::Point<int> netSize(configuration->netWidth, configuration->netHeight);
    op::Wrapper opWrapper{op::ThreadManagerMode::Asynchronous};
    w.configure(opWrapper, netSize);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    opWrapper.start();

    MMM::LoadModelStrategy::LOAD_MODEL_STRATEGY = MMM::LoadModelStrategy::Strategy::NONE;
    MMM::MotionReaderXMLPtr reader(new MMM::MotionReaderXML(true, false, configuration->sensorPluginPaths));
    std::vector<std::pair<std::string, std::string>> motionRecordingPaths;
    if (!configuration->inputMotionPath.empty()) {
        if (!configuration->overwrite && MMM::xml::isValid(configuration->outputMotionPath))
            MMM_INFO << configuration->outputMotionPath << " already exists!" << std::endl;
        else motionRecordingPaths.push_back(std::make_pair(configuration->inputMotionPath, configuration->outputMotionPath));
    }
    if (!configuration->inputMotionDir.empty()) {
        std::filesystem::create_directory(configuration->outputMotionDir);
        for (auto inputMotionPath : reader->getMotionPathsFromDirectory(configuration->inputMotionDir)) {
            std::filesystem::path outputMotionPath = configuration->outputMotionDir / inputMotionPath.filename();
            if (!configuration->overwrite && MMM::xml::isValid(outputMotionPath))
                MMM_INFO << outputMotionPath << " already exists!" << std::endl;
            else motionRecordingPaths.push_back(std::make_pair(inputMotionPath, outputMotionPath));
        }
    }

    for (const auto &motionRecordingPath : motionRecordingPaths) {
        MMM::MotionRecordingPtr recording;
        try {
            recording = reader->loadMotionRecording(motionRecordingPath.first);
        }
        catch (MMM::Exception::MMMFormatException &e) {
            MMM_ERROR << e.what() << std::endl;
            continue;
        }
        bool extracted = false;
        MMM_INFO << "Extraction from motion file " << motionRecordingPath.first << std::endl;
        for (auto motion : *recording) {
            auto sensors = motion->getSensorsByType<MMM::AzureKinectDKSensor>();
            if (sensors.size() > 0) {
                cv::Mat colorImage;
                cv::Mat depthImage;
                cv::Mat irImage;

                auto sensor = sensors.at(0);
                MMM::Body25SensorPtr body25sensor(new MMM::Body25Sensor());

                body25sensor->source.sensorType = sensor->getType();
                body25sensor->source.fileName = sensor->getVideoFilePath().stem();
                body25sensor->source.keyPointSource.method = MMM::BODY25::KeyPointExtractionMethod::OpenPose;
                body25sensor->source.keyPointSource.parameters["netsize"] = std::to_string(configuration->netWidth) + "x" + std::to_string(configuration->netHeight);
                body25sensor->source.depthSource = MMM::BODY25::DepthSource();
                body25sensor->source.depthSource.value().method = MMM::BODY25::DepthExtractionMethod::_from_string(configuration->depthMethod.c_str());
                body25sensor->source.depthSource.value().radius = configuration->depthRadius;
                body25sensor->source.depthSource.value().min = configuration->depthMin;
                body25sensor->source.depthSource.value().max = configuration->depthMax;
                if (body25sensor->source.depthSource.value().method._value == MMM::BODY25::DepthExtractionMethod::Gaussian)
                    body25sensor->source.depthSource.value().sigma = configuration->depthSigma;

                auto measurement = sensor->getFirstMeasurement(); // Retrieving timesteps is very slow for AzureKinectSensorDK
                while (measurement) {
                    try {
                        float timestep = measurement->getTimestep();
                        if (configuration->log) MMM_INFO << "Extraction at timestep " << timestep << "\n";

                        if (!measurement->getImages(colorImage, depthImage, irImage, true, false)) {
                            MMM_ERROR << "Capturing image went wrong!" << std::endl;
                            break;
                        }

                        cv::Mat colorImageBGR;
                        cv::cvtColor(colorImage, colorImageBGR, CV_RGB2BGR);

                        std::map<int, MMMVision::OpenPoseWrapper::OpenPoseKeyPoint> keyPoints;
                        std::vector<cv::Rect2f> rois;

                        w.extractKeyPoints(opWrapper, colorImageBGR, keyPoints, rois);

                        std::map<int, MMM::Body25KeyPoint> body25KeyPoints;
                        std::vector<cv::Point2f> points;
                        for (const auto &k : keyPoints) {
                            if (configuration->log) MMM_INFO << k.first << ": " << k.second.toString() << "\n";

                            MMM::Body25KeyPoint kp;
                            kp.camera_x = k.second.x;
                            kp.camera_y = k.second.y;
                            kp.score = k.second.score;

                            Eigen::MatrixXf depthMatrix = Eigen::MatrixXf::Zero(configuration->depthRadius * 2 + 1, configuration->depthRadius * 2 + 1);
                            Eigen::MatrixXf filterMatrix = Eigen::MatrixXf::Zero(configuration->depthRadius * 2 + 1, configuration->depthRadius * 2 + 1);

                            for (int i = -configuration->depthRadius; i <= (int)configuration->depthRadius; i++) {
                                for (int j = -configuration->depthRadius; j <= (int)configuration->depthRadius; j++) {
                                    float depthValue = depthImage.at<unsigned short>(cv::Point(k.second.x + i, k.second.y + j));
                                    depthMatrix(configuration->depthRadius + i, configuration->depthRadius + j) = depthValue;
                                    if (depthValue > configuration->depthMin && depthValue < configuration->depthMax) {
                                        if (configuration->depthMethod == "Median") {
                                            filterMatrix(configuration->depthRadius + i, configuration->depthRadius + j) = 1.0f;
                                        }
                                        else if (configuration->depthMethod == "Gaussian") {
                                            filterMatrix(configuration->depthRadius + i, configuration->depthRadius + j) =
                                                std::exp(-0.5 * (std::pow(i/configuration->depthSigma, 2.0) + std::pow(j/configuration->depthSigma, 2.0))) / (2 * M_PI * configuration->depthSigma * configuration->depthSigma);
                                        }
                                        else {
                                            throw std::runtime_error("Unknown depth method " + configuration->depthMethod);
                                        }
                                    }
                                }
                            }

                            float sum = filterMatrix.sum();
                            if (sum > 0) {
                                float depthValue = (depthMatrix.cwiseProduct(filterMatrix)).sum() / sum;
                                Eigen::Vector3f cameraPosition3D = sensor->transform2dTo3d(k.second.x, k.second.y, depthValue);
                                Eigen::Vector3f position = sensor->toModelPoseVec(cameraPosition3D);
                                kp.local_pose = position;
                            }

                            body25KeyPoints[k.first] = kp;

                            if (configuration->anonymize) {
                                if (k.first == 0 || k.first == 15 || k.first == 16 || k.first == 17 || k.first == 18) {
                                    points.push_back(cv::Point2f(k.second.x, k.second.y));
                                }
                            }
                        }

                        if (configuration->anonymize) {
                            rois.push_back(getROI(points, 0,0,0,0));
                            //auto r = getROI(points, 0,0,0,0);
                            //cv::circle(mask_img, cv::Point2f(r.x + r.width / 2, r.y + r.height / 2), 100, cv::Scalar(255, 255, 255), -1);

                            rois.push_back(cv::Rect2f(cv::Point2f(0, 0), cv::Point2f(colorImageBGR.cols / 4, colorImageBGR.rows)));
                            rois.push_back(cv::Rect2f(cv::Point2f(colorImageBGR.cols * 3 / 4, 0), cv::Point2f(colorImageBGR.cols, colorImageBGR.rows)));

                            for (const auto &roi : rois) {
                                MMMVision::Tools::anonymize(colorImage, roi, 0, 51, cv::Rect2f(0, 0, colorImage.cols, colorImage.rows));
                            }
                            std::string directory = std::string(configuration->inputMotionPath.parent_path()) + "/" + motion->getName() + "/";
                            std::filesystem::create_directory(directory);
                            cv::imwrite(directory + std::to_string(timestep) + ".png", colorImage);
                        }

                        MMM::Body25SensorMeasurementPtr body25measurement(new MMM::Body25SensorMeasurement(timestep, body25KeyPoints));
                        body25sensor->addSensorMeasurement(body25measurement);

                        measurement.reset();
                        measurement = sensor->getNextMeasurement();

                        colorImage.release();
                        colorImageBGR.release();
                        depthImage.release();
                        irImage.release();
                    }
                    catch (std::runtime_error &e) {
                        MMM_ERROR << "Runtime Error: " << e.what() << std::endl;
                        colorImage.release();
                        depthImage.release();
                        irImage.release();
                        break;
                    }

                }
                motion->addSensor(body25sensor, 0.0f);
                MMM_INFO << "Body 25 sensor added for motion " << motion->getName() << std::endl;
                extracted = true;
            }
        }

        if (extracted) {
            MMM_INFO << "Saving motion.." << std::endl;
            recording->saveXML(motionRecordingPath.second);
            MMM_INFO << "Motion file saved at " << motionRecordingPath.second << std::endl;
        }
        else {
            MMM_ERROR << motionRecordingPath.first << " does not contain AzureKinectDKSensor" << std::endl;
        }
    }
    opWrapper.stop();


    return 0;
}



/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#pragma once

#include <filesystem>
#include <functional>
#include <atomic>
#include <fstream>
#include <mutex>

#include "IAzureKinect.h"

namespace MMMVision
{
    class AzureKinect : public IAzureKinect
    {
    public:
        AzureKinect(AzureKinectConfigPtr config = AzureKinectConfigPtr(new AzureKinectConfig()));

        virtual ~AzureKinect();

        uint32_t getDeviceCount();
        void checkValidDeviceIndex(uint8_t deviceIndex);

        static inline std::string VersionToString(const k4a_version_t& version);


        /*std::string getSerialNumber() {
            return k4a::device::get_serialnum()
        }

        std::string getRGBVersion() {
            return VersionToString(device.get_version().rgb);
        }

        std::string getDepthVersion() {
            return VersionToString(device.get_version().depth);
        }

        std::string getDepthSensorVersion() {
            return VersionToString(device.get_version().depth_sensor);
        }

        std::string getAudioVersion() {
            return VersionToString(device.get_version().audio);
        }*/

        void startCamera(uint8_t deviceIndex = K4A_DEVICE_DEFAULT);
        void stopCamera();
        void saveRawCalibration(const std::filesystem::path &directory, const std::string &fileName = "calibration");
        void close();

        bool getCapture(k4a_capture_t &capture) override;
        void writeCapture(k4a_capture_t capture);

        void startRecording(const std::string& fileName, bool storeAdditionalInfo = true);
        void runRecording(std::function<std::string()> additionalInfo = []() { return std::string(); });
        void recordCapture(std::function<std::string()> additionalInfo = []() { return std::string(); });
        void stopRecording();

        enum POWERLINE_FREQ { FIFTY_HZ, SIXTY_HZ };
        void setManualColorControl(int32_t color_exposure_usec = 8000);
        void setManualColorControl(POWERLINE_FREQ powerline_freq = SIXTY_HZ);

        bool checkSync();

        AzureKinectConfigPtr getConfig() override;

    private:
        AzureKinectConfigPtr config;
        k4a_device_t device;
        k4a_record_t record;
        uint8_t deviceIndex;
        std::atomic<bool> recording = false;
        std::atomic<bool> isCameraStarted = false;
        std::chrono::steady_clock::time_point lastTest;
        std::ofstream additionalInfoFile;
        std::mutex writeRecordMutex;
    };

    typedef std::shared_ptr<AzureKinect> AzureKinectPtr;
}

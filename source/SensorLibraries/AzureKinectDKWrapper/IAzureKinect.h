/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#pragma once

#include <k4a/k4atypes.h>
#include "AzureKinectConfig.h"
#include <opencv2/core/mat.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace MMMVision
{

class IAzureKinect
{
public:
    IAzureKinect();
    virtual ~IAzureKinect();

    static cv::Mat transform(k4a_image_t &k4a_image);
    static cv::Mat normalize(const cv::Mat &image, const std::pair<int, int> &range);

    virtual bool getCapture(k4a_capture_t &capture) = 0;
    k4a_image_t getColorImageFromCapture(const k4a_capture_t &capture);
    k4a_image_t getDepthImageFromCapture(const k4a_capture_t &capture);
    k4a_image_t getIRImageFromCapture(const k4a_capture_t &capture);
    k4a_capture_t getImages(k4a_image_t &colorImage, k4a_image_t &depthImage, bool transformToColor = true);
    k4a_capture_t getImages(k4a_image_t &colorImage, k4a_image_t &depthImage, k4a_image_t &irImage, bool transformToColor = true);
    bool getImagesFromCapture(const k4a_capture_t &capture, k4a_image_t &colorImage, k4a_image_t &depthImage, bool transformToColor = true);
    bool getImagesFromCapture(const k4a_capture_t &capture, k4a_image_t &colorImage, k4a_image_t &depthImage, k4a_image_t &irImage, bool transformToColor = true);

    bool getImages(cv::Mat &colorImage, cv::Mat &depthImage, bool transformToColor = true, bool normalize = false);
    bool getImages(cv::Mat &colorImage, cv::Mat &depthImage, cv::Mat &irImage, bool transformToColor = true, bool normalize = false);
    bool getImagesFromCapture(const k4a_capture_t &capture, cv::Mat &colorImage, cv::Mat &depthImage, bool transformToColor = true, bool normalize = false);
    bool getImagesFromCapture(const k4a_capture_t &capture, cv::Mat &colorImage, cv::Mat &depthImage, cv::Mat &irImage, bool transformToColor = true, bool normalize = false);

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr getRGBAPointCloud(int maximumDistanceMM = -1);
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr getRGBAPointCloudFromCapture(const k4a_capture_t &capture, int maximumDistanceMM = -1);

    Eigen::Vector3f transform2dTo3d(float x, float y, float depthMM);

    Eigen::Matrix3f getIntrinsicParametersColorEigen();
    virtual AzureKinectConfigPtr getConfig() = 0;

    k4a_calibration_t getCalibration();

    k4a_transformation_t getTransformation();

    bool is2x2BinColorImage();

    void set2x2BinColorImage(bool binColorImage = true);

protected:
    k4a_calibration_t calibration;
    k4a_transformation_t transformation;

private:
    k4a_image_t downscale_color_image_2x2_binning(const k4a_image_t color_image);
    bool transformToColor(const k4a_image_t &colorImage, const k4a_image_t &depthImage, k4a_image_t &transformedDepthImage);
    bool transformToColor(const k4a_image_t &colorImage, const k4a_image_t &depthImage, const k4a_image_t &irImage,
                          k4a_image_t &transformedDepthImage, k4a_image_t &transformedIRImage);


    bool binColorImage;
};

}

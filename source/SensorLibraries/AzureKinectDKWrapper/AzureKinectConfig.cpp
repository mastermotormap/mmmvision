#include "AzureKinectConfig.h"

namespace MMMVision
{

// Gets the dimensions of the color images that the color camera will produce for a given color resolution.
std::pair<int, int> colorDimensions(const k4a_color_resolution_t resolution)
{
    switch (resolution)
    {
        case K4A_COLOR_RESOLUTION_720P:
            return { 1280, 720 };
        case K4A_COLOR_RESOLUTION_2160P:
            return { 3840, 2160 };
        case K4A_COLOR_RESOLUTION_1440P:
            return { 2560, 1440 };
        case K4A_COLOR_RESOLUTION_1080P:
            return { 1920, 1080 };
        case K4A_COLOR_RESOLUTION_3072P:
            return { 4096, 3072 };
        case K4A_COLOR_RESOLUTION_1536P:
            return { 2048, 1536 };
        default:
            throw std::runtime_error("Invalid color dimensions value!");
    }
}

// Gets the dimensions of the depth images that the depth camera will produce for a given depth mode.
std::pair<int, int> depthDimensions(const k4a_depth_mode_t depthMode)
{
    switch (depthMode)
    {
        case K4A_DEPTH_MODE_NFOV_2X2BINNED:
            return { 320, 288 };
        case K4A_DEPTH_MODE_NFOV_UNBINNED:
            return { 640, 576 };
        case K4A_DEPTH_MODE_WFOV_2X2BINNED:
            return { 512, 512 };
        case K4A_DEPTH_MODE_WFOV_UNBINNED:
            return { 1024, 1024 };
        case K4A_DEPTH_MODE_PASSIVE_IR:
            return { 1024, 1024 };
        default:
            throw std::runtime_error("Invalid depth dimensions value!");
    }
}


static std::unordered_map<std::string, k4a_image_format_t> STRING_TO_IMAGE_FORMAT
{
    {"MJPG",            K4A_IMAGE_FORMAT_COLOR_MJPG},
    {"NV12",            K4A_IMAGE_FORMAT_COLOR_NV12},
    {"YUY2",            K4A_IMAGE_FORMAT_COLOR_YUY2},
    {"BGRA32",          K4A_IMAGE_FORMAT_COLOR_BGRA32},
    {"DEPTH16",         K4A_IMAGE_FORMAT_DEPTH16},
    {"IR16",            K4A_IMAGE_FORMAT_IR16},
    {"CUSTOM",          K4A_IMAGE_FORMAT_CUSTOM},
};

static std::unordered_map<k4a_image_format_t, std::string> IMAGE_FORMAT_TO_STRING = flipMap(STRING_TO_IMAGE_FORMAT);


static std::unordered_map<std::string, k4a_color_resolution_t> STRING_TO_COLOR_RES
{
    {"OFF",             K4A_COLOR_RESOLUTION_OFF},
    {"720P",            K4A_COLOR_RESOLUTION_720P},
    {"1080P",           K4A_COLOR_RESOLUTION_1080P},
    {"1440P",           K4A_COLOR_RESOLUTION_1440P},
    {"1536P",           K4A_COLOR_RESOLUTION_1536P},
    {"2160P",           K4A_COLOR_RESOLUTION_2160P},
    {"3072P",           K4A_COLOR_RESOLUTION_3072P},
};

static std::unordered_map<k4a_color_resolution_t, std::string> COLOR_RES_TO_STRING = flipMap(STRING_TO_COLOR_RES);


static std::unordered_map<std::string, k4a_depth_mode_t> STRING_TO_DEPTH_MODE
{
    {"OFF",             K4A_DEPTH_MODE_OFF},
    {"NFOV_2X2BINNED",  K4A_DEPTH_MODE_NFOV_2X2BINNED},
    {"NFOV_UNBINNED",   K4A_DEPTH_MODE_NFOV_UNBINNED},
    {"WFOV_2X2BINNED",  K4A_DEPTH_MODE_WFOV_2X2BINNED},
    {"WFOV_UNBINNED",   K4A_DEPTH_MODE_WFOV_UNBINNED},
    {"PASSIVE_IR",      K4A_DEPTH_MODE_PASSIVE_IR},
};

static std::unordered_map<k4a_depth_mode_t, std::string> DEPTH_MODE_TO_STRING = flipMap(STRING_TO_DEPTH_MODE);


static std::unordered_map<std::string, k4a_fps_t> STRING_TO_FPS
{
    {"5",               K4A_FRAMES_PER_SECOND_5},
    {"15",              K4A_FRAMES_PER_SECOND_15},
    {"30",              K4A_FRAMES_PER_SECOND_30},
};

static std::unordered_map<k4a_fps_t, std::string> FPS_TO_STRING = flipMap(STRING_TO_FPS);


static std::unordered_map<std::string, k4a_wired_sync_mode_t> STRING_TO_WIRED_SYNC
{
    {"STANDALONE",      K4A_WIRED_SYNC_MODE_STANDALONE},
    {"MASTER",          K4A_WIRED_SYNC_MODE_MASTER},
    {"SUBORDINATE",     K4A_WIRED_SYNC_MODE_SUBORDINATE},
};

static std::unordered_map<k4a_wired_sync_mode_t, std::string> WIRED_SYNC_TO_STRING = flipMap(STRING_TO_WIRED_SYNC);

AzureKinectConfigPtr AzureKinectConfig::DEFAULT_STREAMING() {
    return AzureKinectConfigPtr(new AzureKinectConfig(K4A_IMAGE_FORMAT_COLOR_BGRA32, K4A_COLOR_RESOLUTION_720P));
}

AzureKinectConfigPtr AzureKinectConfig::DEFAULT_RECORDING() {
    return AzureKinectConfigPtr(new AzureKinectConfig(K4A_IMAGE_FORMAT_COLOR_MJPG, K4A_COLOR_RESOLUTION_1080P));
}

AzureKinectConfig::AzureKinectConfig()
    : config(K4A_DEVICE_CONFIG_INIT_DISABLE_ALL)
{
}

AzureKinectConfig::AzureKinectConfig(k4a_image_format_t colorFormat, k4a_color_resolution_t colorRes)
    : config(K4A_DEVICE_CONFIG_INIT_DISABLE_ALL)
{
    config.color_format                         = colorFormat;
    config.color_resolution                     = colorRes;
    config.depth_mode                           = K4A_DEPTH_MODE_NFOV_UNBINNED;
    config.camera_fps                           = K4A_FRAMES_PER_SECOND_30;
    config.synchronized_images_only             = true;
    config.depth_delay_off_color_usec           = 0;
    config.wired_sync_mode                      = K4A_WIRED_SYNC_MODE_STANDALONE;
    config.subordinate_delay_off_master_usec    = 0;
    config.disable_streaming_indicator          = false;
}

AzureKinectConfig::AzureKinectConfig(k4a_device_configuration_t config)
    : config(config)
{
}

AzureKinectConfig::AzureKinectConfig(k4a_record_configuration_t config)
{
    this->config.color_format                         = config.color_format;
    this->config.color_resolution                     = config.color_resolution;
    this->config.depth_mode                           = config.depth_mode;
    this->config.camera_fps                           = config.camera_fps;
    this->config.synchronized_images_only             = true;
    this->config.depth_delay_off_color_usec           = config.depth_delay_off_color_usec;
    this->config.wired_sync_mode                      = config.wired_sync_mode;
    this->config.subordinate_delay_off_master_usec    = config.subordinate_delay_off_master_usec;
    this->config.disable_streaming_indicator          = false;
}



void AzureKinectConfig::setColorFormat(const std::string& colorFormat) {
    setColorFormat(getThrow(colorFormat, STRING_TO_IMAGE_FORMAT));
}

void AzureKinectConfig::setColorFormat(k4a_image_format_t colorFormat) {
    config.color_format = colorFormat;
}

k4a_image_format_t AzureKinectConfig::getColorFormat() const {
    return config.color_format;
}

const std::string AzureKinectConfig::getColorFormatAsStr() const {
    return getThrow(getColorFormat(), IMAGE_FORMAT_TO_STRING);
}



void AzureKinectConfig::setColorResolution(const std::string& colorRes) {
    setColorResolution(getThrow(colorRes, STRING_TO_COLOR_RES));
}

void AzureKinectConfig::setColorResolution(k4a_color_resolution_t colorRes) {
    config.color_resolution = colorRes;
}

void AzureKinectConfig::deactivateRGBCamera() {
    setColorResolution(K4A_COLOR_RESOLUTION_OFF);
}

k4a_color_resolution_t AzureKinectConfig::getColorResolution() const {
    return config.color_resolution;
}

const std::string AzureKinectConfig::getColorResolutionAsStr() const {
    return getThrow(getColorResolution(), COLOR_RES_TO_STRING);
}

std::pair<int, int> AzureKinectConfig::getColorDimensions() const {
    return colorDimensions(config.color_resolution);
}



void AzureKinectConfig::setDepthMode(const std::string& depthMode) {
    setDepthMode(getThrow(depthMode, STRING_TO_DEPTH_MODE));
}

void AzureKinectConfig::setDepthMode(k4a_depth_mode_t depthMode) {
    config.depth_mode = depthMode;
}

void AzureKinectConfig::deactivateDepthCamera() {
    setDepthMode(K4A_DEPTH_MODE_OFF);
}

k4a_depth_mode_t AzureKinectConfig::getDepthMode() const {
    return config.depth_mode;
}

const std::string AzureKinectConfig::getDepthModeAsStr() const {
    return getThrow(getDepthMode(), DEPTH_MODE_TO_STRING);
}

std::pair<int, int> AzureKinectConfig::getDepthDimensions() const {
    return depthDimensions(config.depth_mode);
}

std::pair<int, int> AzureKinectConfig::GetDepthModeRange() const {
    return depthModeRange(getDepthMode());
}

std::pair<int, int> AzureKinectConfig::GetIrLevels() const {
    return irLevels(getDepthMode());
}



void AzureKinectConfig::setFPS(const std::string& fps) {
    setFPS(getThrow(fps, STRING_TO_FPS));
}

void AzureKinectConfig::setFPS(k4a_fps_t fps) {
    config.camera_fps = fps;
}

k4a_fps_t AzureKinectConfig::getFPS() const {
    return config.camera_fps;
}

const std::string AzureKinectConfig::getFPSAsStr() const {
    return getThrow(getFPS(), FPS_TO_STRING);
}

unsigned int AzureKinectConfig::getFPSAsInt() const {
    switch (getFPS()) {
    case K4A_FRAMES_PER_SECOND_5:
        return 5;
    case K4A_FRAMES_PER_SECOND_15:
        return 15;
    case K4A_FRAMES_PER_SECOND_30:
        return 30;
    }
    return 0;
}

/*void AzureKinectConfig::setSynchronizeImages(const std::string& synchronize) {
    try {
        setSynchronizeImages(simox::alg::to_string(synchronize);
    }
    catch (SimoxError &e) {
        // TODO
    }
}*/

void AzureKinectConfig::setSynchronizeImages(bool synchronize) {
    config.synchronized_images_only = synchronize;
}

bool AzureKinectConfig::getSynchronizeImages() const {
    return config.synchronized_images_only;
}

/*const std::string AzureKinectConfig::getSynchronizeImagesAsStr() const {
    return simox::alg::to_string(getSynchronizeImages());
}*/



/*void AzureKinectConfig::setDepthDelayInUsec(const std::string& depthDelayOffColor) {
    try {
        setDepthDelayInUsec(simox::alg::to_<int>(depthDelayOffColor);
    }
    catch (SimoxError &e) {
        // TODO
    }
}*/

void AzureKinectConfig::setDepthDelayInUsec(int depthDelayOffColor) {
    config.depth_delay_off_color_usec = depthDelayOffColor;
}

int AzureKinectConfig::getDepthDelayInUsec() const {
    return config.depth_delay_off_color_usec;
}

/*const std::string AzureKinectConfig::getDepthDelayInUsecAsStr() const {
    return simox::alg::to_string(getDepthDelayInUsec());
}*/



void AzureKinectConfig::setWiredSyncMode(const std::string& wiredSyncMode) {
    setWiredSyncMode(getThrow(wiredSyncMode, STRING_TO_WIRED_SYNC));
}

void AzureKinectConfig::setWiredSyncMode(k4a_wired_sync_mode_t wiredSyncMode) {
    config.wired_sync_mode = wiredSyncMode;
}

k4a_wired_sync_mode_t AzureKinectConfig::getWiredSyncMode() const {
    return config.wired_sync_mode;
}

const std::string AzureKinectConfig::getWiredSyncModeAsStr() const {
    return getThrow(getWiredSyncMode(), WIRED_SYNC_TO_STRING);
}



/*void AzureKinectConfig::setSubordinateDelayInUsec(const std::string& subordinateDelayOffMaster) {
    try {
        setSubordinateDelayInUsec(simox::alg::to_<int>(subordinateDelayOffMaster);
    }
    catch (SimoxError &e) {
        // TODO
    }
}*/

void AzureKinectConfig::setSubordinateDelayInUsec(int subordinateDelayOffMaster) {
    config.subordinate_delay_off_master_usec = subordinateDelayOffMaster;
}

int AzureKinectConfig::getSubordinateDelayInUsec() const {
    return config.depth_delay_off_color_usec;
}

/*const std::string AzureKinectConfig::getSubordinateDelayInUsecAsStr() const {
    return simox::alg::to_string(getSubordinateDelayInUsec());
}*/

int AzureKinectConfig::getTimeout() {
    return 100; // return 1000 / getFPSAsInt();
}


bool AzureKinectConfig::isCameraActivated() {
    return config.color_resolution != K4A_COLOR_RESOLUTION_OFF || config.depth_mode != K4A_DEPTH_MODE_OFF;
}


k4a_device_configuration_t* AzureKinectConfig::getConfig() {
    return &config;
}

std::pair<uint16_t, uint16_t> depthModeRange(const k4a_depth_mode_t depthMode)
{
    switch (depthMode)
    {
    case K4A_DEPTH_MODE_NFOV_2X2BINNED:
        return { (uint16_t)500, (uint16_t)5800 };
    case K4A_DEPTH_MODE_NFOV_UNBINNED:
        return { (uint16_t)500, (uint16_t)4000 };
    case K4A_DEPTH_MODE_WFOV_2X2BINNED:
        return { (uint16_t)250, (uint16_t)3000 };
    case K4A_DEPTH_MODE_WFOV_UNBINNED:
        return { (uint16_t)250, (uint16_t)2500 };

    case K4A_DEPTH_MODE_PASSIVE_IR:
    default:
        throw std::logic_error("Invalid depth mode!");
    }
}

std::pair<uint16_t, uint16_t> irLevels(const k4a_depth_mode_t depthMode)
{
    switch (depthMode)
    {
    case K4A_DEPTH_MODE_PASSIVE_IR:
        return { (uint16_t)0, (uint16_t)100 };

    case K4A_DEPTH_MODE_OFF:
        throw std::logic_error("Invalid depth mode!");

    default:
        return { (uint16_t)0, (uint16_t)1000 };
    }
}

}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#pragma once

#include <filesystem>

#include "IAzureKinect.h"

namespace MMMVision
{

class AzureKinectPlayback : public IAzureKinect
{

public:
    AzureKinectPlayback(const std::filesystem::path &path, int offsetTimestampMS = 0);

    AzureKinectPlayback(const std::filesystem::path &path, k4a_image_format_t imageFormat, int offsetTimestampMS = 0);

    virtual ~AzureKinectPlayback();

    void setImageFormatConversion(k4a_image_format_t imageFormat = K4A_IMAGE_FORMAT_COLOR_BGRA32);

    void jumpToMS(int offsetTimestampMS = 0);

    void jumpTo(float timestep = 0.0f, float offset = 0.0f);

    void exportMKVFile(cv::Mat colorImage, cv::Mat depthImage, cv::Mat irImage, const std::filesystem::path &fileWithoutExtension);

    bool getPreviousCapture(k4a_capture_t &capture);

    bool getNextCapture(k4a_capture_t &capture);

    std::vector<float> getTimesteps(float offset = 0);

    bool getCapture(k4a_capture_t &capture) override;

    AzureKinectConfigPtr getConfig() override;

    float getStartTimestep() const;

    float getEndTimestep() const;

    float getRecordingLength() const;

    float getTimestep(const k4a_capture_t &capture, float offset = 0.0f);

private:
    AzureKinectConfigPtr config;
    k4a_playback_t playback;
    float startTimestepInSeconds;
};

typedef std::shared_ptr<AzureKinectPlayback> AzureKinectPlaybackPtr;

}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <k4a/k4atypes.h>
#include <k4arecord/types.h>
#include <algorithm>
#include <unordered_map>
#include <memory>

namespace MMMVision
{

template<typename A, typename B>
std::pair<B,A> flipPair(const std::pair<A,B> &p)
{
    return std::pair<B,A>(p.second, p.first);
}

template<typename A, typename B>
std::unordered_map<B,A> flipMap(const std::unordered_map<A,B> &src)
{
    std::unordered_map<B,A> dst;
    std::transform(src.begin(), src.end(), inserter(dst, dst.begin()), flipPair<A,B>);
    return dst;
}

template<typename A, typename B>
B getThrow(const A &value, const std::unordered_map<A, B> &unordered_map)
{
    if (unordered_map.find(value) != unordered_map.end())
        return unordered_map.at(value);
    else throw std::runtime_error("Key not found!");
}

//! Gets the dimensions of the color images that the color camera will produce for a given color resolution.
std::pair<int, int> colorDimensions(const k4a_color_resolution_t resolution);

//! Gets the dimensions of the depth images that the depth camera will produce for a given depth mode.
std::pair<int, int> depthDimensions(const k4a_depth_mode_t depthMode);

// Gets the range of values that we expect to see from the depth camera
// when using a given depth mode, in millimeters
//
std::pair<uint16_t, uint16_t> depthModeRange(const k4a_depth_mode_t depthMode);

// Gets the expected min/max IR brightness levels that we expect to see
// from the IR camera when using a given depth mode
//
std::pair<uint16_t, uint16_t> irLevels(const k4a_depth_mode_t depthMode);


class AzureKinectConfig;

typedef std::shared_ptr<AzureKinectConfig> AzureKinectConfigPtr;

class AzureKinectConfig
{
public:
    static AzureKinectConfigPtr DEFAULT_RECORDING();
    static AzureKinectConfigPtr DEFAULT_STREAMING();

    AzureKinectConfig();
    AzureKinectConfig(k4a_image_format_t colorFormat, k4a_color_resolution_t colorRes);
    AzureKinectConfig(k4a_device_configuration_t config);

    //! Converts k4a_record_configuration_t to k4a_device_configuration_t as good as possible
    AzureKinectConfig(k4a_record_configuration_t config);

    void setColorFormat(const std::string& colorFormat);
    void setColorFormat(k4a_image_format_t colorFormat);
    k4a_image_format_t getColorFormat() const;
    const std::string getColorFormatAsStr() const;

    void setColorResolution(const std::string& colorRes);
    void setColorResolution(k4a_color_resolution_t colorRes);
    void setColorDimensions(int width, int height);
    void deactivateRGBCamera();
    k4a_color_resolution_t getColorResolution() const;
    const std::string getColorResolutionAsStr() const;
    std::pair<int, int> getColorDimensions() const;

    void setDepthMode(const std::string& depthMode);
    void setDepthMode(k4a_depth_mode_t depthMode);
    void deactivateDepthCamera();
    k4a_depth_mode_t getDepthMode() const;
    const std::string getDepthModeAsStr() const;
    std::pair<int, int> getDepthDimensions() const;
    std::pair<int, int> GetDepthModeRange() const;
    std::pair<int, int> GetIrLevels() const;

    void setFPS(const std::string& fps);
    void setFPS(k4a_fps_t fps);
    k4a_fps_t getFPS() const;
    const std::string getFPSAsStr() const;
    unsigned int getFPSAsInt() const;

    // void setSynchronizeImages(const std::string& synchronize);
    void setSynchronizeImages(bool synchronize);
    bool getSynchronizeImages() const;
    //const std::string getSynchronizeImagesAsStr() const;

    //void setDepthDelayInUsec(const std::string& depthDelayOffColor);
    void setDepthDelayInUsec(int depthDelayOffColor);
    int getDepthDelayInUsec() const;
    //const std::string getDepthDelayInUsecAsStr() const;

    void setWiredSyncMode(const std::string& wiredSyncMode);
    void setWiredSyncMode(k4a_wired_sync_mode_t wiredSyncMode);
    k4a_wired_sync_mode_t getWiredSyncMode() const;
    const std::string getWiredSyncModeAsStr() const;

    //void setSubordinateDelayInUsec(const std::string& subordinateDelayOffMaster);
    void setSubordinateDelayInUsec(int subordinateDelayOffMaster);
    int getSubordinateDelayInUsec() const;
    //const std::string getSubordinateDelayInUsecAsStr() const;

    int getTimeout();

    bool isCameraActivated();

    k4a_device_configuration_t* getConfig();
private:
    k4a_device_configuration_t config;
};

}

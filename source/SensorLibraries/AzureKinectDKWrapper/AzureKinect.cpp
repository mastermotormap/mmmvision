#include "AzureKinect.h"

#include <k4a/k4a.h>
#include <k4arecord/record.h>
#include <iostream>
#include <fstream>
#include <future>
#include <sstream>
#include <k4arecord/types.h>
#include <memory>
#include <chrono>
#include <fstream>
#include "AzureKinectConfig.h"

namespace MMMVision
{
    AzureKinect::AzureKinect(AzureKinectConfigPtr config) :
        IAzureKinect(),
        config(config),
        recording(false),
        isCameraStarted(false)
    {
    }

    AzureKinect::~AzureKinect()
    {
        close();
    }

    uint32_t AzureKinect::getDeviceCount() {
        return k4a_device_get_installed_count();
    }

    void AzureKinect::checkValidDeviceIndex(uint8_t deviceIndex) {
        const uint32_t deviceCount = getDeviceCount();
        if (deviceCount == 0)
            throw std::runtime_error("No Azure Kinect devices detected");
        if (deviceCount < deviceIndex)
            throw std::runtime_error("No Azure Kinect at index " + std::to_string(deviceIndex));
    }

    std::string AzureKinect::VersionToString(const k4a_version_t &version)
    {
        std::stringstream s;
        s << version.major << "." << version.minor << "." << version.iteration;
        return s.str();
    }

    void AzureKinect::startCamera(uint8_t deviceIndex) {
        if (!isCameraStarted) {
            checkValidDeviceIndex(deviceIndex);
            if (K4A_FAILED(k4a_device_open(deviceIndex, &device)))
                throw std::runtime_error("Could not open camera");
            if (K4A_FAILED(k4a_device_start_cameras(device, config->getConfig())))
            {
                k4a_device_close(device);
                throw std::runtime_error("Could not start camera");
            }
            if (K4A_FAILED(k4a_device_set_color_control(device, K4A_COLOR_CONTROL_EXPOSURE_TIME_ABSOLUTE, K4A_COLOR_CONTROL_MODE_AUTO, 0)))
            {
                k4a_device_stop_cameras(device);
                k4a_device_close(device);
                throw std::runtime_error("Could set color control mode");
            }
            k4a_device_get_calibration(device, config->getDepthMode(), config->getColorResolution(), &calibration);
            transformation = k4a_transformation_create(&calibration);
            isCameraStarted = true;

            this->deviceIndex = deviceIndex;
        }
    }

    void AzureKinect::startRecording(const std::string &fileName, bool storeAdditionalInfo)
    {
        if (!isCameraStarted)
            throw std::runtime_error("Camera not started");

        if (K4A_FAILED(k4a_record_create((fileName + ".mkv").c_str(), device, *config->getConfig(), &record)))
            throw std::runtime_error("Unable to create recording file at " + fileName);

        if (storeAdditionalInfo) {
            additionalInfoFile.open(fileName + ".txt");
        }

        // Add a custom recording tag
        // if (K4A_FAILED(k4a_record_add_tag(record, "CUSTOM_TAG", "Hello, World!")))
        //    throw std::runtime_error("Unable to create recording file at " + fileName);

        // Write the recording header now that all the track metadata is set up.
        if (K4A_FAILED(k4a_record_write_header(record)))
            throw std::runtime_error("Could not write header");

        recording = true;
    }

    void AzureKinect::stopRecording()
    {
        writeRecordMutex.lock();
        if (recording) {
            recording = false;

            std::cout << "Saving recodings";

            if (additionalInfoFile.is_open()) {
                additionalInfoFile.close();
            }

            if (K4A_FAILED(k4a_record_flush(record)))
                throw std::runtime_error("Writing failed");
            k4a_record_close(record);

            std::cout << "Recording saved";
        }
        writeRecordMutex.unlock();
    }

    void AzureKinect::setManualColorControl(int32_t color_exposure_usec) {

        // If you want to synchronize cameras, you need to manually set their exposures
        k4a_device_set_color_control(device, K4A_COLOR_CONTROL_EXPOSURE_TIME_ABSOLUTE,
                                 K4A_COLOR_CONTROL_MODE_MANUAL,
                                 color_exposure_usec);
    }

    void AzureKinect::setManualColorControl(POWERLINE_FREQ powerline_freq) {
        // This setting compensates for the flicker of lights due to the frequency of AC power in your region.
        // (check the docs for k4a_color_control_command_t)
        if (powerline_freq == SIXTY_HZ)
            k4a_device_set_color_control(device, K4A_COLOR_CONTROL_POWERLINE_FREQUENCY,
                                 K4A_COLOR_CONTROL_MODE_MANUAL,
                                 2);
        else
            k4a_device_set_color_control(device, K4A_COLOR_CONTROL_POWERLINE_FREQUENCY,
                                 K4A_COLOR_CONTROL_MODE_MANUAL,
                                 1);
    }

    bool AzureKinect::checkSync() {
        bool sync_in_connected;
        bool sync_out_connected;
        k4a_device_get_sync_jack(device, &sync_in_connected, &sync_out_connected);
        if (config->getWiredSyncMode() == K4A_WIRED_SYNC_MODE_SUBORDINATE && !sync_in_connected) {
            std::cout << "No sync in connected but subordinate" << std::endl;
            return false;
        }
        else if (config->getWiredSyncMode() == K4A_WIRED_SYNC_MODE_MASTER && !sync_out_connected) {
            std::cout << "No sync out connected but master" << std::endl;
            return false;
        }
        else if (config->getWiredSyncMode() == K4A_WIRED_SYNC_MODE_STANDALONE && (sync_in_connected || sync_out_connected)) {
            std::cout << "Sync cable connected but standalone" << std::endl;
            return false;
        }
        return true;
    }

    AzureKinectConfigPtr AzureKinect::getConfig() {
        return config;
    }

    void AzureKinect::stopCamera()
    {
        if (isCameraStarted)
            k4a_device_stop_cameras(device);
    }

    void AzureKinect::saveRawCalibration(const std::filesystem::path &directory, const std::string &fileName) {
        size_t calibration_size = 0;
        k4a_buffer_result_t buffer_result = k4a_device_get_raw_calibration(device, NULL, &calibration_size);
        if (buffer_result == K4A_BUFFER_RESULT_TOO_SMALL)
        {
            std::vector<uint8_t> calibration_buffer = std::vector<uint8_t>(calibration_size);
            buffer_result = k4a_device_get_raw_calibration(device, calibration_buffer.data(), &calibration_size);
            if (buffer_result == K4A_BUFFER_RESULT_SUCCEEDED)
            {
                std::filesystem::path filePath = directory / (fileName + ".json");
                std::ofstream file(filePath, std::ofstream::binary);
                file.write(reinterpret_cast<const char *>(&calibration_buffer[0]), (long)calibration_size);
                file.close();
                std::cout << "Calibration blob for device " << (int)deviceIndex << " is saved to " << filePath;
            }
            else
            {
                throw std::runtime_error("Failed to get calibration blob");
            }
        }
        else
        {
            throw std::runtime_error("Failed to get calibration blob size");
        }
    }

    void AzureKinect::close()
    {
        stopRecording();
        stopCamera();
        if (device)
        {
            k4a_device_close(device);
        }
    }

    bool AzureKinect::getCapture(k4a_capture_t &capture) {
        if (!isCameraStarted)
            throw std::runtime_error("Camera not started");
        k4a_wait_result_t get_capture_result = k4a_device_get_capture(device, &capture, config->getTimeout());

        if (get_capture_result == K4A_WAIT_RESULT_SUCCEEDED)
        {
            /*auto timeNow = std::chrono::steady_clock::now();
            if (std::chrono::duration_cast<std::chrono::seconds>(timeNow - lastTest).count() > 30) {
                lastTest = std::chrono::steady_clock::now();
                auto temperature = k4a_capture_get_temperature_c(capture);
                if (temperature > 40)
                    std::cout << "Temperature of kinect at " << temperature << " °C" << std::endl;
            }*/
            return true;
        }
        else if (get_capture_result == K4A_WAIT_RESULT_TIMEOUT)
        {
            std::cout << "Did not get frame until timeout of " << 100;
            return false;
        }
        else
        {
            std::cout << "k4a_device_get_capture() returned error: " << get_capture_result;
            return false;
        }
    }

    void AzureKinect::writeCapture(k4a_capture_t capture) {
        writeRecordMutex.lock();
        if (recording)
        {
            if (K4A_FAILED(k4a_record_write_capture(record, capture)))
            {
                stopRecording();
                throw std::runtime_error("Writing capture failed");
            }
        }
        writeRecordMutex.unlock();
    }

    void AzureKinect::recordCapture(std::function<std::string()> additionalInfo) {
        k4a_capture_t capture;
        if (getCapture(capture)) {
            if (additionalInfoFile.is_open()) {
                auto info = additionalInfo();
                auto color_image = k4a_capture_get_color_image(capture);
                auto timestamp_color_device = k4a_image_get_device_timestamp_usec(color_image);
                auto timestamp_color_system = k4a_image_get_system_timestamp_nsec(color_image);
                k4a_image_release(color_image);
                additionalInfoFile << std::to_string(timestamp_color_device) << ", " << std::to_string(timestamp_color_system) << ", ";
                auto ir_image = k4a_capture_get_ir_image(capture);
                auto timestamp_ir_device = k4a_image_get_device_timestamp_usec(ir_image);
                auto timestamp_ir_system = k4a_image_get_system_timestamp_nsec(ir_image);
                k4a_image_release(ir_image);
                additionalInfoFile << std::to_string(timestamp_ir_device) << ", " << std::to_string(timestamp_ir_system) << ", ";

                additionalInfoFile << (info.empty() ? "NO FURTHER INFORMATION!" : info) << "\n";
            }
            writeCapture(capture);
            k4a_capture_release(capture);
        }
    }

    void AzureKinect::runRecording(std::function<std::string()> additionalInfo) {
        while (recording) {
            recordCapture(additionalInfo);
        }
    }

}



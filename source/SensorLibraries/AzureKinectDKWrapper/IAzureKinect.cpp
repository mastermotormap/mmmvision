#include "IAzureKinect.h"

#include <k4a/k4a.h>
#include <opencv2/opencv.hpp>

namespace MMMVision
{

IAzureKinect::IAzureKinect() :
    transformation(NULL),
    binColorImage(false)
{
}

IAzureKinect::~IAzureKinect()
{
    if (transformation != NULL)
        k4a_transformation_destroy(transformation);
}

k4a_image_t IAzureKinect::getColorImageFromCapture(const k4a_capture_t &capture) {
    if(!capture)
        throw std::runtime_error("No capture");

    k4a_image_t colorImage = k4a_capture_get_color_image(capture);
    if (colorImage == 0)
        throw std::runtime_error("Failed to get color image from capture");
    if (binColorImage)
        colorImage = downscale_color_image_2x2_binning(colorImage);
    return colorImage;
}

k4a_image_t IAzureKinect::getDepthImageFromCapture(const k4a_capture_t &capture) {
    if(!capture)
        throw std::runtime_error("No capture");

    k4a_image_t depthImage = k4a_capture_get_depth_image(capture);
    if (depthImage == 0)
        throw std::runtime_error("Failed to get depth image from capture");
    return depthImage;
}

k4a_image_t IAzureKinect::getIRImageFromCapture(const k4a_capture_t &capture) {
    if(!capture)
        throw std::runtime_error("No capture");

    k4a_image_t depthImage = k4a_capture_get_ir_image(capture);
    if (depthImage == 0)
        throw std::runtime_error("Failed to get depth image from capture");
    return depthImage;
}

k4a_capture_t IAzureKinect::getImages(k4a_image_t &colorImage, k4a_image_t &depthImage, bool transformToColor) {
    k4a_capture_t capture;
    if (!getCapture(capture))
        throw std::runtime_error("No capture");

    this->getImagesFromCapture(capture, colorImage, depthImage, transformToColor);

    return capture;
}

k4a_capture_t IAzureKinect::getImages(k4a_image_t &colorImage, k4a_image_t &depthImage, k4a_image_t &irImage, bool transformToColor) {
    k4a_capture_t capture;
    if (!getCapture(capture))
        throw std::runtime_error("No capture");

    this->getImagesFromCapture(capture, colorImage, depthImage, irImage, transformToColor);

    return capture;
}

bool IAzureKinect::getImagesFromCapture(const k4a_capture_t &capture, k4a_image_t &colorImage, k4a_image_t &depthImage, bool transformToColor) {
    colorImage = getColorImageFromCapture(capture);
    k4a_image_t d = getDepthImageFromCapture(capture);
    if (transformToColor) {
        if (!this->transformToColor(colorImage, d, depthImage))
            throw std::runtime_error("Failed transformation to color resolution!");
        k4a_image_release(d);
    }
    else {
        depthImage = d;
    }
    return true;
}

bool IAzureKinect::getImagesFromCapture(const k4a_capture_t &capture, k4a_image_t &colorImage, k4a_image_t &depthImage, k4a_image_t &irImage, bool transformToColor) {
    colorImage = getColorImageFromCapture(capture);
    k4a_image_t d = getDepthImageFromCapture(capture);
    k4a_image_t ir = getIRImageFromCapture(capture);
    if (transformToColor) {
        if (!this->transformToColor(colorImage, d, ir, depthImage, irImage))
            throw std::runtime_error("Failed transformation to color resolution!");
    }
    else {
        depthImage = d;
        irImage = ir;
    }
    return true;
}

bool IAzureKinect::transformToColor(const k4a_image_t &colorImage, const k4a_image_t &depthImage, k4a_image_t &transformedDepthImage) {
    if (k4a_image_get_format(depthImage) != K4A_IMAGE_FORMAT_DEPTH16)
        return false;

    int width = k4a_image_get_width_pixels(colorImage);
    int height = k4a_image_get_height_pixels(colorImage);

    if (K4A_FAILED(k4a_image_create(k4a_image_get_format(depthImage), width, height,
                                    width * (k4a_image_get_stride_bytes(depthImage) / k4a_image_get_width_pixels(depthImage)), &transformedDepthImage)))
        return false;
    if (K4A_FAILED(k4a_transformation_depth_image_to_color_camera(transformation, depthImage, transformedDepthImage)))
        return false;

    return true;
}

bool IAzureKinect::transformToColor(const k4a_image_t &colorImage, const k4a_image_t &depthImage, const k4a_image_t &irImage,
                      k4a_image_t &transformedDepthImage, k4a_image_t &transformedIRImage) {
    if (k4a_image_get_format(depthImage) != K4A_IMAGE_FORMAT_DEPTH16 || k4a_image_get_format(irImage) != K4A_IMAGE_FORMAT_IR16)
        return false;

    int width = k4a_image_get_width_pixels(colorImage);
    int height = k4a_image_get_height_pixels(colorImage);

    if (K4A_FAILED(k4a_image_create(k4a_image_get_format(depthImage), width, height,
                                    width * (k4a_image_get_stride_bytes(depthImage) / k4a_image_get_width_pixels(depthImage)), &transformedDepthImage)))
        return false;

    // Create custom IR image because image of type Custom16 is required for transformation!
    k4a_image_t custom_ir_image = NULL;
    int ir_image_width_pixels = k4a_image_get_width_pixels(irImage);
    int ir_image_height_pixels = k4a_image_get_height_pixels(irImage);
    int ir_image_stride_bytes = k4a_image_get_stride_bytes(irImage);
    uint8_t *ir_image_buffer = k4a_image_get_buffer(irImage);
    if (K4A_FAILED(k4a_image_create_from_buffer(K4A_IMAGE_FORMAT_CUSTOM16,
                                             ir_image_width_pixels,
                                             ir_image_height_pixels,
                                             ir_image_width_pixels * (int)sizeof(uint16_t),
                                             ir_image_buffer,
                                             ir_image_height_pixels * ir_image_stride_bytes,
                                             [](void *_buffer, void *context) {
                                                delete[](uint8_t *) _buffer;
                                                (void)context;
                                             },
                                             NULL,
                                             &custom_ir_image))) {
        return false;
    }

    if (K4A_FAILED(k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM16, width, height,
                                    width * (k4a_image_get_stride_bytes(custom_ir_image) / k4a_image_get_width_pixels(custom_ir_image)),
                                    &transformedIRImage)))
        return false;
    if (K4A_FAILED(k4a_transformation_depth_image_to_color_camera_custom(transformation, depthImage, custom_ir_image, transformedDepthImage, transformedIRImage, K4A_TRANSFORMATION_INTERPOLATION_TYPE_NEAREST, 0)))
        return false;
    k4a_image_release(custom_ir_image);

    return true;
}

void IAzureKinect::set2x2BinColorImage(bool bin) {
    if (bin)
    {
        k4a_calibration_t calibration_downscaled;
        memcpy(&calibration_downscaled, &calibration, sizeof(k4a_calibration_t));
        calibration_downscaled.color_camera_calibration.resolution_width /= 2;
        calibration_downscaled.color_camera_calibration.resolution_height /= 2;
        calibration_downscaled.color_camera_calibration.intrinsics.parameters.param.cx /= 2;
        calibration_downscaled.color_camera_calibration.intrinsics.parameters.param.cy /= 2;
        calibration_downscaled.color_camera_calibration.intrinsics.parameters.param.fx /= 2;
        calibration_downscaled.color_camera_calibration.intrinsics.parameters.param.fy /= 2;
        transformation = k4a_transformation_create(&calibration_downscaled);
        binColorImage = true;
    }
    else
    {
        transformation = k4a_transformation_create(&calibration);
        binColorImage = false;
    }
}

cv::Mat IAzureKinect::normalize(const cv::Mat &image, const std::pair<int, int> &range) {
    cv::Mat scaledImage;
    int min = range.first;
    int max = range.second;
    image.convertTo(scaledImage,CV_8U, 255.0 / (max-min), 255.0 * min / (min-max));
    return scaledImage;
}

cv::Mat IAzureKinect::transform(k4a_image_t &k4a_image) {
    cv::Mat image;
    int width = k4a_image_get_width_pixels(k4a_image);
    int height = k4a_image_get_height_pixels(k4a_image);
    uint8_t* buffer = k4a_image_get_buffer(k4a_image);

    switch(k4a_image_get_format(k4a_image)) {
    case K4A_IMAGE_FORMAT_COLOR_BGRA32: {
        image = cv::Mat(height, width, CV_8UC4, buffer);
        //cv::cvtColor(img2, image, cv::COLOR_BGRA2RGB); // seems like no conversion is required
        break;
    }
    case K4A_IMAGE_FORMAT_IR16: {
        image = cv::Mat(height, width, CV_16UC1, buffer);
        break;
    }
    case K4A_IMAGE_FORMAT_DEPTH16: {
        image = cv::Mat(height, width, CV_16UC1, buffer);
        break;
    }
    case K4A_IMAGE_FORMAT_CUSTOM8: {
        image = cv::Mat(height, width, CV_8UC1, buffer);
        break;
    }
    case K4A_IMAGE_FORMAT_CUSTOM16: {
        image = cv::Mat(height, width, CV_16UC1, buffer);
        break;
    }
    default:
        throw std::runtime_error("Unhandled image format!");
    }
    return image;
}

bool IAzureKinect::getImages(cv::Mat &colorImage, cv::Mat &depthImage, bool transformToColor, bool normalize) {
    k4a_capture_t capture;
    if (!getCapture(capture))
        return false;
    bool value = getImagesFromCapture(capture, colorImage, depthImage, transformToColor, normalize);
    k4a_capture_release(capture);
    return value;
}

bool IAzureKinect::getImages(cv::Mat &colorImage, cv::Mat &depthImage, cv::Mat &irImage, bool transformToColor, bool normalize) {
    k4a_capture_t capture;
    if (!getCapture(capture))
        return false;
    bool value = getImagesFromCapture(capture, colorImage, depthImage, irImage, transformToColor, normalize);
    k4a_capture_release(capture);
    return value;
}

bool IAzureKinect::getImagesFromCapture(const k4a_capture_t &capture, cv::Mat &colorImage, cv::Mat &depthImage, bool transformToColor, bool normalize) {
    k4a_image_t k4aColorImage, k4aDepthImage;
    if (!getImagesFromCapture(capture, k4aColorImage, k4aDepthImage, transformToColor)) return false;
    colorImage = transform(k4aColorImage).clone(); // Clone mat to avoid destroying buffer after releasing k4aImage
    depthImage = transform(k4aDepthImage).clone();
    if (normalize)
        depthImage = this->normalize(depthImage, getConfig()->GetDepthModeRange());
    k4a_image_release(k4aColorImage);
    k4a_image_release(k4aDepthImage);
    return true;
}

bool IAzureKinect::getImagesFromCapture(const k4a_capture_t &capture, cv::Mat &colorImage, cv::Mat &depthImage, cv::Mat &irImage, bool transformToColor, bool normalize) {
    k4a_image_t k4aColorImage, k4aDepthImage, k4aIRImage;
    if (!getImagesFromCapture(capture, k4aColorImage, k4aDepthImage, k4aIRImage, transformToColor)) return false;
    colorImage = transform(k4aColorImage).clone(); // Clone mat to avoid destroying buffer after releasing k4aImage
    depthImage = transform(k4aDepthImage).clone();
    irImage = transform(k4aIRImage).clone();
    if (normalize) {
        depthImage = this->normalize(depthImage, getConfig()->GetDepthModeRange());
        irImage = this->normalize(irImage, getConfig()->GetIrLevels());
    }
    k4a_image_release(k4aColorImage);
    k4a_image_release(k4aDepthImage);
    k4a_image_release(k4aIRImage);
    return true;
}

pcl::PointCloud<pcl::PointXYZRGBA>::Ptr IAzureKinect::getRGBAPointCloud(int maximumDistanceMM) {
    k4a_capture_t capture;
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud = nullptr;
    if (getCapture(capture)) {
        pointCloud = getRGBAPointCloudFromCapture(capture, maximumDistanceMM);
        k4a_capture_release(capture);
    }
    return pointCloud;
}

pcl::PointCloud<pcl::PointXYZRGBA>::Ptr IAzureKinect::getRGBAPointCloudFromCapture(const k4a_capture_t &capture, int maximumDistanceMM) {
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>());
    k4a_image_t colorImage;
    k4a_image_t depthImage;
    if (!getImagesFromCapture(capture, colorImage, depthImage, true))
        return nullptr;

    k4a_image_t xyzImage;
    unsigned int width = k4a_image_get_width_pixels(colorImage);
    unsigned int height = k4a_image_get_height_pixels(colorImage);
    if (K4A_FAILED(k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM, width, height, width * 3 * (int)sizeof(int16_t), &xyzImage)))
        return nullptr;
    if (K4A_FAILED(k4a_transformation_depth_image_to_point_cloud(transformation, depthImage, ::K4A_CALIBRATION_TYPE_COLOR, xyzImage)))
        return nullptr;
    cloud->width = width;
    cloud->height = height;

    /*k4a_image_t xy_table = NULL;
    unsigned int width = calibration.depth_camera_calibration.resolution_width;
    unsigned int height = calibration.depth_camera_calibration.resolution_height;
    if (K4A_FAILED(k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM, width, height, width * (int)sizeof(k4a_float2_t), &xy_table)))
        return nullptr;
    cloud->width = width;
    cloud->height = height;

    k4a_image_t point_cloud = NULL;

    create_xy_table(&calibration, xy_table);
    int point_count = 0;

    if (k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM, width, height, width * (int)sizeof(k4a_float3_t), &point_cloud)) return nullptr;

    generate_point_cloud(depthImage, xy_table, point_cloud, &point_count);*/

    cloud->is_dense = false;
    cloud->points.resize(cloud->height * cloud->width);
    int16_t *point_cloud_image_data = (int16_t *)(void *)k4a_image_get_buffer(xyzImage);
    uint8_t *color_image_data = k4a_image_get_buffer(colorImage);
#ifdef VTK_VISUALIZATION
    Eigen::Matrix3f m;
    m = Eigen::AngleAxisf(-M_PI, Eigen::Vector3f::UnitZ());
#endif
    for (unsigned int i = 0; i < cloud->width * cloud->height; ++i){
        pcl::PointXYZRGBA point;
        point.x = point_cloud_image_data[3 * i + 0];
        point.y = point_cloud_image_data[3 * i + 1];
        point.z = point_cloud_image_data[3 * i + 2];
        if (point.z == 0 || (maximumDistanceMM > 0 && point.z > maximumDistanceMM)) {
            continue;
        }
#ifdef VTK_VISUALIZATION
        point.getVector3fMap() = m * point.getVector3fMap();
#endif
        point.b = color_image_data[4 * i + 0];
        point.g = color_image_data[4 * i + 1];
        point.r = color_image_data[4 * i + 2];
        point.a = color_image_data[4 * i + 3];
        if (point.b == 0 && point.g == 0 && point.r == 0 && point.a == 0){
            continue;
        }
        cloud->points[i] = point;
    }

    k4a_image_release(colorImage);
    k4a_image_release(depthImage);
    k4a_image_release(xyzImage);
    return cloud;
}

Eigen::Vector3f IAzureKinect::transform2dTo3d(float x, float y, float depthMM) {
    k4a_float2_t point;
    point.xy.x = x;
    point.xy.y = y;
    k4a_float3_t point3D;
    int valid;
    k4a_calibration_2d_to_3d(&calibration, &point, depthMM, K4A_CALIBRATION_TYPE_COLOR, K4A_CALIBRATION_TYPE_COLOR, &point3D, &valid);
    return Eigen::Vector3f(point3D.xyz.x, point3D.xyz.y, point3D.xyz.z);
}

Eigen::Matrix3f IAzureKinect::getIntrinsicParametersColorEigen() {
    k4a_calibration_intrinsic_parameters_t *intrinsics = &calibration.color_camera_calibration.intrinsics.parameters;
    Eigen::Matrix3f intrinsics_eigen;
    intrinsics_eigen <<
            intrinsics->param.fx, 0.0f, intrinsics->param.cx,
            0.0f, intrinsics->param.fy, intrinsics->param.cy,
            0.0f, 0.0f, 1.0f;
    return intrinsics_eigen;
}

k4a_calibration_t IAzureKinect::getCalibration() {
    return calibration;
}

k4a_transformation_t IAzureKinect::getTransformation() {
    return transformation;
}

k4a_image_t IAzureKinect::downscale_color_image_2x2_binning(const k4a_image_t color_image)
{
    if (k4a_image_get_format(color_image) != K4A_IMAGE_FORMAT_COLOR_BGRA32)
        throw std::runtime_error("Invalid image format");
    int color_image_width_pixels = k4a_image_get_width_pixels(color_image);
    int color_image_height_pixels = k4a_image_get_height_pixels(color_image);
    int color_image_downscaled_width_pixels = color_image_width_pixels / 2;
    int color_image_downscaled_height_pixels = color_image_height_pixels / 2;
    k4a_image_t color_image_downscaled = NULL;
    if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_COLOR_BGRA32, color_image_downscaled_width_pixels,
                                                 color_image_downscaled_height_pixels,
                                                 color_image_downscaled_width_pixels * 4 * (int)sizeof(uint8_t),
                                                 &color_image_downscaled))
    {
        printf("Failed to create downscaled color image\n");
        return color_image_downscaled;
    }

    uint8_t *color_image_data = k4a_image_get_buffer(color_image);
    uint8_t *color_image_downscaled_data = k4a_image_get_buffer(color_image_downscaled);
    for (int j = 0; j < color_image_downscaled_height_pixels; j++)
    {
        for (int i = 0; i < color_image_downscaled_width_pixels; i++)
        {
            int index_downscaled = j * color_image_downscaled_width_pixels + i;
            int index_tl = (j * 2 + 0) * color_image_width_pixels + i * 2 + 0;
            int index_tr = (j * 2 + 0) * color_image_width_pixels + i * 2 + 1;
            int index_bl = (j * 2 + 1) * color_image_width_pixels + i * 2 + 0;
            int index_br = (j * 2 + 1) * color_image_width_pixels + i * 2 + 1;

            color_image_downscaled_data[4 * index_downscaled + 0] = (uint8_t)(
                (color_image_data[4 * index_tl + 0] + color_image_data[4 * index_tr + 0] +
                 color_image_data[4 * index_bl + 0] + color_image_data[4 * index_br + 0]) /
                4.0f);
            color_image_downscaled_data[4 * index_downscaled + 1] = (uint8_t)(
                (color_image_data[4 * index_tl + 1] + color_image_data[4 * index_tr + 1] +
                 color_image_data[4 * index_bl + 1] + color_image_data[4 * index_br + 1]) /
                4.0f);
            color_image_downscaled_data[4 * index_downscaled + 2] = (uint8_t)(
                (color_image_data[4 * index_tl + 2] + color_image_data[4 * index_tr + 2] +
                 color_image_data[4 * index_bl + 2] + color_image_data[4 * index_br + 2]) /
                4.0f);
            color_image_downscaled_data[4 * index_downscaled + 3] = (uint8_t)(
                (color_image_data[4 * index_tl + 3] + color_image_data[4 * index_tr + 3] +
                 color_image_data[4 * index_bl + 3] + color_image_data[4 * index_br + 3]) /
                4.0f);
        }
    }

    return color_image_downscaled;
}

}

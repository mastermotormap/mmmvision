#ifndef AZUREKINECTUTIL_H
#define AZUREKINECTUTIL_H

#include <k4a/k4atypes.h>

namespace armarx
{
    void convertMJPEGtoRGBA(k4a_image_t colorImage);
}


#endif // AZUREKINECTUTIL_H

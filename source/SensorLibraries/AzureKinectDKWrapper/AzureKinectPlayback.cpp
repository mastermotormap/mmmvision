#include "AzureKinectPlayback.h"

#include <sstream>
#include <k4a/k4a.h>
#include <k4a/k4a.hpp>
#include <k4arecord/record.h>
#include <k4arecord/playback.h>
#include <k4arecord/playback.hpp>
#include <memory>
#include <mutex>
#include <atomic>
#include <filesystem>

namespace MMMVision
{

AzureKinectPlayback::AzureKinectPlayback(const std::filesystem::path &path, int offsetTimestampMS) :
    IAzureKinect()
{
    if (K4A_FAILED(k4a_playback_open(path.c_str(), &playback)))
        throw std::runtime_error("Opening playback failed");

    if (K4A_FAILED(k4a_playback_set_color_conversion(playback, K4A_IMAGE_FORMAT_COLOR_BGRA32)))
        throw std::runtime_error("Failed setting image format conversion to bgra32");

    if (K4A_FAILED(k4a_playback_get_calibration(playback, &calibration)))
        throw std::runtime_error("Get calibration from playback failed");
    transformation = k4a_transformation_create(&calibration);

    k4a_record_configuration_t record_config;
    if (K4A_FAILED(k4a_playback_get_record_configuration(playback, &record_config)))
        throw std::runtime_error("Config not readable");
    config = AzureKinectConfigPtr(new AzureKinectConfig(record_config));

    startTimestepInSeconds = record_config.start_timestamp_offset_usec / 1000000.0f;

    jumpToMS(offsetTimestampMS);
}

AzureKinectPlayback::AzureKinectPlayback(const std::filesystem::path &path, k4a_image_format_t imageFormat, int offsetTimestampMS)
    : AzureKinectPlayback(path, offsetTimestampMS)
{
    setImageFormatConversion(imageFormat);
}

AzureKinectPlayback::~AzureKinectPlayback() {
    k4a_playback_close(playback);
}

void AzureKinectPlayback::setImageFormatConversion(k4a_image_format_t imageFormat) {
    if (K4A_FAILED(k4a_playback_set_color_conversion(playback, imageFormat)))
        throw std::runtime_error("Could not set image format");
    config->setColorFormat(imageFormat);
}

void AzureKinectPlayback::jumpToMS(int offsetTimestampMS) {
    if (K4A_FAILED(k4a_playback_seek_timestamp(playback, offsetTimestampMS * 1000, K4A_PLAYBACK_SEEK_BEGIN)))
        throw std::runtime_error("Seek timestamp failed");
}

void AzureKinectPlayback::jumpTo(float timestep, float offset) {
    int offsetTimestampMS = (timestep - offset) * 1000;
    jumpToMS(offsetTimestampMS);
}

bool AzureKinectPlayback::getCapture(k4a_capture_t &capture) {
    return getNextCapture(capture);
}

void AzureKinectPlayback::exportMKVFile(cv::Mat colorImage, cv::Mat depthImage, cv::Mat irImage, const std::filesystem::path &fileWithoutExtension) {

    k4a_device_t device = NULL;

    k4a_record_t record; // needed as first input for record_write_capture, is the handle of the new recording

    std::string filePath = std::string(fileWithoutExtension) + ".mkv";
    if (K4A_FAILED(k4a_record_create(filePath.c_str(), device, *config->getConfig(), &record)))
        throw std::runtime_error("Unable to create recording file at " + filePath);

    if (K4A_FAILED(k4a_record_write_header(record)))
        throw std::runtime_error("Could not write header");

    k4a_capture_t capture; // needed as first input for record_write_capture
    if (K4A_FAILED(k4a_capture_create(&capture)))
        throw std::runtime_error("Could not create empty capture object");
    k4a_image_t k4aColorImage, k4aDepthImage, k4aIrImage;

    k4a_image_create_from_buffer(K4A_IMAGE_FORMAT_COLOR_BGRA32, colorImage.cols, colorImage.rows, colorImage.step, colorImage.data, colorImage.total() * colorImage.elemSize(), NULL, NULL, &k4aColorImage);
    k4a_image_create_from_buffer(K4A_IMAGE_FORMAT_DEPTH16, depthImage.cols, depthImage.rows, depthImage.step, depthImage.data, depthImage.total() * depthImage.elemSize(), NULL, NULL, &k4aDepthImage);
    k4a_image_create_from_buffer(K4A_IMAGE_FORMAT_IR16, irImage.cols, irImage.rows, irImage.step, irImage.data, irImage.total() * irImage.elemSize(), NULL, NULL, &k4aIrImage);

    k4a_image_create_from_buffer(K4A_IMAGE_FORMAT_COLOR_BGRA32, colorImage.cols, colorImage.rows, colorImage.step, colorImage.data, colorImage.total() * colorImage.elemSize(), NULL, NULL, &k4aColorImage);


    k4a_image_set_device_timestamp_usec(k4aColorImage, 0);
    k4a_image_set_device_timestamp_usec(k4aDepthImage, 0);
    k4a_image_set_device_timestamp_usec(k4aIrImage, 0);

    k4a_capture_set_color_image(capture, k4aColorImage);
    k4a_capture_set_depth_image(capture, k4aDepthImage);
    k4a_capture_set_ir_image(capture, k4aIrImage);
    k4a_record_write_capture(record, capture);


    k4a_capture_release(capture);

    k4a_record_close(record);
}

bool AzureKinectPlayback::getPreviousCapture(k4a_capture_t &capture) {
    auto result = k4a_playback_get_previous_capture(playback, &capture);
    if (result == K4A_STREAM_RESULT_SUCCEEDED) {
        if (capture == NULL)
            throw std::runtime_error("Failed to fetch frame");
        return true;
    }
    else if (result == K4A_STREAM_RESULT_EOF)
        return false;
    else
        throw std::runtime_error("Get previous capture failed");
}

bool AzureKinectPlayback::getNextCapture(k4a_capture_t &capture) {
    auto result = k4a_playback_get_next_capture(playback, &capture);
    if (result == K4A_STREAM_RESULT_SUCCEEDED) {
        if (capture == NULL)
            throw std::runtime_error("Failed to fetch frame");
        return true;
    }
    else if (result == K4A_STREAM_RESULT_EOF)
        return false;
    else
        throw std::runtime_error("Get next capture failed");
}

std::vector<float> AzureKinectPlayback::getTimesteps(float offset) {
    std::vector<float> timesteps;
    jumpToMS(0);
    k4a_capture_t capture;
    while(getNextCapture(capture)) {
        timesteps.push_back(getTimestep(capture, offset));
        k4a_capture_release(capture);
    }
    return timesteps;
}

AzureKinectConfigPtr AzureKinectPlayback::getConfig() {
    return config;
}

float AzureKinectPlayback::getStartTimestep() const {
    return startTimestepInSeconds;
}

float AzureKinectPlayback::getEndTimestep() const {
    return getStartTimestep() + getRecordingLength();
}

float AzureKinectPlayback::getRecordingLength() const {
    long recordingLength = k4a_playback_get_recording_length_usec(playback);
    return recordingLength / 1000000.0f;
}

float AzureKinectPlayback::getTimestep(const k4a_capture_t &capture, float offset) {
    k4a_image_t colorImage = getColorImageFromCapture(capture);
    float timestampInS = k4a_image_get_device_timestamp_usec(colorImage) / 1000000.0f;
    auto value = timestampInS - getStartTimestep() + offset;
    k4a_image_release(colorImage);
    return value;
}

}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <opencv/cv.h>
#include <map>

namespace MMMVision
{

struct Tools {
    static float linearInterpolation(float value1, float v1_timestep, float value2, float v2_timestep, float timestep) {
        return value1 + (value2 - value1) / (v2_timestep - v1_timestep) * (timestep - v1_timestep);
    }

    static cv::Rect2f linearInterpolation(cv::Rect2f  value1, float v1_timestep, cv::Rect2f  value2, float v2_timestep, float timestep) {
        float x = linearInterpolation(value1.x, v1_timestep, value2.x, v2_timestep, timestep);
        float y = linearInterpolation(value1.y, v1_timestep, value2.y, v2_timestep, timestep);
        float w = linearInterpolation(value1.x + value1.width, v1_timestep, value2.x + value2.width, v2_timestep, timestep) - x;
        float h = linearInterpolation(value1.y + value1.height, v1_timestep, value2.y + value2.height, v2_timestep, timestep) - y;
        return cv::Rect2f(x, y, w, h);
    }

    static std::map<int, std::map<float, cv::Rect2f>> groupAndExtendBoundingBoxes(const std::map<float, std::vector<cv::Rect2f>> &boundingBoxesByTime, const std::vector<float> &timesteps, float minDuration = 0.3f, float extrapolation = 0.1f,
                                     int maxDistance = 150) {
        int personID = 0;
        std::map<int, std::map<float, cv::Rect2f>> roisAfterPerson;
        for (const auto &byTime : boundingBoxesByTime) {
            float timestep = byTime.first;
            for (const cv::Rect2f &roi : byTime.second) {
                bool added = false;
                for (auto &r3 : roisAfterPerson) {
                    auto lastRect = r3.second.rbegin();
                    if (timestep - lastRect->first > 0.00001 && timestep - lastRect->first < minDuration) {
                        if (std::abs(roi.x - lastRect->second.x) < maxDistance &&
                                std::abs(roi.y - lastRect->second.y) < maxDistance &&
                                std::abs((roi.x + roi.width) - (lastRect->second.x + lastRect->second.width)) < maxDistance &&
                                std::abs((roi.y + roi.height) - (lastRect->second.y + lastRect->second.height)) < maxDistance) {
                            added = true;
                            r3.second[timestep] = roi;
                            break;
                        }
                    }
                }
                if (!added) {
                    roisAfterPerson[personID++][timestep] = roi;
                }
            }
        }

        for (auto it = roisAfterPerson.begin(); it != roisAfterPerson.end();) {
            float startTimestep = it->second.begin()->first;
            float endTimestep = it->second.rbegin()->first;
            if ((endTimestep > minDuration + timesteps.front() && startTimestep < timesteps.back() - minDuration && endTimestep - startTimestep < minDuration) || it->second.size() < 4) {
                it = roisAfterPerson.erase(it);
            }
            else {
                for (float timestep : timesteps) {
                    if (timestep > startTimestep - extrapolation && timestep < endTimestep + extrapolation) {
                        if (it->second.find(timestep) == it->second.end()) {
                            auto it1 = it->second.lower_bound(timestep);
                            auto it2 = it1;
                            if (it1 != it->second.begin()) {
                                it1 = std::prev(it1);
                                it->second[timestep] = linearInterpolation(it1->second, it1->first, it2->second, it2->first, timestep);
                            }
                            else if (it1 == it->second.end()) {
                                it2 = std::prev(it->second.end());
                                it->second[timestep] = it2->second; // use last one as extrapolation doesnt work
                            }
                            else {
                                it->second[timestep] = it1->second; // use first one as extrapolation doesnt work
                            }
                        }
                    }
                }
                it++;
            }
        }
        return roisAfterPerson;
    }

    static bool anonymize(const cv::Mat &frame, cv::Rect2f roi, int padding, int gaussKernel, cv::Rect2i region, bool logging = false);
};

}

#include "OpenPoseTools.h"

#include <openpose/headers.hpp>
#include <openpose/core/matrix.hpp>
#include <opencv/cv.h>
// Command-line user interface
#define OPENPOSE_FLAGS_DISABLE_PRODUCER
#define OPENPOSE_FLAGS_DISABLE_DISPLAY
#include <openpose/flags.hpp>
// OpenPose dependencies
#include <openpose/headers.hpp>
#include <openpose/utilities/keypoint.hpp>

namespace MMMVision
{

void OpenPoseWrapper::configure(op::Wrapper &opWrapper, op::Point<int> netInputSize, const std::string &modelFolder) {
    op::WrapperStructPose wrapperStructPose;
    wrapperStructPose.netInputSize = netInputSize;
    wrapperStructPose.modelFolder = op::String(modelFolder);
    opWrapper.configure(wrapperStructPose);

    // Set to single-thread (for sequential processing and/or debugging and/or reducing latency)
    if (FLAGS_disable_multi_thread)
        opWrapper.disableMultiThreading();
}

void OpenPoseWrapper::extractKeyPoints(op::Wrapper &opWrapper, const cv::Mat &frame, std::map<int, OpenPoseKeyPoint> &keyPoints, std::vector<cv::Rect2f> &rois, float threshold) {
    op::Matrix imageToProcess = OP_CV2OPCONSTMAT(frame);

    auto datumsPtr = opWrapper.emplaceAndPop(imageToProcess);
    if (datumsPtr != nullptr && !datumsPtr->empty())
    {

        for (auto s : *datumsPtr) {
            const auto keypoints = s->poseKeypoints;
            int biggestPerson = getBiggestPerson(keypoints, threshold); // only biggest person is relevant
            for (auto person = 0; person < keypoints.getSize(0); person++) {
                if (person == biggestPerson) {
                    const auto keypoints_person = op::getKeypointsPerson(keypoints, person);
                    for (auto bodyPart = 0; bodyPart < keypoints_person.getSize(1); bodyPart++) {
                        OpenPoseKeyPoint keyPoint;
                        keyPoint.x = keypoints_person[{0, bodyPart, 0}];
                        keyPoint.y = keypoints_person[{0, bodyPart, 1}];
                        keyPoint.score = keypoints_person[{0, bodyPart, 2}];
                        if (!keyPoint.isNull()) keyPoints[bodyPart] = keyPoint;
                    }
                }
                else {
                    const auto personRectangle = op::getKeypointsRectangle(keypoints, person, threshold);
                    if (personRectangle.area() > 0)
                    {
                        cv::Rect2f roi = cv::Rect2f(personRectangle.x, personRectangle.y, personRectangle.width, personRectangle.height);
                        rois.push_back(roi);
                    }
                }
            }
        }
    }
}

std::map<float, std::vector<cv::Rect2f> > OpenPoseWrapper::createBoundingBoxes(op::Wrapper &opWrapper, const std::map<float, cv::Mat> &frames, float treshold) {
    const auto opTimer = op::getTimerInit();

    // Starting OpenPose
    opWrapper.start();

    std::map<float, std::vector<cv::Rect2f>> rois;
    for (const auto &frame : frames) {
        op::Matrix imageToProcess = OP_CV2OPCONSTMAT(frame.second);

        auto datumsPtr = opWrapper.emplaceAndPop(imageToProcess);
        if (datumsPtr != nullptr && !datumsPtr->empty())
        {
            for (auto s : *datumsPtr) {
                const auto keypoints = s->poseKeypoints;
                const auto thresholdRectangle = treshold;
                for (auto person = 0 ; person < keypoints.getSize(0) ; person++)
                {
                    const auto personRectangle = op::getKeypointsRectangle(keypoints, person, thresholdRectangle);
                    if (personRectangle.area() > 0)
                    {
                        if (rois.find(frame.first) == rois.end()) rois[frame.first] = std::vector<cv::Rect2f>();
                        op::opLog("At " + std::to_string(frame.first) + " - bounding box: " + personRectangle.toString(), op::Priority::High);
                        cv::Rect2f roi = cv::Rect2f(personRectangle.x, personRectangle.y, personRectangle.width, personRectangle.height);
                        rois[frame.first].push_back(roi);
                    }
                }
            }
        }
    }

    // Measuring total time
    op::printTime(opTimer, "OpenPose successfully finished. Total time: ", " seconds.", op::Priority::High);

    return rois;
}

void OpenPoseWrapper::getModel(op::Wrapper &opWrapper, const cv::Mat& frame) {
    op::Matrix imageToProcess = OP_CV2OPCONSTMAT(frame);

    auto datumsPtr = opWrapper.emplaceAndPop(imageToProcess);
    if (datumsPtr != nullptr && !datumsPtr->empty())
    {
        for (auto s : *datumsPtr) {
             op::opLog("\nKeypoints:", op::Priority::High);
             const auto& poseKeypoints = datumsPtr->at(0)->poseKeypoints;
             op::opLog("Person pose keypoints:", op::Priority::High);
             for (auto person = 0 ; person < poseKeypoints.getSize(0) ; person++)
             {
                 op::opLog("Person " + std::to_string(person) + " (x, y, score):", op::Priority::High);
                 for (auto bodyPart = 0 ; bodyPart < poseKeypoints.getSize(1) ; bodyPart++)
                 {
                     std::string valueToPrint;
                     for (auto xyscore = 0 ; xyscore < poseKeypoints.getSize(2) ; xyscore++)
                         valueToPrint += std::to_string(   poseKeypoints[{person, bodyPart, xyscore}]   ) + " ";
                     op::opLog(valueToPrint, op::Priority::High);
                 }
             }
        }
    }
}

}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <opencv/cv.h>
#include <openpose/headers.hpp>

namespace MMMVision
{

struct OpenPoseWrapper
{

struct OpenPoseKeyPoint {
    float x;
    float y;
    float score;

    bool isNull() const {
        return score == 0.0f;
    }

    std::string toString() const {
        std::stringstream ss;
        ss << x << " " << y << " " << score;
        return ss.str();
    }
};

void configure(op::Wrapper &opWrapper, op::Point<int> netInputSize, const std::string &modelFolder = std::string(OPENPOSE_MODEL_FOLDER));

void extractKeyPoints(op::Wrapper &opWrapper, const cv::Mat &frame, std::map<int, OpenPoseKeyPoint> &keyPoints, std::vector<cv::Rect2f> &rois, float threshold = 0.3f);

std::map<float, std::vector<cv::Rect2f>> createBoundingBoxes(op::Wrapper &opWrapper, const std::map<float, cv::Mat> &frames, float treshold = 0.3f);

void getModel(op::Wrapper &opWrapper, const cv::Mat& frame);

};

}

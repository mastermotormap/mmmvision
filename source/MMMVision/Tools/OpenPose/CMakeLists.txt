set(TARGET_NAME "MMMVisionOpenPoseTools")

if(NOT(OpenPose_DIR) OR NOT(OpenCV_FOUND))
    message(STATUS "Required libraries not found for ${TARGET_NAME}")
    return()
endif()

set(LIBRARIES
    ${OpenCV_LIBS}
    ${OPENPOSE_LINK_LIBRARY}
    MMMVisionTools
    stdc++fs
    gflags
)

# These headers make up the public interface of the library
# They are installed via "make install" and should not include
# private headers which are only used for implementation.
set(PUBLIC_HEADERS
    OpenPoseTools.h
)

set(HEADERS
    ${PUBLIC_HEADERS}
)

set(SOURCES
    OpenPoseTools.cpp
)

add_library(${TARGET_NAME} SHARED ${SOURCES} ${HEADERS})
target_link_libraries(${TARGET_NAME} PUBLIC ${LIBRARIES})
set_target_properties(${TARGET_NAME} PROPERTIES PUBLIC_HEADER "${PUBLIC_HEADERS}")

target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${OpenCV_INCLUDE_DIR})
target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${OPENPOSE_INCLUDE_DIRS})

target_compile_definitions(${TARGET_NAME} PUBLIC -D_REENTRANT)
target_include_directories(${TARGET_NAME} PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>)
target_include_directories(${TARGET_NAME} PUBLIC $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>)

target_compile_definitions(${TARGET_NAME} PUBLIC -DOPENPOSE_MODEL_FOLDER="${OPENPOSE_MODEL_FOLDER}")

install(TARGETS ${TARGET_NAME}
    EXPORT ${TARGET_NAME}Targets
    RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
    LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
    PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}/${TARGET_NAME}"
    COMPONENT dev
)


/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <filesystem>
#include "opencv2/opencv.hpp"
#include <memory>
#include <functional>

namespace MMMVision
{

class Video
{
public:
    Video(const std::filesystem::path &filePath, float timeOffset = 0.0f);

    static std::filesystem::path getFilePathSuffix(const std::filesystem::path &filePath, const std::string &suffix) {
        return (filePath.parent_path() / filePath.stem()).c_str() + suffix + filePath.extension().c_str();
    }

    bool write(const std::filesystem::path &filePath);

    bool writeWithSuffix(const std::string &suffix);

    std::filesystem::path getFilePathSuffix(const std::string &suffix);

    std::filesystem::path getFilePath() const;

    int getCodec() const;

    double getFps() const;

    cv::Size getSize() const;

    int getWidth() const;

    int getHeight() const;

    std::string getResolutionStr();

    std::map<float, cv::Mat> getFrames() const;

    double getRecordingLength() const;

    bool anonymize(float timestep, cv::Rect2f roi, int padding, int gaussKernel, cv::Rect2i region, bool logging = true);

    bool apply(float timestep, std::function<bool(cv::Mat&)> imageManipulation);

    std::string getFileExtension();

    std::string getFilePathWithoutExtension();

    std::vector<float> getTimesteps();

private:
    std::filesystem::path filePath;
    int codec;
    double fps;
    cv::Size size;
    std::map<float, cv::Mat> frames;
    double recordingLength;
};

typedef std::shared_ptr<Video> VideoPtr;

}

#include "VideoTools.h"

#include "opencv2/opencv.hpp"

namespace MMMVision
{

bool Tools::anonymize(const cv::Mat &frame, cv::Rect2f roi, int padding, int gaussKernel, cv::Rect2i region, bool logging) {
    int x = roi.x - padding;
    if (x < region.x) x = region.x;
    int width = roi.width + 2*padding;
    if (x + width > region.width) width = region.width - x;
    if (width < 0) return false;
    int y = roi.y - padding;
    if (y < region.y) y = region.y;
    int height = roi.height + 2*padding;
    if (y + height > region.height) height = region.height - y;
    if (height < 0) return false;

    cv::Rect r = cv::Rect(x, y, width, height);
    if (r.width < padding + 1 || r.height < padding + 1) {
        std::cout << "NOT anonymizing as area is small from ROI (" << r.x << "," << r.y << ") (" << r.x + r.width << "," << r.y + r.height << ")" << std::endl;
        return false;
    }
    if (logging)
        std::cout << "Anonymizing ROI (" << r.x << "," << r.y << ") (" << r.x + r.width << "," << r.y + r.height << ")" << std::endl;
    cv::GaussianBlur(frame(r), frame(r), cv::Size(gaussKernel, gaussKernel), 0);
    return true;
}

}

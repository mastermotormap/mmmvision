#include "Video.h"

#include "opencv2/opencv.hpp"
#include "Errors.h"
#include "VideoTools.h"

namespace MMMVision
{

Video::Video(const std::filesystem::path &filePath, float timeOffset) :
    filePath(filePath)
{
    cv::VideoCapture cap(cv::String(filePath.c_str()));
    if(!cap.isOpened()){
        throw MMMVisionError("Error opening video file from " + std::string(filePath));
    }

    fps = cap.get(cv::CAP_PROP_FPS);
    codec = cap.get(cv::CAP_PROP_FOURCC);
    size = cv::Size(cap.get(cv::CAP_PROP_FRAME_WIDTH), cap.get(cv::CAP_PROP_FRAME_HEIGHT));
    auto startTimestep = cap.get(cv::CAP_PROP_POS_MSEC) / 1000.0f;

    float timestep = 0.0f;
    int failed = 0;
    while(true) {
        auto timestep = cap.get(cv::CAP_PROP_POS_MSEC) / 1000.0f - startTimestep + timeOffset;
        cv::Mat frame;
        if(!cap.read(frame)) {
            failed++;
        }
        if (!frame.empty()) {
            failed = 0;
            frames[timestep] = frame;
        }

        if (failed > 5)
            break;
    }

    recordingLength = timestep - timeOffset;

    cap.release();
}

bool Video::write(const std::filesystem::path &filePath) {
    std::cout << "Writing file to file path " << filePath << std::endl;
    cv::VideoWriter writer(cv::String(filePath.c_str()), codec, fps, size);

    if (writer.isOpened()) {
        for (const auto &frame : frames) {
            writer.write(frame.second);
        }
        return true;
    }
    else {
        std::cout << "Error while opening!" << std::endl;
        return false;
    }
}

bool Video::writeWithSuffix(const std::string &suffix) {
    return write(getFilePathSuffix(suffix));
}

std::filesystem::path Video::getFilePathSuffix(const std::string &suffix) {
    return getFilePathWithoutExtension() + suffix + getFileExtension();
}

std::filesystem::path Video::getFilePath() const
{
    return filePath;
}

int Video::getCodec() const
{
    return codec;
}

double Video::getFps() const
{
    return fps;
}

cv::Size Video::getSize() const
{
    return size;
}

int Video::getWidth() const {
    return size.width;
}

int Video::getHeight() const {
    return size.height;
}

std::string Video::getResolutionStr() {
    std::stringstream ss;
    ss << getWidth() << "x" << getHeight();
    return ss.str();
}

std::map<float, cv::Mat> Video::getFrames() const
{
    return frames;
}

double Video::getRecordingLength() const
{
    return recordingLength;
}

bool Video::anonymize(float timestep, cv::Rect2f roi, int padding, int gaussKernel, cv::Rect2i region, bool logging) {
    return apply(timestep, [=](cv::Mat &frame) {
        return Tools::anonymize(frame, roi, padding, gaussKernel, region, logging);
    });
}

bool Video::apply(float timestep, std::function<bool (cv::Mat &)> imageManipulation) {
    if (frames.find(timestep) == frames.end())
        return false;
    else return imageManipulation(frames[timestep]);
}

std::string Video::getFileExtension() {
    return filePath.extension();
}

std::string Video::getFilePathWithoutExtension() {
    return filePath.parent_path() / filePath.stem();
}

std::vector<float> Video::getTimesteps() {
    std::vector<float> timesteps;
    for (const auto &f : frames)
        timesteps.push_back(f.first);
    return timesteps;
}

}

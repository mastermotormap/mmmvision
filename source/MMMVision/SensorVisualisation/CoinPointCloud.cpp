#include "CoinPointCloud.h"

#include <vector>

//Coin includes
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/SbColor.h>
#include <Inventor/SbVec3f.h>

#include <pcl/common/colors.h>

namespace MMM
{

void fillColorArray(float array[3], const pcl::PointXYZRGBA& color)
{
    array[0] = static_cast<float>(color.r) / 255.0f;
    array[1] = static_cast<float>(color.g) / 255.0f;
    array[2] = static_cast<float>(color.b) / 255.0f;
}

CoinPointCloud::CoinPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr originalCloud, float pointSize) :
    originalCloud(originalCloud)
{
    // Insert color information into scene graph
    SoMaterial* materialInfo = new SoMaterial();
    std::vector<SbColor> colorData;
    colorData.reserve(originalCloud->points.size());
    // Add point coordinates
    SoCoordinate3* coordinates = new SoCoordinate3();
    std::vector<SbVec3f> pointData;
    pointData.reserve(originalCloud->points.size());
    for (const pcl::PointXYZRGBA& p : originalCloud->points)
    {
        if (std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))
        {
            SbColor colorContainer;
            float colorArray[3];

            fillColorArray(colorArray, p);

            colorContainer.setValue(colorArray);
            colorData.push_back(colorContainer);

            SbVec3f pointContainer;
            pointContainer[0] = p.x;
            pointContainer[1] = p.y;
            pointContainer[2] = p.z;

            pointData.push_back(pointContainer);
        }
    }

    materialInfo->diffuseColor.setValues(0, colorData.size(), colorData.data());
    this->addChild(materialInfo);

    // Bind materials to per part
    SoMaterialBinding* binding = new SoMaterialBinding();
    binding->value = SoMaterialBinding::PER_PART;
    this->addChild(binding);

    coordinates->point.setValues(0, pointData.size(), pointData.data());
    this->addChild(coordinates);

    // Set point size
    SoDrawStyle* sopointSize = new SoDrawStyle();
    sopointSize->pointSize = pointSize;
    this->addChild(sopointSize);

    // Draw a point set out of all that data
    SoPointSet* pointSet = new SoPointSet();
    this->addChild(pointSet);
}

CoinPointCloud::~CoinPointCloud() = default;

pcl::PointCloud<pcl::PointXYZRGBA>::Ptr CoinPointCloud::getPCLCloud() const
{
    return this->originalCloud;
}

}

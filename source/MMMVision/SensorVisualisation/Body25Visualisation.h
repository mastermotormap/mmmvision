/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <vector>
#include <tuple>

namespace MMM
{

namespace BODY25
{

std::vector<std::tuple<float, float, float>> openposeKeyPointColor = {
    std::make_tuple(1.0f, 0.0f, 0.2f),
    std::make_tuple(1.0f, 1.0f, 1.0f),
    std::make_tuple(1.0f, 0.4f, 0.0f),
    std::make_tuple(1.0f, 0.65f, 0.0f),
    std::make_tuple(1.0f, 1.0f, 0.0f),
    std::make_tuple(0.4f, 1.0f, 0.0f),
    std::make_tuple(0.2f, 1.0f, 0.0f),
    std::make_tuple(0.0f, 1.0f, 0.0f),
    std::make_tuple(1.0f, 0.0f, 0.0f),
    std::make_tuple(0.0f, 1.0f, 0.4f),
    std::make_tuple(0.0f, 1.0f, 0.65f),
    std::make_tuple(0.0f, 1.0f, 1.0f),
    std::make_tuple(0.0f, 0.65f, 1.0f),
    std::make_tuple(0.0f, 0.2f, 1.0f),
    std::make_tuple(0.0f, 0.0f, 1.0f),
    std::make_tuple(1.0f, 0.0f, 0.65f),
    std::make_tuple(0.65f, 0.0f, 1.0f),
    std::make_tuple(1.0f, 0.0f, 1.0f),
    std::make_tuple(0.4f, 0.0f, 1.0f),
    std::make_tuple(0.0f, 0.0f, 1.0f),
    std::make_tuple(0.0f, 0.0f, 1.0f),
    std::make_tuple(0.0f, 0.0f, 1.0f),
    std::make_tuple(0.0f, 1.0f, 1.0f),
    std::make_tuple(0.0f, 1.0f, 1.0f),
    std::make_tuple(0.0f, 1.0f, 1.0f)
};

//                                 0   1  2  3  4  5  6  7  8  9  10  11 12  13  14 15 16  17  18  19  20  21  22  23  24
std::vector<int> endPointIndex = { 1, -1, 1, 2, 3, 1, 5, 6, 1, 8,  9, 10, 8, 12, 13, 0, 0, 15, 16, 14, 19, 14, 11, 22, 11};


}

}

#include "QtImage.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "MMM/MMMCore.h"

namespace MMM
{

QImage QtImage::Mat2QImage(const cv::Mat &mat) {
    if (mat.type() == CV_8UC1) {
        QVector<QRgb> colorTable;
        for (int i=0; i<256; i++)
            colorTable.push_back(qRgb(i,i,i));
        const uchar *qImageBuffer = (const uchar*)mat.data;
        QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_Indexed8);
        img.setColorTable(colorTable);
        return img;
    }
    else if (mat.type() == CV_8UC3)
    {
        const uchar *qImageBuffer = (const uchar*)mat.data;
        QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
        return img.rgbSwapped();
    }
    else if (mat.type() == CV_8UC4)
    {
        const uchar *qImageBuffer = (const uchar*)mat.data;
        QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGBA8888);
        return img.rgbSwapped();
    }
    else MMM_ERROR << mat.type() << " not supported" << std::endl;
    return QImage();
}

}

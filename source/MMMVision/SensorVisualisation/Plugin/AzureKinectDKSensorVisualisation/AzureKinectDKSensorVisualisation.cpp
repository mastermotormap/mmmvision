#include "AzureKinectDKSensorVisualisation.h"

#include <MMMVision/SensorVisualisation/CoinPointCloud.h>
#include <Inventor/nodes/SoMatrixTransform.h>
#include <Inventor/nodes/SoUnits.h>
#include <VirtualRobot/Robot.h>

namespace MMM
{

AzureKinectDKSensorVisualisation::AzureKinectDKSensorVisualisation(AzureKinectDKSensorPtr sensor, VirtualRobot::RobotPtr robot, SoSeparator *sceneSep, int maxDistanceMM) :
    SensorVisualisation(sceneSep, new SoSeparator(), false),
    sensor(sensor),
    robot(robot),
    maxDistanceMM(maxDistanceMM)
{
}

std::string AzureKinectDKSensorVisualisation::getType() {
    return sensor->getType();
}

int AzureKinectDKSensorVisualisation::getPriority() {
    return sensor->getPriority();
}

void AzureKinectDKSensorVisualisation::update(float timestep, float delta) {
    update(sensor->getDerivedMeasurement(timestep, delta));
}

void AzureKinectDKSensorVisualisation::update(float timestep) {
    update(sensor->getDerivedMeasurement(timestep, 0.005));
}

void AzureKinectDKSensorVisualisation::update(AzureKinectDKSensorMeasurementPtr measurement) {
    removeChildVisualisationSep(0);
    if (measurement) {
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud = measurement->getPointCloud(maxDistanceMM);
        if (!pointCloud) return;

        SoSeparator* result = new SoSeparator();
        SoUnits *u = new SoUnits();
        u->units = SoUnits::MILLIMETERS;
        result->addChild(u);
        Eigen::Matrix4f transformation = sensor->getCameraSensorGlobalPose(robot);
        SoMatrixTransform* mt = new SoMatrixTransform();
        SbMatrix m_(reinterpret_cast<SbMat*>(transformation.data()));
        mt->matrix.setValue(m_);
        result->addChild(mt);
        SoSeparator* pclSep = new CoinPointCloud(pointCloud, 2);
        result->addChild(pclSep);
        addChildVisualisationSep(result);
    }
}

}

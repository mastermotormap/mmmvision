/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#pragma once

#include <MMM/Viewer/SensorVisualisation/SensorVisualisation.h>
#include <MMMVision/Sensor/Plugin/AzureKinectDKSensor/AzureKinectDKSensor.h>
#include <memory>

namespace MMM
{

class AzureKinectDKSensorVisualisation;
typedef std::shared_ptr<AzureKinectDKSensorVisualisation> AzureKinectDKSensorVisualisationPtr;

class AzureKinectDKSensorVisualisation : public SensorVisualisation
{
public:
    AzureKinectDKSensorVisualisation(AzureKinectDKSensorPtr sensor, VirtualRobot::RobotPtr robot, SoSeparator* sceneSep, int maxDistanceMM = 3000);

    virtual std::string getType() override;
    virtual int getPriority() override;

protected:
    virtual void update(float timestep, float delta) override;

    virtual void update(float timestep) override;

    virtual void update(AzureKinectDKSensorMeasurementPtr measurement);

private:
    AzureKinectDKSensorPtr sensor;
    VirtualRobot::RobotPtr robot;
    int maxDistanceMM;

};

}

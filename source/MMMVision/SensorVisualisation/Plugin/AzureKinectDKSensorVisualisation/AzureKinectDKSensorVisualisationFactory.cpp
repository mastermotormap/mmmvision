#include "AzureKinectDKSensorVisualisationFactory.h"

#include <boost/extension/extension.hpp>

#include "AzureKinectDKSensorVisualisation.h"
#include <MMMVision/Sensor/Plugin/AzureKinectDKSensor/AzureKinectDKSensor.h>

namespace MMM
{

// register this factory
SensorVisualisationFactory::SubClassRegistry AzureKinectDKSensorVisualisationFactory::registry(VIS_STR(AzureKinectDKSensor::TYPE), &AzureKinectDKSensorVisualisationFactory::createInstance);

AzureKinectDKSensorVisualisationFactory::AzureKinectDKSensorVisualisationFactory() : SensorVisualisationFactory() {}

AzureKinectDKSensorVisualisationFactory::~AzureKinectDKSensorVisualisationFactory() = default;

SensorVisualisationPtr AzureKinectDKSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, std::shared_ptr<VirtualRobot::CoinVisualization> /*visualization*/, SoSeparator* sceneSep) {
    AzureKinectDKSensorPtr s = std::dynamic_pointer_cast<AzureKinectDKSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << AzureKinectDKSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new AzureKinectDKSensorVisualisation(s, robot, sceneSep));
}

std::string AzureKinectDKSensorVisualisationFactory::getName()
{
    return VIS_STR(AzureKinectDKSensor::TYPE);
}

SensorVisualisationFactoryPtr AzureKinectDKSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new AzureKinectDKSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new AzureKinectDKSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}

}

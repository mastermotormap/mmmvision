#include "Body25SensorVisualisationFactory.h"
#include "Body25SensorVisualisation.h"

#include <boost/extension/extension.hpp>

namespace MMM {

// register this factory
SensorVisualisationFactory::SubClassRegistry Body25SensorVisualisationFactory::registry(VIS_STR(Body25Sensor::TYPE), &Body25SensorVisualisationFactory::createInstance);

Body25SensorVisualisationFactory::Body25SensorVisualisationFactory() : SensorVisualisationFactory() {}

Body25SensorVisualisationFactory::~Body25SensorVisualisationFactory() {}

std::string Body25SensorVisualisationFactory::getName()
{
    return VIS_STR(Body25Sensor::TYPE);
}

SensorVisualisationPtr Body25SensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, VirtualRobot::CoinVisualizationPtr visualization, SoSeparator* sceneSep) {
    Body25SensorPtr s = std::dynamic_pointer_cast<Body25Sensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << Body25Sensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new Body25SensorVisualisation(s, robot, visualization, sceneSep));
}

SensorVisualisationFactoryPtr Body25SensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new Body25SensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new Body25SensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}

}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_Body25Sensor_VISUALISATIONFACTORY_H_
#define __MMM_Body25Sensor_VISUALISATIONFACTORY_H_

#include <string>

#include <MMM/Viewer/SensorVisualisation/SensorVisualisationFactory.h>
#include <MMM/Viewer/SensorVisualisation/SensorVisualisation.h>

namespace MMM
{

class Body25SensorVisualisationFactory : public SensorVisualisationFactory
{
public:
    Body25SensorVisualisationFactory();

    virtual ~Body25SensorVisualisationFactory();

    std::string getName();

    SensorVisualisationPtr createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, VirtualRobot::CoinVisualizationPtr visualization, SoSeparator* sceneSep);

    static SensorVisualisationFactoryPtr createInstance(void*);

private:
    static SubClassRegistry registry;

};

}

#endif

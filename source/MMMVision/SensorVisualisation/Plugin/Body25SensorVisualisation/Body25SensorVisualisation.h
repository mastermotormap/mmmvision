/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_Body25Sensor_VISUALISATION_H_
#define __MMM_Body25Sensor_VISUALISATION_H_

#include <MMM/Viewer/SensorVisualisation/SensorVisualisation.h>

#include <MMMVision/Sensor/Plugin/Body25Sensor/Body25Sensor.h>

#include <Inventor/nodes/SoSeparator.h>
#include <VirtualRobot/RobotConfig.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

namespace MMM
{

    class Body25SensorVisualisation : public SensorVisualisation
    {
    public:
        Body25SensorVisualisation(Body25SensorPtr sensor, VirtualRobot::RobotPtr robot, VirtualRobot::CoinVisualizationPtr visualization, SoSeparator* sceneSep);

        std::string getType();

        int getPriority();

    private:
        void update(float timestep, float delta);
        void update(float timestep);
        void update(Body25SensorMeasurementPtr measurement);

        Body25SensorPtr sensor;
        VirtualRobot::RobotPtr robot;
        VirtualRobot::CoinVisualizationPtr visualization;
    };
}

#endif

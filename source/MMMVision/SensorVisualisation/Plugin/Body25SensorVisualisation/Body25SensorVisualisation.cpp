#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/SceneObject.h>
#include <Inventor/nodes/SoSphere.h>

#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoLineSet.h>

#include <MMMVision/Sensor/Plugin/Body25Sensor/Body25SensorMeasurement.h>
#include <MMMVision/SensorVisualisation/Body25Visualisation.h>

#include "Body25SensorVisualisation.h"

#include <SimoxUtility/math/convert/pos_to_mat4f.h>

namespace MMM {

Body25SensorVisualisation::Body25SensorVisualisation(Body25SensorPtr sensor, VirtualRobot::RobotPtr robot, VirtualRobot::CoinVisualizationPtr visualization, SoSeparator *sceneSep)
    : SensorVisualisation(sceneSep), sensor(sensor), robot(robot), visualization(visualization) {
    setVisualisationSep(new SoSeparator());
}

std::string Body25SensorVisualisation::getType() {
    return sensor->getType();
}

int Body25SensorVisualisation::getPriority() {
    return sensor->getPriority();
}

void Body25SensorVisualisation::update(float timestep, float delta) {
    update(sensor->getDerivedMeasurement(timestep, delta));
}

void Body25SensorVisualisation::update(float timestep) {
    update(sensor->getDerivedMeasurement(timestep));
}

void Body25SensorVisualisation::update(Body25SensorMeasurementPtr measurement) {
    removeChildVisualisationSep(0);
    if (!measurement) return;

    SoSeparator* result = new SoSeparator();
    auto keyPoints = measurement->getKeyPoints();
    for (int i = 0; i <= 24; i++)
    {
        if (keyPoints.find(i) == keyPoints.end()) continue;

        Body25KeyPoint keyPoint = keyPoints.at(i);
        if (!keyPoint.local_pose || keyPoint.score < 0.25f) continue;

        Eigen::Vector3f keypos = robot->toGlobalCoordinateSystemVec(keyPoint.local_pose.value());
        std::tuple<float, float, float> color = BODY25::openposeKeyPointColor[i];
        result->addChild(VirtualRobot::CoinVisualizationFactory::CreateSphere(keypos, 10.0f, std::get<0>(color), std::get<1>(color), std::get<2>(color)));

        // Draw line if possible
        if (keyPoints.find(BODY25::endPointIndex[i]) == keyPoints.end()) continue;

        Body25KeyPoint keyPoint2 = keyPoints.at(BODY25::endPointIndex[i]);
        if (!keyPoint2.local_pose) continue;

        Eigen::Vector3f keypos2 = robot->toGlobalCoordinateSystemVec(keyPoint2.local_pose.value());
        result->addChild(VirtualRobot::CoinVisualizationFactory::createCoinLine(simox::math::pos_to_mat4f(keypos), simox::math::pos_to_mat4f(keypos2), 5.0f, std::get<0>(color), std::get<1>(color), std::get<2>(color)));
    }

    addChildVisualisationSep(result);
}

}

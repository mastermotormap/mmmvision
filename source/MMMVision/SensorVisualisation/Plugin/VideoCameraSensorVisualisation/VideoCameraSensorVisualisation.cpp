#include "VideoCameraSensorVisualisation.h"

#include <MMMVision/SensorVisualisation/CoinImage.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoUnits.h>
#include <Inventor/nodes/SoMatrixTransform.h>
#include <VirtualRobot/Robot.h>

namespace MMM
{

VideoCameraSensorVisualisation::VideoCameraSensorVisualisation(VideoCameraSensorPtr sensor, VirtualRobot::RobotPtr robot, SoSeparator *sceneSep) :
    SensorVisualisation(sceneSep, new SoSeparator(), false),
    sensor(sensor),
    robot(robot)
{
}

std::string VideoCameraSensorVisualisation::getType() {
    return sensor->getType();
}

int VideoCameraSensorVisualisation::getPriority() {
    return sensor->getPriority();
}

void VideoCameraSensorVisualisation::update(float timestep, float delta) {
    update(sensor->getDerivedMeasurement(timestep, delta));
}

void VideoCameraSensorVisualisation::update(float timestep) {
    update(sensor->getDerivedMeasurement(timestep));
}

void VideoCameraSensorVisualisation::update(VideoCameraSensorMeasurementPtr measurement) {
    removeChildVisualisationSep(0);
    if (measurement) {
        SoSeparator* result = new SoSeparator();
        if (robot) {
            Eigen::Matrix4f transformation = robot->getGlobalPose() * sensor->getTransformation();
            SoMatrixTransform* mt = new SoMatrixTransform();
            SbMatrix m_(reinterpret_cast<SbMat*>(transformation.data()));
            mt->matrix.setValue(m_);
            result->addChild(mt);
        }
        result->addChild(new CoinImage(measurement->getImageRGB(), 0.1));
        addChildVisualisationSep(result);
    }
}

}

#include "VideoCameraSensorVisualisationFactory.h"

#include <boost/extension/extension.hpp>

#include "VideoCameraSensorVisualisation.h"
#include <MMMVision/Sensor/Plugin/VideoCameraSensor/VideoCameraSensor.h>

namespace MMM
{

// register this factory
SensorVisualisationFactory::SubClassRegistry VideoCameraSensorVisualisationFactory::registry(VIS_STR(VideoCameraSensor::TYPE), &VideoCameraSensorVisualisationFactory::createInstance);

VideoCameraSensorVisualisationFactory::VideoCameraSensorVisualisationFactory() : SensorVisualisationFactory() {}

VideoCameraSensorVisualisationFactory::~VideoCameraSensorVisualisationFactory() = default;

SensorVisualisationPtr VideoCameraSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, std::shared_ptr<VirtualRobot::CoinVisualization> /*visualization*/, SoSeparator* sceneSep) {
    VideoCameraSensorPtr s = std::dynamic_pointer_cast<VideoCameraSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << VideoCameraSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new VideoCameraSensorVisualisation(s, robot, sceneSep));
}

std::string VideoCameraSensorVisualisationFactory::getName()
{
    return VIS_STR(VideoCameraSensor::TYPE);
}

SensorVisualisationFactoryPtr VideoCameraSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new VideoCameraSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new VideoCameraSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}

}

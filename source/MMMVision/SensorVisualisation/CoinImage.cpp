#include "CoinImage.h"

namespace MMM
{

CoinImage::CoinImage(const cv::Mat &image, float pxlPerMM) {
    height = image.rows * pxlPerMM;
    width = image.cols * pxlPerMM;
    horAlignment = SoImage::HorAlignment::CENTER;
    SoSFImage sosfiImage = SoSFImage();
    sosfiImage.setValue(SbVec2s(image.cols, image.rows), image.channels(), image.data, SoSFImage::NO_COPY);
    this->image = sosfiImage;
}

}

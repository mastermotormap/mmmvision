/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <opencv2/core/mat.hpp>

namespace MMM
{

class RGBSensorMeasurement;
typedef std::shared_ptr<RGBSensorMeasurement> RGBSensorMeasurementPtr;

class RGBSensorMeasurement : public SensorMeasurement
{
public:
    virtual ~RGBSensorMeasurement() = default;

    //! Return BGR Image
    virtual cv::Mat getImage() = 0;

    virtual cv::Mat getImageRGB() = 0;

protected:
    RGBSensorMeasurement(float timestep, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    virtual void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) override;
};

}

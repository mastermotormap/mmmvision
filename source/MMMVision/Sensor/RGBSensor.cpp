#include "RGBSensor.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/Sensor.h>
#include <SimoxUtility/math.h>

namespace MMM
{

bool MMM::RGBSensor::checkModel(MMM::ModelPtr model) {
    return true; // does not require a model
}

std::filesystem::path RGBSensor::getVideoFilePath() {
    return filePath;
}

void MMM::RGBSensor::setTransformation(const Eigen::Matrix4f &transformation) {
    this->transformation = transformation;
}

Eigen::Matrix4f MMM::RGBSensor::getTransformation() {
    return transformation;
}

Eigen::Matrix4f RGBSensor::getCameraSensorGlobalPose(ModelPtr model) {
    Eigen::Matrix4f transformation = model ? model->getGlobalPose() : Eigen::Matrix4f::Identity();
    if (getTransformation() != Eigen::Matrix4f::Identity()) {
        transformation *= getTransformation();
    }
    else if (model) {
        VirtualRobot::SensorPtr rgbSensor = model->getSensor("rgb");
        if (rgbSensor)
            transformation *= rgbSensor->getParentNodeToSensorTransformation();
    }
    return transformation;
}

Eigen::Matrix4f RGBSensor::toModelPose(const Eigen::Matrix4f &localPose_cameraCoord) {
    return simox::math::inverted_pose(transformation) * localPose_cameraCoord;
}

Eigen::Vector3f RGBSensor::toModelPoseVec(const Eigen::Vector3f &localPosition_cameraCoord) {
    return simox::math::mat4f_to_pos(toModelPose(simox::math::pos_to_mat4f(localPosition_cameraCoord)));
}

std::filesystem::path RGBSensor::checkFilePath(const std::filesystem::path &path, const std::filesystem::path &motionFilePath) {
    std::filesystem::path dataFilePath = path;
    if (!MMM::xml::isValid(dataFilePath)) {
        // check if relative file path
        auto relativePath = std::filesystem::absolute(motionFilePath.parent_path() / dataFilePath);
        if (!MMM::xml::isValid(relativePath)) {
            if (handleSensorFilePath) {
                bool result = handleSensorFilePath(dataFilePath);
                if (!MMM::xml::isValid(dataFilePath)) {
                    if (result)
                        throw Exception::MMMFormatException(std::string(dataFilePath) + " is neither a valid relative nor absolute file path!");
                    else
                        throw Exception::MMMSensorFileException(dataFilePath);
                }
            }
            else throw Exception::MMMFormatException(std::string(dataFilePath) + " is neither a valid relative nor absolute file path!");
        }
        else return relativePath;
    }
    return dataFilePath;
}

MMM::RGBSensor::RGBSensor(const std::filesystem::path &filePath, const std::string &description) :
    Sensor(description),
    filePath(filePath),
    timeOffset(0.0f),
    transformation(Eigen::Matrix4f::Identity())
{
}

void MMM::RGBSensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    auto videoContainerNode = node->first_node("VideoContainer");
    if (videoContainerNode->has_attribute("offset"))
        this->timeOffset = videoContainerNode->attribute_value_<float>("offset");
    else this->timeOffset = 0.0f;
    this->filePath = checkFilePath(videoContainerNode->attribute_value(xml::attribute::PATH), filePath);
    if (node->has_node("CameraSensorTransformation")) {
        auto cameraSensorTransformationNode = node->first_node("CameraSensorTransformation");
        transformation = cameraSensorTransformationNode->value_matrix4f();
    }
    else transformation = Eigen::Matrix4f::Identity();

    loadDerivedConfigurationXML(node, filePath);
}

void MMM::RGBSensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    auto videoContainerNode = node->append_node("VideoContainer");
    videoContainerNode->append_attribute(xml::attribute::PATH, std::filesystem::proximate(this->filePath, filePath.parent_path()));
    if (timeOffset != 0.0f)
        videoContainerNode->append_attribute("offset", timeOffset);
    if (!transformation.isIdentity()) {
        node->append_data_node(transformation, "CameraSensorTransformation");
    }

    appendDerivedConfigurationXML(node, filePath);
}

}

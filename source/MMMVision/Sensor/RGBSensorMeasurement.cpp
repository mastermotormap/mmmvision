#include "RGBSensorMeasurement.h"

namespace MMM
{

RGBSensorMeasurement::RGBSensorMeasurement(float timestep, SensorMeasurementType type) : SensorMeasurement(timestep, type)
{
}

void RGBSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    // do nothing
}

}

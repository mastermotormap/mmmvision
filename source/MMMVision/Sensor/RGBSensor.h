/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <MMM/Motion/Sensor/BasicSensor.h>
#include "RGBSensorMeasurement.h"

namespace MMM
{

class RGBSensor : public Sensor
{
public:
    virtual ~RGBSensor() = default;

    virtual bool checkModel(ModelPtr model) override;

    virtual unsigned int getFramesPerSecond() = 0;

    virtual std::pair<unsigned int, unsigned int> getResolution() = 0;

    virtual void shiftMeasurements(float delta) override {
        timeOffset += delta;
    }

    std::filesystem::path getVideoFilePath();

    void setTransformation(const Eigen::Matrix4f &transformation);

    //! Returns the transformation from model pose to image sensor pose
    Eigen::Matrix4f getTransformation();

    Eigen::Matrix4f getCameraSensorGlobalPose(ModelPtr model);

    Eigen::Matrix4f toModelPose(const Eigen::Matrix4f &localPose_cameraCoord);

    Eigen::Vector3f toModelPoseVec(const Eigen::Vector3f &localPosition_cameraCoord);

    virtual RGBSensorMeasurementPtr getRGBSensorMeasurement(float timestep, float delta) = 0;

    std::filesystem::path checkFilePath(const std::filesystem::path &path, const std::filesystem::path &motionFilePath);

protected:
    RGBSensor(const std::filesystem::path &filePath, const std::string &description = std::string());

    RGBSensor() : Sensor()
    {
    }

    virtual void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    virtual void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    virtual void loadMeasurementNodeXML(simox::xml::RapidXMLWrapperNodePtr node) override {
        // Do nothing
    }

    virtual void appendDataXML(simox::xml::RapidXMLWrapperNodePtr node) override {
        // Do nothing
    }

    virtual void loadDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) = 0;

    virtual void appendDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) = 0;

    std::filesystem::path filePath;
    float timeOffset;
    Eigen::Matrix4f transformation;
};

typedef std::shared_ptr<RGBSensor> RGBSensorPtr;

}

#include "VideoCameraSensorMeasurement.h"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

namespace MMM
{

VideoCameraSensorMeasurement::VideoCameraSensorMeasurement(const cv::Mat &image, float timestep) : RGBSensorMeasurement(timestep), image(image)
{
}

VideoCameraSensorMeasurement::~VideoCameraSensorMeasurement() {
    image.release();
}

cv::Mat VideoCameraSensorMeasurement::getImage() {
    return image;
}

cv::Mat VideoCameraSensorMeasurement::getImageRGB() {
    cv::Mat cvtImage;
    cv::cvtColor(getImage(), cvtImage, CV_BGR2RGB);
    return cvtImage;
}

SensorMeasurementPtr VideoCameraSensorMeasurement::clone() {
    // TODO
    return nullptr;
}

bool VideoCameraSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    // TODO
    return false;
}

void VideoCameraSensorMeasurement::shiftMeasurements(float offset) {
    timestep += offset;
}

}

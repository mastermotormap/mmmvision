#include "VideoCameraSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "VideoCameraSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry VideoCameraSensorFactory::registry(NAME_STR<VideoCameraSensor>(), &VideoCameraSensorFactory::createInstance);

VideoCameraSensorFactory::VideoCameraSensorFactory() : SensorFactory() {}

VideoCameraSensorFactory::~VideoCameraSensorFactory() = default;

SensorPtr VideoCameraSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    try {
        return VideoCameraSensor::loadSensorXML(sensorNode, filePath);
    }
    catch (Exception::MMMSensorFileException &e) {
        MMM_WARNING << "Could not read video camera sensor from path " << e.getFilePath() << ". Sensor ignored!" << std::endl;
        return nullptr;
    }
}

std::string VideoCameraSensorFactory::getName()
{
    return NAME_STR<VideoCameraSensor>();
}

SensorFactoryPtr VideoCameraSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new VideoCameraSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new VideoCameraSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}

#include "VideoCameraSensor.h"

#include "opencv2/opencv.hpp"

namespace MMM
{

VideoCameraSensor::VideoCameraSensor(const std::filesystem::path &filePath, const std::string &description) : RGBSensor(filePath, description)
{
    loadVideoData();
}

VideoCameraSensorPtr VideoCameraSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    VideoCameraSensorPtr sensor(new VideoCameraSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

SensorMeasurementPtr VideoCameraSensor::getMeasurement(float timestep) {
    return getDerivedMeasurement(timestep);
}

SensorMeasurementPtr VideoCameraSensor::getMeasurement(float timestep, float delta){
    return getDerivedMeasurement(timestep, delta);
}

VideoCameraSensorMeasurementPtr VideoCameraSensor::getDerivedMeasurement(float timestep) {
    return getDerivedMeasurement(timestep, 0.0001);
}

VideoCameraSensorMeasurementPtr VideoCameraSensor::getDerivedMeasurement(float timestep, float delta) {
    return SensorMeasurement::getDerivedMeasurement(imageMeasurements, timestep, delta);
}

SensorPtr VideoCameraSensor::getSegmentSensor(float startTimestep, float endTimestep, bool changeTimestep) {
    throw MMM::Exception::NotImplementedException();
}

bool VideoCameraSensor::hasMeasurement() {
    return !imageMeasurements.empty();
}

bool VideoCameraSensor::hasMeasurement(float timestep) {
    return imageMeasurements.find(timestep) != imageMeasurements.end();
}

std::vector<float> VideoCameraSensor::getTimesteps() {
    std::vector<float> timesteps;
    for (const auto &m : imageMeasurements) {
        timesteps.push_back(m.first);
    }
    return timesteps;
}

std::map<float, SensorMeasurementPtr> VideoCameraSensor::getMeasurements() {
    std::map<float, SensorMeasurementPtr> measurements;
    for (const auto &image : imageMeasurements) {
        measurements[image.first] = image.second;
    }
    return measurements;
}

SensorPtr VideoCameraSensor::clone() {
    throw MMM::Exception::NotImplementedException();
}

float VideoCameraSensor::getMinTimestep() {
    return timeOffset;
}

float VideoCameraSensor::getMaxTimestep() {
    return timeOffset + recordingLength;
}

void VideoCameraSensor::shiftMeasurements(float delta) {
    RGBSensor::shiftMeasurements(delta);
    std::map<float, VideoCameraSensorMeasurementPtr> measurements;
    for (const auto &image : imageMeasurements) {
        image.second->shiftMeasurements(delta);
        measurements[image.first + delta] = image.second;
    }
    imageMeasurements.clear();
    imageMeasurements = measurements;
}

void VideoCameraSensor::extend(float minTimestep, float maxTimestep) {
    throw MMM::Exception::NotImplementedException();
}

bool VideoCameraSensor::equalsConfiguration(SensorPtr other) {
    // TODO
    return false;
}

unsigned int VideoCameraSensor::getFramesPerSecond() {
    return fps;
}

std::pair<unsigned int, unsigned int> VideoCameraSensor::getResolution() {
    throw MMM::Exception::NotImplementedException();
}

std::string VideoCameraSensor::getType() {
    return TYPE;
}

std::string VideoCameraSensor::getVersion() {
    return VERSION;
}

RGBSensorMeasurementPtr VideoCameraSensor::getRGBSensorMeasurement(float timestep, float delta) {
    return getDerivedMeasurement(timestep, delta);
}

VideoCameraSensor::VideoCameraSensor() : RGBSensor()
{
}

void VideoCameraSensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type) {
    // Do nothing
}

bool VideoCameraSensor::hasSensorConfigurationContent() const {
    return true;
}

SensorPtr VideoCameraSensor::joinSensor(SensorPtr sensor) {
    throw MMM::Exception::NotImplementedException();
}

void VideoCameraSensor::loadDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &/*filePath*/) {
    loadVideoData();
}

void VideoCameraSensor::appendDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &/*filePath*/) {
    // Do nothing
}

void VideoCameraSensor::loadVideoData() {
    cv::VideoCapture cap(cv::String(this->filePath));
    if(!cap.isOpened()){
        throw MMM::Exception::MMMFormatException("Error opening video file from " + std::string(this->filePath));
    }

    fps = cap.get(cv::CAP_PROP_FPS);
//    auto frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
    auto startTimestep = cap.get(cv::CAP_PROP_POS_MSEC) / 1000.0f;

    float timestep;
    int failed = 0;
    while(true) {
        timestep = cap.get(cv::CAP_PROP_POS_MSEC) / 1000.0f - startTimestep + timeOffset;
        cv::Mat frame;
        if(!cap.read(frame)) {
            failed++;
        }
        if (!frame.empty()) {
            failed = 0;
            VideoCameraSensorMeasurementPtr measurement = std::make_shared<VideoCameraSensorMeasurement>(frame, timestep);
            imageMeasurements[timestep] = measurement;
        }
        else MMM_INFO << "Empty frame when retrieving image at timestep " << timestep << ". Will retry." << std::endl;

        if (failed > 5)
            break;
    }

    recordingLength = timestep - timeOffset;

    cap.release();
}

}

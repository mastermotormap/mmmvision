/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#pragma once

#include <MMM/Motion/Sensor/BasicSensor.h>
#include "VideoCameraSensorMeasurement.h"

namespace MMM
{

class VideoCameraSensor;
typedef std::shared_ptr<VideoCameraSensor> VideoCameraSensorPtr;

class VideoCameraSensor : public RGBSensor
{
public:
    VideoCameraSensor(const std::filesystem::path &filePath, const std::string &description = std::string());

    virtual ~VideoCameraSensor() = default;

    static VideoCameraSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    virtual SensorMeasurementPtr getMeasurement(float timestep) override;

    virtual SensorMeasurementPtr getMeasurement(float timestep, float delta) override;

    virtual VideoCameraSensorMeasurementPtr getDerivedMeasurement(float timestep);

    virtual VideoCameraSensorMeasurementPtr getDerivedMeasurement(float timestep, float delta);

    virtual SensorPtr getSegmentSensor(float startTimestep, float endTimestep, bool changeTimestep) override;

    virtual bool hasMeasurement() override;

    virtual bool hasMeasurement(float timestep) override;

    virtual std::vector<float> getTimesteps() override;

    virtual std::map<float, SensorMeasurementPtr> getMeasurements() override;

    virtual SensorPtr clone() override;

    virtual float getMinTimestep() override;

    virtual float getMaxTimestep() override;

    virtual void shiftMeasurements(float delta) override;

    virtual void extend(float minTimestep, float maxTimestep) override;

    virtual bool equalsConfiguration(SensorPtr other) override;

    virtual unsigned int getFramesPerSecond() override;

    virtual std::pair<unsigned int, unsigned int> getResolution() override;

    virtual std::string getType() override;

    virtual std::string getVersion() override;

    RGBSensorMeasurementPtr getRGBSensorMeasurement(float timestep, float delta) override;

    static constexpr const char* TYPE = "VideoCamera";

    static constexpr const char* VERSION = "1.0";

protected:
    VideoCameraSensor();

    virtual void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);

    virtual bool hasSensorConfigurationContent() const override;

    virtual SensorPtr joinSensor(SensorPtr sensor) override;

    virtual void loadDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    virtual void appendDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    void loadVideoData();

private:
    unsigned int fps;
    float recordingLength;
    std::map<float, VideoCameraSensorMeasurementPtr> imageMeasurements;
};

}

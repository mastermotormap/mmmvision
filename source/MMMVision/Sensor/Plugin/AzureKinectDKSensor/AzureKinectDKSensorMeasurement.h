/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <k4a/k4atypes.h>
#include <MMMVision/Sensor/RGBDSensorMeasurement.h>
#include <opencv2/core/mat.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <SensorLibraries/AzureKinectDKWrapper/AzureKinectPlayback.h>

namespace MMM
{

class AzureKinectDKSensorMeasurement;
typedef std::shared_ptr<AzureKinectDKSensorMeasurement> AzureKinectDKSensorMeasurementPtr;

class AzureKinectDKSensorMeasurement : public RGBDSensorMeasurement
{
public:
    AzureKinectDKSensorMeasurement(float timestep, MMMVision::AzureKinectPlaybackPtr playback, k4a_capture_t capture, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    virtual ~AzureKinectDKSensorMeasurement();

    //! Retrieve the color image as BGR. Method copys the image buffer
    cv::Mat getImage() override;

    //! Retrieve the color image as RGB. Method copys the image buffer
    cv::Mat getImageRGB() override;

    //! Retrieve the color and depth image. Method copys the image buffer
    bool getImages(cv::Mat &colorImage, cv::Mat &depthImage, bool transformToColor = true, bool normalize = false) override;

    //! Retrieve the color, depth and ir image. Method copys the image buffer
    bool getImages(cv::Mat &colorImage, cv::Mat &depthImage, cv::Mat &irImage, bool transformToColor = true, bool normalize = false) override;

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr getPointCloud(int maximumDistanceMM = -1) override;

    virtual SensorMeasurementPtr clone() override;

    virtual bool equals(SensorMeasurementPtr sensorMeasurement) override;

protected:
    virtual void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) override;

    MMMVision::AzureKinectPlaybackPtr playback;
    k4a_capture_t capture;
};

}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <MMMVision/Sensor/RGBDSensor.h>
#include "AzureKinectDKSensorMeasurement.h"

namespace MMM
{

class AzureKinectDKSensor;
typedef std::shared_ptr<AzureKinectDKSensor> AzureKinectDKSensorPtr;

class AzureKinectDKSensor : public RGBDSensor
{
public:
    static AzureKinectDKSensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    AzureKinectDKSensor(const std::filesystem::path &filePath, const std::string &description = std::string(), float timeDelta = 0.0001);

    virtual ~AzureKinectDKSensor() {}

    virtual bool equalsConfiguration(SensorPtr other) override;

    virtual std::string getType() override;

    virtual std::string getVersion() override;

    virtual SensorMeasurementPtr getMeasurement(float timestep) override;

    virtual SensorMeasurementPtr getMeasurement(float timestep, float delta) override;

    AzureKinectDKSensorMeasurementPtr getFirstMeasurement();

    AzureKinectDKSensorMeasurementPtr getNextMeasurement();

    AzureKinectDKSensorMeasurementPtr getDerivedMeasurement(float timestep);

    AzureKinectDKSensorMeasurementPtr getDerivedMeasurement(float timestep, float delta);

    virtual SensorPtr getSegmentSensor(float startTimestep, float endTimestep, bool changeTimestep) override;

    virtual bool hasMeasurement() override;

    virtual bool hasMeasurement(float timestep) override;

    virtual std::vector<float> getTimesteps() override;

    virtual std::map<float, SensorMeasurementPtr> getMeasurements() override;

    virtual SensorPtr clone() override;

    virtual float getMinTimestep() override;

    virtual float getMaxTimestep() override;

    virtual void shiftMeasurements(float delta) override;

    virtual void extend(float minTimestep, float maxTimestep) override;

    virtual unsigned int getFramesPerSecond() override;

    virtual std::pair<unsigned int, unsigned int> getColorResolution() override;

    virtual std::pair<unsigned int, unsigned int> getDepthResolution() override;

    Eigen::Vector3f transform2dTo3d(float x, float y, float depthMM);

    RGBDSensorMeasurementPtr getRGBDSensorMeasurement(float timestep, float delta) override;

    static constexpr const char* TYPE = "AzureKinectDK";

    static constexpr const char* VERSION = "1.0";

protected:
    AzureKinectDKSensor(float timeDelta = 0.0001);

    virtual bool hasSensorConfigurationContent() const override;

    virtual SensorPtr joinSensor(SensorPtr sensor) override;

    virtual void loadDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    virtual void appendDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) override;

    void retrieveTimesteps();

    MMMVision::AzureKinectPlaybackPtr playback;
    std::vector<float> timesteps;
    float recordingLength;
    float timeDelta;
};

}

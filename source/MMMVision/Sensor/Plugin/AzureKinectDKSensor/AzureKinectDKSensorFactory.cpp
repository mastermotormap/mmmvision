#include "AzureKinectDKSensorFactory.h"

#include <boost/extension/extension.hpp>

#include "AzureKinectDKSensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry AzureKinectDKSensorFactory::registry(NAME_STR<AzureKinectDKSensor>(), &AzureKinectDKSensorFactory::createInstance);

AzureKinectDKSensorFactory::AzureKinectDKSensorFactory() : SensorFactory() {}

AzureKinectDKSensorFactory::~AzureKinectDKSensorFactory() = default;

SensorPtr AzureKinectDKSensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    try {
        return AzureKinectDKSensor::loadSensorXML(sensorNode, filePath);
    }
    catch (Exception::MMMSensorFileException &e) {
        MMM_WARNING << "Could not read video camera sensor from path " << e.getFilePath() << ". Sensor ignored!" << std::endl;
        return nullptr;
    }
}

std::string AzureKinectDKSensorFactory::getName()
{
    return NAME_STR<AzureKinectDKSensor>();
}

SensorFactoryPtr AzureKinectDKSensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new AzureKinectDKSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new AzureKinectDKSensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}

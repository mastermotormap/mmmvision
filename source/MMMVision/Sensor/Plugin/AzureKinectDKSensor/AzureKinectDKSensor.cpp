#include "AzureKinectDKSensor.h"


namespace MMM
{

AzureKinectDKSensorPtr AzureKinectDKSensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    AzureKinectDKSensorPtr sensor(new AzureKinectDKSensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

bool AzureKinectDKSensor::equalsConfiguration(SensorPtr other) {
    // TODO
    return false;
}

std::string AzureKinectDKSensor::getType() {
    return TYPE;
}

std::string AzureKinectDKSensor::getVersion() {
    return VERSION;
}

SensorMeasurementPtr AzureKinectDKSensor::getMeasurement(float timestep) {
    return getDerivedMeasurement(timestep);
}

SensorMeasurementPtr AzureKinectDKSensor::getMeasurement(float timestep, float delta) {
    return getDerivedMeasurement(timestep, delta);
}

AzureKinectDKSensorMeasurementPtr AzureKinectDKSensor::getFirstMeasurement() {
    playback->jumpToMS(0);
    return getNextMeasurement();
}

AzureKinectDKSensorMeasurementPtr AzureKinectDKSensor::getNextMeasurement() {
    k4a_capture_t capture;
    if (!playback->getCapture(capture)) return nullptr;
    float timestamp = playback->getTimestep(capture, timeOffset);
    return AzureKinectDKSensorMeasurementPtr(new AzureKinectDKSensorMeasurement(timestamp, playback, capture));
}

AzureKinectDKSensorMeasurementPtr AzureKinectDKSensor::getDerivedMeasurement(float timestep) {
    return getDerivedMeasurement(timestep, timeDelta);
}

AzureKinectDKSensorMeasurementPtr AzureKinectDKSensor::getDerivedMeasurement(float timestep, float delta) {
    float newDelta = std::min(0.5f / getFramesPerSecond(), delta);
    playback->jumpTo(timestep - timeOffset, newDelta);
    k4a_capture_t capture;
    if (!playback->getCapture(capture)) return nullptr;
    float timestamp = playback->getTimestep(capture, timeOffset);
    if (std::abs(timestep - timestamp) < delta)
        return AzureKinectDKSensorMeasurementPtr(new AzureKinectDKSensorMeasurement(timestamp, playback, capture));
    return nullptr;
}

SensorPtr AzureKinectDKSensor::getSegmentSensor(float startTimestep, float endTimestep, bool changeTimestep) {
    throw Exception::NotImplementedException();
}

bool AzureKinectDKSensor::hasMeasurement() {
    return true; // TODO
}

bool AzureKinectDKSensor::hasMeasurement(float timestep) {
    return true; // TODO!
}

std::vector<float> AzureKinectDKSensor::getTimesteps() {
    retrieveTimesteps();
    return timesteps;
}

std::map<float, SensorMeasurementPtr> AzureKinectDKSensor::getMeasurements() {
    std::map<float, SensorMeasurementPtr> measurements;
    retrieveTimesteps();
    for (float timestep : timesteps) {
        measurements[timestep] = getMeasurement(timestep);
    }
    return measurements;
}

SensorPtr AzureKinectDKSensor::clone() {
    throw Exception::NotImplementedException();
}

float AzureKinectDKSensor::getMinTimestep() {
    return timeOffset;
}

float AzureKinectDKSensor::getMaxTimestep() {
    return timeOffset + recordingLength;
}

void AzureKinectDKSensor::shiftMeasurements(float delta) {
    if (timesteps.size() > 0) {
        std::transform(timesteps.begin(), timesteps.end(), timesteps.begin(), [&](double x){ return  x + delta; });
    }
    RGBSensor::shiftMeasurements(delta);
}

void AzureKinectDKSensor::extend(float minTimestep, float maxTimestep) {
    // TODO
}

unsigned int AzureKinectDKSensor::getFramesPerSecond() {
    return playback->getConfig()->getFPSAsInt();
}

std::pair<unsigned int, unsigned int> AzureKinectDKSensor::getColorResolution() {
    return playback->getConfig()->getColorDimensions();
}

std::pair<unsigned int, unsigned int> AzureKinectDKSensor::getDepthResolution() {
    return playback->getConfig()->getDepthDimensions();
}

Eigen::Vector3f AzureKinectDKSensor::transform2dTo3d(float x, float y, float depthMM) {
    return playback->transform2dTo3d(x, y, depthMM);
}

RGBDSensorMeasurementPtr AzureKinectDKSensor::getRGBDSensorMeasurement(float timestep, float delta) {
    return getDerivedMeasurement(timestep, delta);
}

AzureKinectDKSensor::AzureKinectDKSensor(float timeDelta) : RGBDSensor(), timeDelta(timeDelta)
{
}

AzureKinectDKSensor::AzureKinectDKSensor(const std::filesystem::path &filePath, const std::string &description, float timeDelta) :
    RGBDSensor(filePath, description),
    playback(std::make_shared<MMMVision::AzureKinectPlayback>(filePath)),
    recordingLength(playback->getRecordingLength()),
    timeDelta(timeDelta)
{
}

bool AzureKinectDKSensor::hasSensorConfigurationContent() const {
    return true;
}

SensorPtr AzureKinectDKSensor::joinSensor(SensorPtr sensor) {
    // TODO
    throw Exception::NotImplementedException();
}

void AzureKinectDKSensor::loadDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &/*filePath*/) {
    this->playback = MMMVision::AzureKinectPlaybackPtr(new MMMVision::AzureKinectPlayback(this->filePath));
    recordingLength = playback->getRecordingLength();
}

void AzureKinectDKSensor::appendDerivedConfigurationXML(simox::xml::RapidXMLWrapperNodePtr /*node*/, const std::filesystem::path &/*filePath*/) {
    // Do nothing
}

void AzureKinectDKSensor::retrieveTimesteps() {
    if (timesteps.size() == 0) {
        try {
            timesteps = playback->getTimesteps(timeOffset);
        } catch (std::runtime_error &e) {
            MMM_ERROR << "Retrieving timesteps failed: " << e.what() << std::endl;
        }
    }
}

}

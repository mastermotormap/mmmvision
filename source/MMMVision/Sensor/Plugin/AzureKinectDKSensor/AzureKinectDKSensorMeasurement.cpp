#include "AzureKinectDKSensorMeasurement.h"

#include <k4a/k4a.h>
#include "opencv2/imgproc/imgproc.hpp"

namespace MMM
{

AzureKinectDKSensorMeasurement::AzureKinectDKSensorMeasurement(float timestep, MMMVision::AzureKinectPlaybackPtr playback, k4a_capture_t capture, SensorMeasurementType type) :
    RGBDSensorMeasurement(timestep, type), playback(playback), capture(capture)
{
}

AzureKinectDKSensorMeasurement::~AzureKinectDKSensorMeasurement() {
    k4a_capture_release(capture);
}

cv::Mat AzureKinectDKSensorMeasurement::getImage() {
    k4a_image_t colorImage = playback->getColorImageFromCapture(capture);
    cv::Mat cvtImage = MMMVision::IAzureKinect::transform(colorImage).clone();
    k4a_image_release(colorImage);
    return cvtImage;
}

cv::Mat AzureKinectDKSensorMeasurement::getImageRGB() {
    cv::Mat cvtImage;
    cv::cvtColor(getImage(), cvtImage, CV_RGB2BGR);
    return cvtImage;
}

bool AzureKinectDKSensorMeasurement::getImages(cv::Mat &colorImage, cv::Mat &depthImage, bool transformToColor, bool normalize) {
    return playback->getImagesFromCapture(capture, colorImage, depthImage, transformToColor, normalize);
}

bool AzureKinectDKSensorMeasurement::getImages(cv::Mat &colorImage, cv::Mat &depthImage, cv::Mat &irImage, bool transformToColor, bool normalize) {
    return playback->getImagesFromCapture(capture, colorImage, depthImage, irImage, transformToColor, normalize);
}

pcl::PointCloud<pcl::PointXYZRGBA>::Ptr AzureKinectDKSensorMeasurement::getPointCloud(int maximumDistanceMM) {
    try {
        return playback->getRGBAPointCloudFromCapture(capture, maximumDistanceMM);
    }
    catch (std::runtime_error &e)  {
        MMM_ERROR << "Could not retrieve point cloud" << e.what() << std::endl;
        return nullptr;
    }
}

SensorMeasurementPtr AzureKinectDKSensorMeasurement::clone() {
    throw MMM::Exception::NotImplementedException();
}

bool AzureKinectDKSensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    // TODO
    return false;
}

void AzureKinectDKSensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr /*measurementNode*/) {
    // do nothing
}

}

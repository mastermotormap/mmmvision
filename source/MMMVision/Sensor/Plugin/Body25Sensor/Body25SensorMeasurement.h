/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <MMM/Motion/Sensor/SensorMeasurement.h>

namespace MMM
{

struct Body25KeyPoint {
    float camera_x = 0.0;
    float camera_y = 0.0;
    float score = 1.0f;
    std::optional<Eigen::Vector3f> local_pose;

    bool operator <(const Body25KeyPoint &o) const {
        if (camera_x == o.camera_x)
            return (camera_x < o.camera_x);
        else return (camera_y < o.camera_y);
    }

    bool operator ==(const Body25KeyPoint &o) const {
        return *this < o;
    }
};

class Body25SensorMeasurement;
typedef std::shared_ptr<Body25SensorMeasurement> Body25SensorMeasurementPtr;

class Body25SensorMeasurement : public SensorMeasurement, SMCloneable<Body25SensorMeasurement>
{
public:
    Body25SensorMeasurement(float timestep, const std::map<int, Body25KeyPoint> &keyPoints, SensorMeasurementType type = SensorMeasurementType::MEASURED);

    SensorMeasurementPtr clone();

    bool equals(SensorMeasurementPtr sensorMeasurement);

    Body25SensorMeasurementPtr clone(float newTimestep);

    std::map<int, Body25KeyPoint> getKeyPoints();

protected:
    void appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode);

private:
    std::map<int, Body25KeyPoint> keyPoints;
};

}

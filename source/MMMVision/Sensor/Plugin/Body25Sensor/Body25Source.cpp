#include "Body25Source.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>
#include <MMM/Exceptions.h>

namespace MMM
{

void BODY25::KeyPointSource::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    auto sourceNode = node->append_node("KeyPoint");
    sourceNode->append_attribute("method", method._to_string());
    for (const auto &parameter : parameters) {
        sourceNode->append_attribute(parameter.first, parameter.second);
    }
}

void BODY25::KeyPointSource::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    auto sourceNode = node->first_node("KeyPoint");
    std::string keyPointExtractionMethod = sourceNode->attribute_value("method");
    try {
        method = Method::_from_string(keyPointExtractionMethod.c_str());
    }
    catch(std::runtime_error &e) {
        throw Exception::MMMFormatException("Unknown keypoint extraction method " + keyPointExtractionMethod);
    }
    for (const auto &parameter : sourceNode->get_all_attributes()) {
        if (simox::alg::to_lower(parameter.first) != "method")
            parameters[parameter.first] = parameter.second;
    }
}

void BODY25::DepthSource::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    auto sourceNode = node->append_node("Depth");
    sourceNode->append_attribute("method", method._to_string());
    if (radius > 0) sourceNode->append_attribute("radius", radius);
    if (sigma > 0) sourceNode->append_attribute("sigma", sigma);
    sourceNode->append_attribute("min", min);
    sourceNode->append_attribute("max", max);
}

void BODY25::DepthSource::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    auto sourceNode = node->first_node("Depth");
    std::string depthExtractionMethod = sourceNode->attribute_value("method");
    try {
        method = Method::_from_string(depthExtractionMethod.c_str());
    }
    catch(std::runtime_error &e) {
        throw Exception::MMMFormatException("Unknown depth extraction method " + depthExtractionMethod);
    }
    if (sourceNode->has_attribute("radius"))
        radius = sourceNode->attribute_value_<unsigned int>("radius");
    if (sourceNode->has_attribute("sigma"))
        sigma = sourceNode->attribute_value_<float>("sigma");
    min = sourceNode->attribute_value_<float>("min");
    max = sourceNode->attribute_value_<float>("max");
}

void BODY25::Source::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    auto sourceNode = node->append_node("Source");
    if (!sensorType.empty()) sourceNode->append_attribute("type", sensorType);
    if (!fileName.empty()) sourceNode->append_attribute("name", fileName);
    keyPointSource.appendConfigurationXML(sourceNode);
    if (depthSource.has_value()) depthSource.value().appendConfigurationXML(sourceNode);
}

void BODY25::Source::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node) {
    auto sourceNode = node->first_node("Source");
    sensorType = sourceNode->attribute_value_or_default("type", std::string());
    fileName = sourceNode->attribute_value_or_default("name", std::string());
    keyPointSource.loadConfigurationXML(sourceNode);
    if (sourceNode->has_node("Depth")) {
        depthSource = DepthSource();
        depthSource.value().loadConfigurationXML(sourceNode);
    }
    else depthSource = std::nullopt;
}

}

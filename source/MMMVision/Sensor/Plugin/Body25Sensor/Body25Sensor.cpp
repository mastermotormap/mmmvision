#include "Body25Sensor.h"


namespace MMM
{

Body25SensorPtr Body25Sensor::loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath) {
    Body25SensorPtr sensor(new Body25Sensor());
    sensor->loadSensor(node, filePath);
    return sensor;
}

Body25Sensor::Body25Sensor(const std::string &description) : BasicSensor(description)
{
}

bool Body25Sensor::checkModel(ModelPtr /*model*/) {
    return true;
}

std::shared_ptr<BasicSensor<Body25SensorMeasurement> > Body25Sensor::cloneConfiguration() {
    Body25SensorPtr m(new Body25Sensor(description));
    m->source = source;
    return m;
}

void Body25Sensor::loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &/*filePath*/) {
    assert(node);
    assert(node->name() == "Configuration");

    source.loadConfigurationXML(node);
}

void Body25Sensor::loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type)
{
    assert(node);
    assert(node->name() == xml::tag::MEASUREMENT);

    std::map<int, Body25KeyPoint> keyPoints;
    if (node->has_node("KeyPoint")) {
        for (auto keypointNode : node->nodes("KeyPoint")) {
            int id = keypointNode->attribute_value_<int>("id");
            if (id < 0 || id > 24 || keyPoints.find(id) != keyPoints.end())
                throw Exception::MMMFormatException("Wrong keypoint " + simox::alg::to_string(id) + " at timestep " + simox::alg::to_string(timestep) + " in Body25 sensor data");
            Body25KeyPoint keypoint;
            keypoint.camera_x = keypointNode->attribute_value_<float>("x");
            keypoint.camera_y = keypointNode->attribute_value_<float>("y");
            keypoint.score = keypointNode->attribute_value_<float>("score");
            if (!keypointNode->value().empty())
                keypoint.local_pose = keypointNode->value_eigen_vec3();
            keyPoints[id] = keypoint;
        }
    }
    else throw Exception::MMMFormatException("No keypoints at timestep " + simox::alg::to_string(timestep) + " in Body25 sensor data");
    Body25SensorMeasurementPtr m(new Body25SensorMeasurement(timestep, keyPoints, type));
    addSensorMeasurement(m);
}

void Body25Sensor::appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &/*filePath*/) {
    source.appendConfigurationXML(node);
}

bool Body25Sensor::hasSensorConfigurationContent() const {
    return true;
}

bool Body25Sensor::equalsConfiguration(SensorPtr other) {
    Body25SensorPtr ptr = std::dynamic_pointer_cast<Body25Sensor>(other);
    if (ptr) return true; // TODO!
    else return false;
}

std::string Body25Sensor::getType() {
    return TYPE;
}

std::string Body25Sensor::getVersion() {
    return VERSION;
}

int Body25Sensor::getPriority() {
    return 50;
}

}

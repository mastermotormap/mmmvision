/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#pragma once

#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/xml/rapidxml/RapidXMLForwardDecl.h>
#include <MMM/Enum.h>
#include <map>

namespace MMM
{

namespace BODY25
{

BETTER_ENUM(KeyPointExtractionMethod, short, Unknown = 0, OpenPose = 1)

struct KeyPointSource
{
    typedef KeyPointExtractionMethod Method;
    Method method = Method::Unknown;
    std::map<std::string, std::string> parameters;

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node);

    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node);
};

BETTER_ENUM(DepthExtractionMethod, short, Unknown = 0, Median = 1, Gaussian = 2)

struct DepthSource {
    typedef DepthExtractionMethod Method;
    Method method = Method::Unknown;
    unsigned int radius;
    float min;
    float max;
    float sigma; // Only for Gaussian method

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node);

    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node);
};

struct Source {
    std::string sensorType;
    std::string fileName;
    KeyPointSource keyPointSource;
    std::optional<DepthSource> depthSource;

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node);

    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node);
};

}

}

#include "Body25SensorFactory.h"

#include <boost/extension/extension.hpp>

#include "Body25Sensor.h"

namespace MMM
{

// register this factory
SensorFactory::SubClassRegistry Body25SensorFactory::registry(NAME_STR<Body25Sensor>(), &Body25SensorFactory::createInstance);

Body25SensorFactory::Body25SensorFactory() : SensorFactory() {}

Body25SensorFactory::~Body25SensorFactory() = default;

SensorPtr Body25SensorFactory::createSensor(simox::xml::RapidXMLWrapperNodePtr sensorNode, const std::filesystem::path &filePath)
{
    return Body25Sensor::loadSensorXML(sensorNode, filePath);
}

std::string Body25SensorFactory::getName()
{
    return NAME_STR<Body25Sensor>();
}

SensorFactoryPtr Body25SensorFactory::createInstance(void *)
{
    return SensorFactoryPtr(new Body25SensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorFactoryPtr getFactory() {
    return SensorFactoryPtr(new Body25SensorFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorFactory::VERSION;
}

}

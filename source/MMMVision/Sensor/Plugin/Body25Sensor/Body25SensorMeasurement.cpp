#include "Body25SensorMeasurement.h"

#include <SimoxUtility/xml/rapidxml/RapidXMLWrapper.h>

namespace MMM
{

Body25SensorMeasurement::Body25SensorMeasurement(float timestep, const std::map<int, Body25KeyPoint> &keyPoints, SensorMeasurementType type) :
    SensorMeasurement(timestep, type),
    keyPoints(keyPoints)
{
}

SensorMeasurementPtr Body25SensorMeasurement::clone() {
    return clone(timestep);
}

bool Body25SensorMeasurement::equals(SensorMeasurementPtr sensorMeasurement) {
    Body25SensorMeasurementPtr ptr = std::dynamic_pointer_cast<Body25SensorMeasurement>(sensorMeasurement);
    if (ptr) {
        return SensorMeasurement::compare<std::map<int, Body25KeyPoint> >(keyPoints, ptr->keyPoints);
    }
    return false;
}

Body25SensorMeasurementPtr Body25SensorMeasurement::clone(float newTimestep) {
    Body25SensorMeasurementPtr clonedSensorMeasurement(new Body25SensorMeasurement(newTimestep, keyPoints, type));
    return clonedSensorMeasurement;
}

std::map<int, Body25KeyPoint> Body25SensorMeasurement::getKeyPoints() {
    return keyPoints;
}

void Body25SensorMeasurement::appendMeasurementDataXML(simox::xml::RapidXMLWrapperNodePtr measurementNode) {
    for (const auto &keyPoint : keyPoints) {
        auto node = measurementNode->append_node("KeyPoint");
        node->append_attribute("id", keyPoint.first);
        node->append_attribute("x", keyPoint.second.camera_x);
        node->append_attribute("y", keyPoint.second.camera_y);
        node->append_attribute("score", keyPoint.second.score);
        if (keyPoint.second.local_pose) {
            node->append_data_node(keyPoint.second.local_pose.value());
        }

    }
}



}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#pragma once

#include <MMM/Motion/Sensor/BasicSensor.h>
#include "Body25SensorMeasurement.h"
#include "Body25Source.h"

namespace MMM
{

class Body25Sensor;
typedef std::shared_ptr<Body25Sensor> Body25SensorPtr;

class MMM_IMPORT_EXPORT Body25Sensor : public BasicSensor<Body25SensorMeasurement>
{

public:
    virtual ~Body25Sensor() {}

    static Body25SensorPtr loadSensorXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    Body25Sensor(const std::string &description = std::string());

    bool checkModel(ModelPtr model);

    std::shared_ptr<BasicSensor<Body25SensorMeasurement> > cloneConfiguration();

    bool equalsConfiguration(SensorPtr other);

    std::string getType();

    std::string getVersion();

    int getPriority();

    static constexpr const char* TYPE = "Body25";

    static constexpr const char* VERSION = "1.0";

    BODY25::Source source;

private:
    void loadConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    void loadMeasurementXML(simox::xml::RapidXMLWrapperNodePtr node, float timestep, SensorMeasurementType type);

    void appendConfigurationXML(simox::xml::RapidXMLWrapperNodePtr node, const std::filesystem::path &filePath);

    bool hasSensorConfigurationContent() const;
};
}

/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <MMM/Motion/Sensor/SensorMeasurement.h>
#include <opencv2/core/mat.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "RGBSensorMeasurement.h"

namespace MMM
{

class RGBDSensorMeasurement;
typedef std::shared_ptr<RGBDSensorMeasurement> RGBDSensorMeasurementPtr;

class RGBDSensorMeasurement : public RGBSensorMeasurement
{
public:
    virtual ~RGBDSensorMeasurement() = default;

    virtual bool getImages(cv::Mat &colorImage, cv::Mat &depthImage, bool transformToColor = true, bool normalize = false) = 0;

    virtual bool getImages(cv::Mat &colorImage, cv::Mat &depthImage, cv::Mat &irImage, bool transformToColor = true, bool normalize = false) = 0;

    virtual pcl::PointCloud<pcl::PointXYZRGBA>::Ptr getPointCloud(int maximumDistanceMM = -1) = 0;

protected:
    RGBDSensorMeasurement(float timestep, SensorMeasurementType type = SensorMeasurementType::MEASURED);
};

}

#include "RGBDSensor.h"

namespace MMM
{

std::pair<unsigned int, unsigned int> RGBDSensor::getResolution() {
    return getColorResolution();
}

RGBSensorMeasurementPtr RGBDSensor::getRGBSensorMeasurement(float timestep, float delta) {
    return getRGBDSensorMeasurement(timestep, delta);
}

RGBDSensor::RGBDSensor(const std::filesystem::path &filePath, const std::string &description) : RGBSensor(filePath, description)
{
}

RGBDSensor::RGBDSensor() : RGBSensor()
{
}

}

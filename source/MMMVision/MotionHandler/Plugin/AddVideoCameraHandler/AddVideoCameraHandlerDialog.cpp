#include "AddVideoCameraHandlerDialog.h"
#include "ui_AddVideoCameraHandlerDialog.h"

#include <MMM/Exceptions.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMMVision/Sensor/Plugin/VideoCameraSensor/VideoCameraSensor.h>
#include <QFileDialog>
#include <QCheckBox>
#include <QMessageBox>
#include <set>

AddVideoCameraHandlerDialog::AddVideoCameraHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions) :
    QDialog(parent),
    ui(new Ui::AddVideoCameraHandlerDialog),
    motions(motions),
    currentMotion(nullptr),
    motionConverted(false)
{
    ui->setupUi(this);

    loadData(settings.value("addvideocamera/datafilepath", "").toString().toStdString());

    connect(ui->ChooseMotionComboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(setCurrentMotion(const QString&)));
    connect(ui->DataButton, SIGNAL(clicked()), this, SLOT(loadData()));
    connect(ui->AddButton, SIGNAL(clicked()), this, SLOT(add()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));

    loadMotions();
    enableConvertButton();
}

AddVideoCameraHandlerDialog::~AddVideoCameraHandlerDialog() {
    delete ui;
}

bool AddVideoCameraHandlerDialog::convertMotion() {
    exec();
    return motionConverted;
}

void AddVideoCameraHandlerDialog::loadMotions() {
    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        if (!currentMotion) currentMotion = motion;
        ui->ChooseMotionComboBox->addItem(QString::fromStdString(motion->getName()));
    };
}

void AddVideoCameraHandlerDialog::setCurrentMotion(const QString &name) {
    currentMotion = motions->getMotion(name.toStdString());
    if (!currentMotion->getModel()) {
        MMM_WARNING << "Current motion does not contain a model. Some functions are therefore not available for this sensor until a model is added." << std::endl;
    }
}

void AddVideoCameraHandlerDialog::loadData() {
    std::string dataFilePath = QFileDialog::getOpenFileName(this, tr("Load video recording"), settings.value("addvideocamera/datafilepath", "").toString(), tr("Video recordings (*.mp4 *.MP4)")).toStdString();
    loadData(dataFilePath);
}

void AddVideoCameraHandlerDialog::loadData(const std::filesystem::path &dataFilePath) {
    if (!dataFilePath.empty()) {
        this->dataFilePath = dataFilePath;
        setDataLabel(dataFilePath);
        settings.setValue("datagloveconverter/datafilepath", QString::fromStdString(dataFilePath));
        enableConvertButton();
    }
}

void AddVideoCameraHandlerDialog::setDataLabel(const std::filesystem::path &filePath) {
    QString label = QString::fromStdString(filePath.filename());
    ui->DataLabel->setText(label);
    ui->DataLabel->setToolTip(label);
}

void AddVideoCameraHandlerDialog::enableConvertButton() {
    if (currentMotion && !dataFilePath.empty()) ui->AddButton->setEnabled(true);
}

void AddVideoCameraHandlerDialog::add() {
    try {
        std::cout << "Adding video camera recording from " << dataFilePath << std::endl;
        MMM::VideoCameraSensorPtr sensor = std::make_shared<MMM::VideoCameraSensor>(dataFilePath);
        currentMotion->addSensor(sensor, ui->TimestepDeltaSpinBox->value(), currentMotion->getModel() != nullptr);
        motionConverted = true;
        this->close();
    }
    catch (MMM::Exception::MMMException &e) {
        QMessageBox* msgBox = new QMessageBox(this);
        std::string error_message = std::string("Could not add video camera sensor! ") + e.what();
        msgBox->setText(QString::fromStdString(error_message));
        MMM_ERROR << error_message << std::endl;
        msgBox->exec();
    }
}

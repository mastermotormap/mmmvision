#include "AddVideoCameraHandler.h"
#include "AddVideoCameraHandlerDialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

AddVideoCameraHandler::AddVideoCameraHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::ADD_SENSOR, "Video Recording"),
    searchPath(""),
    widget(widget)
{
}

void AddVideoCameraHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (motions && !motions->empty()) {
        AddVideoCameraHandlerDialog* dialog = new AddVideoCameraHandlerDialog(widget, motions);
        if (dialog->convertMotion()) emit openMotions(motions);
    }
    else MMM_ERROR << "Cannot open add video camera dialog, because no motions are present!" << std::endl;
}

std::string AddVideoCameraHandler::getName() {
    return NAME;
}

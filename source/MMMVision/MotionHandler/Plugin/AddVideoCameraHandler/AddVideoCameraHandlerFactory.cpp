#include "AddVideoCameraHandlerFactory.h"
#include "AddVideoCameraHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddVideoCameraHandlerFactory::registry(AddVideoCameraHandler::NAME, &AddVideoCameraHandlerFactory::createInstance);

AddVideoCameraHandlerFactory::AddVideoCameraHandlerFactory() : MotionHandlerFactory() {}

AddVideoCameraHandlerFactory::~AddVideoCameraHandlerFactory() = default;

std::string AddVideoCameraHandlerFactory::getName() {
    return AddVideoCameraHandler::NAME;
}

MotionHandlerPtr AddVideoCameraHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new AddVideoCameraHandler(widget));
}

MotionHandlerFactoryPtr AddVideoCameraHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddVideoCameraHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddVideoCameraHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}

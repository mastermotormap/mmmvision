/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <QDialog>
#include <QSettings>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#endif

namespace Ui {
class AddAzureKinectDKHandlerDialog;
}

class AddAzureKinectDKHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddAzureKinectDKHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions);
    ~AddAzureKinectDKHandlerDialog();

    bool convertMotion();

private slots:
    void setCurrentMotion(const QString &name);
    void loadData();
    void add();

private:
    void loadMotions();
    void loadData(const std::filesystem::path &dtaFilePath);
    void setDataLabel(const std::filesystem::path &filePath);
    void enableConvertButton();

    Ui::AddAzureKinectDKHandlerDialog* ui;
    MMM::MotionRecordingPtr motions;
    MMM::MotionPtr currentMotion;
    std::filesystem::path dataFilePath;
    bool motionConverted;
    QSettings settings;
};

#include "AddAzureKinectDKHandlerFactory.h"
#include "AddAzureKinectDKHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddAzureKinectDKHandlerFactory::registry(AddAzureKinectDKHandler::NAME, &AddAzureKinectDKHandlerFactory::createInstance);

AddAzureKinectDKHandlerFactory::AddAzureKinectDKHandlerFactory() : MotionHandlerFactory() {}

AddAzureKinectDKHandlerFactory::~AddAzureKinectDKHandlerFactory() = default;

std::string AddAzureKinectDKHandlerFactory::getName() {
    return AddAzureKinectDKHandler::NAME;
}

MotionHandlerPtr AddAzureKinectDKHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new AddAzureKinectDKHandler(widget));
}

MotionHandlerFactoryPtr AddAzureKinectDKHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddAzureKinectDKHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddAzureKinectDKHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}

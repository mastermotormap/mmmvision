#include "AddAzureKinectDKHandler.h"
#include "AddAzureKinectDKHandlerDialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

AddAzureKinectDKHandler::AddAzureKinectDKHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::ADD_SENSOR, "Azure Kinect DK recording"),
    searchPath(""),
    widget(widget)
{
}

void AddAzureKinectDKHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (motions && !motions->empty()) {
        AddAzureKinectDKHandlerDialog* dialog = new AddAzureKinectDKHandlerDialog(widget, motions);
        if (dialog->convertMotion()) emit openMotions(motions);
    }
    else MMM_ERROR << "Cannot open add azure kinect dk dialog, because no motions are present!" << std::endl;
}

std::string AddAzureKinectDKHandler::getName() {
    return NAME;
}

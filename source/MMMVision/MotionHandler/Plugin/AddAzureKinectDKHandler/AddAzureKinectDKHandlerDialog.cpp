#include "AddAzureKinectDKHandlerDialog.h"
#include "ui_AddAzureKinectDKHandlerDialog.h"

#include <MMM/Exceptions.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMMVision/Sensor/Plugin/AzureKinectDKSensor/AzureKinectDKSensor.h>
#include <VirtualRobot/Robot.h>
#include <QFileDialog>
#include <QCheckBox>
#include <QMessageBox>
#include <set>

AddAzureKinectDKHandlerDialog::AddAzureKinectDKHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions) :
    QDialog(parent),
    ui(new Ui::AddAzureKinectDKHandlerDialog),
    motions(motions),
    currentMotion(nullptr),
    motionConverted(false)
{
    ui->setupUi(this);

    loadData(settings.value("addazurekinectdk/datafilepath", "").toString().toStdString());

    connect(ui->ChooseMotionComboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(setCurrentMotion(const QString&)));
    connect(ui->DataButton, SIGNAL(clicked()), this, SLOT(loadData()));
    connect(ui->AddButton, SIGNAL(clicked()), this, SLOT(add()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));

    loadMotions();
    enableConvertButton();
}

AddAzureKinectDKHandlerDialog::~AddAzureKinectDKHandlerDialog() {
    delete ui;
}

bool AddAzureKinectDKHandlerDialog::convertMotion() {
    exec();
    return motionConverted;
}

void AddAzureKinectDKHandlerDialog::loadMotions() {
    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        if (!currentMotion) currentMotion = motion;
        ui->ChooseMotionComboBox->addItem(QString::fromStdString(motion->getName()));
    };
}

void AddAzureKinectDKHandlerDialog::setCurrentMotion(const QString &name) {
    currentMotion = motions->getMotion(name.toStdString());
    if (!currentMotion->getModel()) {
        MMM_WARNING << "Current motion does not contain a model. Some functions are therefore not available for this sensor until a model is added." << std::endl;
    }
}

void AddAzureKinectDKHandlerDialog::loadData() {
    std::string dataFilePath = QFileDialog::getOpenFileName(this, tr("Load Azure Kinect DK recording"), settings.value("addazurekinectdk/datafilepath", "").toString(), tr("Azure Kinect DK recordings (*.mkv)")).toStdString();
    loadData(dataFilePath);
}

void AddAzureKinectDKHandlerDialog::loadData(const std::filesystem::path &dataFilePath) {
    if (!dataFilePath.empty()) {
        this->dataFilePath = dataFilePath;
        setDataLabel(dataFilePath);
        settings.setValue("addazurekinectdk/datafilepath", QString::fromStdString(dataFilePath));
        enableConvertButton();
    }
}

void AddAzureKinectDKHandlerDialog::setDataLabel(const std::filesystem::path &filePath) {
    QString label = QString::fromStdString(filePath.filename());
    ui->DataLabel->setText(label);
    ui->DataLabel->setToolTip(label);
}

void AddAzureKinectDKHandlerDialog::enableConvertButton() {
    if (currentMotion && !dataFilePath.empty()) ui->AddButton->setEnabled(true);
}

void AddAzureKinectDKHandlerDialog::add() {
    try {
        std::cout << "Adding azure kinect dk recording from " << dataFilePath << std::endl;
        MMM::AzureKinectDKSensorPtr sensor = std::make_shared<MMM::AzureKinectDKSensor>(dataFilePath);
        auto model = currentMotion->getModel();
        if (model) {
            auto rgbSensor = model->getSensor("rgb");
            if (rgbSensor)
                sensor->setTransformation(rgbSensor->getParentNodeToSensorTransformation());
        }
        currentMotion->addSensor(sensor, ui->TimestepDeltaSpinBox->value(), currentMotion->getModel() != nullptr);
        motionConverted = true;
        this->close();
    }
    catch (MMM::Exception::MMMException &e) {
        QMessageBox* msgBox = new QMessageBox(this);
        std::string error_message = std::string("Could not add AzureKinect sensor! ") + e.what();
        msgBox->setText(QString::fromStdString(error_message));
        MMM_ERROR << error_message << std::endl;
        msgBox->exec();
    }
}

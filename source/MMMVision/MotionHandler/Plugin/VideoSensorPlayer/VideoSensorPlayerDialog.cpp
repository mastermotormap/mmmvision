#include "VideoSensorPlayerDialog.h"
#include "ui_VideoSensorPlayerDialog.h"

#include <MMM/Exceptions.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMMVision/Sensor/Plugin/VideoCameraSensor/VideoCameraSensor.h>
#include <MMMVision/SensorVisualisation/Body25Visualisation.h>
#include <MMMVision/SensorVisualisation/QtImage.h>
#include <QFileDialog>
#include <QCheckBox>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <set>

VideoSensorPlayerDialog::VideoSensorPlayerDialog(QWidget* parent, MMM::MotionRecordingPtr motions) :
    QDialog(parent),
    ui(new Ui::VideoSensorPlayerDialog),
    motions(motions)
{
    ui->setupUi(this);

    loadRGBSensors(motions);
}

VideoSensorPlayerDialog::~VideoSensorPlayerDialog() {
    delete ui;
}

void VideoSensorPlayerDialog::loadRGBSensors(MMM::MotionRecordingPtr motions) {
    rgbSensors.clear();
    for (auto motion : *motions) {
        MMM::Body25SensorPtr body25Sensor = motion->getSensorByType<MMM::Body25Sensor>();
        for (auto sensor : motion->getSensorData()) {
            MMM::RGBSensorPtr rgbSensor = std::dynamic_pointer_cast<MMM::RGBSensor>(sensor.second); // TODO not pretty! - Add function to retrieve all sensor types of parents
            if (rgbSensor) {
                std::cout << motion->getName() << std::endl;
                rgbSensors.push_back(std::make_pair(rgbSensor, body25Sensor ? body25Sensor : nullptr));
            }
        }
    }
}

void VideoSensorPlayerDialog::clearLayout() {
    QLayoutItem* child;
    while (ui->VideoSensorPlayerLayout->count() != 0) {
        child = ui->VideoSensorPlayerLayout->takeAt(0);
        if ( child->widget() != 0 ) {
            delete child->widget();
        }
        delete child;
    }
}

void VideoSensorPlayerDialog::jumpTo(float timestep) {
    clearLayout();
    for (auto rgbSensor : rgbSensors) {
        auto measurement = rgbSensor.first->getRGBSensorMeasurement(timestep, 0.1);
        if (!measurement) continue;
        cv::Mat cvtImage = measurement->getImage();
        QImage image = MMM::QtImage::Mat2QImage(cvtImage);
        float timestep = measurement->getTimestep();

        float scaleFactor = 0.25f;

        QPainter painter(&image);

        if (rgbSensor.second) {
            auto body25measurement = rgbSensor.second->getDerivedMeasurement(timestep, 0.1);
            if (body25measurement) {
                auto keyPoints = body25measurement->getKeyPoints();
                for (int i = 0; i <= 24; i++)
                {
                    if (keyPoints.find(i) == keyPoints.end()) continue;

                    MMM::Body25KeyPoint keyPoint = keyPoints.at(i);

                    std::tuple<float, float, float> color = MMM::BODY25::openposeKeyPointColor[i];

                    QPen pen;
                    pen.setWidth(10);
                    pen.setColor(QColor::fromRgbF(std::get<0>(color), std::get<1>(color), std::get<2>(color), keyPoint.score));
                    painter.setPen(pen);
                    painter.drawPoint(keyPoint.camera_x, keyPoint.camera_y);

                    // Draw line if possible
                    if (keyPoints.find(MMM::BODY25::endPointIndex[i]) == keyPoints.end()) continue;

                    MMM::Body25KeyPoint keyPoint2 = keyPoints.at(MMM::BODY25::endPointIndex[i]);

                    painter.drawLine(keyPoint.camera_x, keyPoint.camera_y, keyPoint2.camera_x, keyPoint2.camera_y);
                }
            }
        }

        painter.setPen(QPen(Qt::white));
        QFont font = painter.font();
        font.setPixelSize(16 / scaleFactor);
        painter.setFont(font);
        painter.drawText(image.rect(), Qt::AlignCenter | Qt::AlignBottom, QString::fromStdString(std::to_string(timestep)));
        painter.end();

        QLabel *lbl = new QLabel(this);
        QPixmap px = QPixmap::fromImage(image);
        lbl->setPixmap(px.scaledToHeight(image.height() * scaleFactor));
        ui->VideoSensorPlayerLayout->addWidget(lbl);

        cvtImage.release();
    }
}

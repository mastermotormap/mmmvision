#include "VideoSensorPlayerFactory.h"
#include "VideoSensorPlayer.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry VideoSensorPlayerFactory::registry(VideoSensorPlayer::NAME, &VideoSensorPlayerFactory::createInstance);

VideoSensorPlayerFactory::VideoSensorPlayerFactory() : MotionHandlerFactory() {}

VideoSensorPlayerFactory::~VideoSensorPlayerFactory() = default;

std::string VideoSensorPlayerFactory::getName() {
    return VideoSensorPlayer::NAME;
}

MotionHandlerPtr VideoSensorPlayerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new VideoSensorPlayer(widget));
}

MotionHandlerFactoryPtr VideoSensorPlayerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new VideoSensorPlayerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new VideoSensorPlayerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}

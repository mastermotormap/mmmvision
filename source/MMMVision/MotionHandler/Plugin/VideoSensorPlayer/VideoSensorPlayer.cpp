#include "VideoSensorPlayer.h"
#include "VideoSensorPlayerDialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

VideoSensorPlayer::VideoSensorPlayer(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Video Player"),
    widget(widget),
    dialog(nullptr)
{
}

void VideoSensorPlayer::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (motions && !motions->empty()) {
        dialog = new VideoSensorPlayerDialog(widget, motions);
        dialog->show();
    }
    else MMM_ERROR << "Cannot open add video camera dialog, because no motions are present!" << std::endl;
}

std::string VideoSensorPlayer::getName() {
    return NAME;
}

void VideoSensorPlayer::timestepChanged(float timestep) {
    if (dialog && dialog->isVisible())
        dialog->jumpTo(timestep);
}

void VideoSensorPlayer::motionsOpened(bool opened, MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (opened && dialog && dialog->isVisible())
        dialog->loadRGBSensors(motions);
}

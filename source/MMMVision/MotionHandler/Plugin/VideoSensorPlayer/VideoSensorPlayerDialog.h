/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <QDialog>
#include <QSettings>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include <MMMVision/Sensor/RGBSensor.h>
#include <MMMVision/Sensor/Plugin/Body25Sensor/Body25Sensor.h>
#endif

namespace Ui {
class VideoSensorPlayerDialog;
}

class VideoSensorPlayerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VideoSensorPlayerDialog(QWidget* parent, MMM::MotionRecordingPtr motions);
    ~VideoSensorPlayerDialog();

    void loadRGBSensors(MMM::MotionRecordingPtr motions);

    void clearLayout();

    void jumpTo(float timestep);

private:
    Ui::VideoSensorPlayerDialog* ui;
    MMM::MotionRecordingPtr motions;
    std::vector<std::pair<MMM::RGBSensorPtr, MMM::Body25SensorPtr>> rgbSensors;
};

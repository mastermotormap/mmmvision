#!/bin/bash

AZURE_KINECT_SCRIPT_NAME="azure_kinect"
UBUNTU_VERSION=$(lsb_release -rs)
AZURE_KINECT_DE_DOWNLOAD="https://packages.microsoft.com/ubuntu/${UBUNTU_VERSION}/prod/pool/main/libk/libk4a1.4/libk4a1.4_1.4.1_amd64.deb"

azure_kinect_install()
{
  AZURE_KINECT_DIR="$DIR/azure_kinect"
  export AZURE_KINECT_DIR

  azure_kinect_setup_repo
  azure_kinect_install_package
  azure_kinect_build
  azure_kinect_create
}

azure_kinect_setup()
{
  AZURE_KINECT_DIR="$DIR/azure_kinect"
  export AZURE_KINECT_DIR

  azure_kinect_setup_repo
  azure_kinect_build
  azure_kinect_create
}

azure_kinect_create()
{
  if [[ -d $AZURE_KINECT_DIR ]]; then
  echo "INFO: Created azure_kinect.sh file at $(pwd)/azure_kinect.sh"
  echo 'function azure_kinect_activate
{
  if [[ -d '$AZURE_KINECT_DIR' ]]; then
    export AZURE_KINECT_ROOT="'$AZURE_KINECT_DIR'/sensor_sdk"

    export LD_LIBRARY_PATH="'$AZURE_KINECT_DIR'/dependencies/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH"
    export PATH="'$AZURE_KINECT_DIR'/sensor_sdk/build/bin:$PATH"

    export k4a_DIR="'$AZURE_KINECT_DIR'/install/lib/cmake/k4a"
    export k4arecord_DIR="'$AZURE_KINECT_DIR'/install/lib/cmake/k4arecord"
  else  
  echo "'$AZURE_KINECT_ROOT' no directory"
  fi
}

export -f azure_kinect_activate

function azure_kinect_deactivate
{
  unset AZURE_KINECT_ROOT
  unset k4a_DIR
  unset k4arecord_DIR
}

export -f azure_kinect_deactivate

function azure_kinect_cd_dir
{
    cd '$AZURE_KINECT_DIR' || true
}

export -f azure_kinect_cd_dir' > "$(pwd)/$AZURE_KINECT_SCRIPT_NAME.sh"
  else
       echo "$AZURE_KINECT_DIR no valid directory."
  fi
}




azure_kinect_install_package()
{
  aptitude update
  chmod a+x ${AZURE_KINECT_DIR}/sensor_sdk/scripts/bootstrap-ubuntu.sh
  bash ${AZURE_KINECT_DIR}/sensor_sdk/scripts/bootstrap-ubuntu.sh
  sudo cp ${AZURE_KINECT_DIR}/sensor_sdk/scripts/99-k4a.rules /etc/udev/rules.d/ #add udev rules for access to azure kinect without root
  udevadm control --reload-rules && udevadm trigger #update udev rules
}


azure_kinect_setup_repo()
{
  local prev_pwd=$(pwd)

  if [[ ! -d $AZURE_KINECT_DIR ]]; then
    mkdir $AZURE_KINECT_DIR
  fi

  cd $AZURE_KINECT_DIR

  if [[ ! -d "sensor_sdk" ]]; then
    git clone https://github.com/microsoft/Azure-Kinect-Sensor-SDK.git sensor_sdk
  fi

  cd sensor_sdk

  git checkout v1.4.1
  git submodule init
  git submodule update --init --recursive

  azure_kinect_setup_depth_engine

  cd "$prev_pwd"
}


azure_kinect_setup_depth_engine()
{
  local prev_pwd=$(pwd)

  cd $AZURE_KINECT_DIR

  mkdir -p "$AZURE_KINECT_DIR/dependencies" &> /dev/null || true

  cd dependencies

  if [[ ! -d usr ]]; then
    wget "$AZURE_KINECT_DE_DOWNLOAD"
    dpkg -x libk4a1.4_1.4.1_amd64.deb .
    rm libk4a1.4_1.4.1_amd64.deb
  fi

  cd "$prev_pwd"
}


azure_kinect_build()
{
  local prev_pwd=$(pwd)

  cd "$AZURE_KINECT_DIR/sensor_sdk"
  if [[ ! -d build ]]; then
    mkdir build
  fi
  cd build
  cmake -G"Ninja" -DCMAKE_C_COMPILER=$(which gcc-6) -DCMAKE_CXX_COMPILER=$(which g++-6) -D"CMAKE_BUILD_TYPE=RelWithDebInfo" -D"CMAKE_INSTALL_PREFIX=$AZURE_KINECT_DIR/install" -D"K4A_VALIDATE_CLANG_FORMAT=OFF" ..
  cmake --build . --target all
  cmake --build . --target install

  cd "$prev_pwd"
}

usage()
{
    echo "Install (sudo required) and Setup      sudo $0 -i <directory>      e.g.    sudo $0 -i ~/repos" 
    echo "Setup                                  $0 -s <directory>           e.g.    $0 -s ~/repos" 
    echo "Source and Activate                    source $0 -a"
    if [[ -d "$ArmarX_DIR/VisionX" ]]; then
    echo "Rebuild VisionX (when activated)       $0 -v"
    fi
}

rebuild_visionx()
{
    if [ "$k4a_DIR" != "" ]; then
      if [[ -d "$ArmarX_DIR/VisionX" ]]; then
        local prev_pwd=$(pwd)
        cd "$ArmarX_DIR/VisionX"
        cd build
        echo "INFO: Executing   cmake .. | grep 'AzureKinect|k4a'"
        cmake .. | grep 'AzureKinect|k4a'
        #echo "INFO: Executing   cmake --build . --target all | grep 'AzureKinect|k4a'"
        #cmake --build . --target all | grep 'AzureKinect|k4a'

        cd "$prev_pwd"
     else
        echo "ERROR: $ArmarX_DIR/VisionX no directory"
     fi
   else
     echo "ERROR: k4a_DIR not set"
   fi
}


azure_kinect_install_from_package()
{
  # UNTESTED!
  curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
  sudo apt-add-repository https://packages.microsoft.com/ubuntu/${UBUNTU_VERSION}/prod
  sudo apt-get update

  sudo apt install k4a-tools
  sudo apt install libk4a1.1-dev
}


if [ $1 != "" ]; then
    case $1 in
        -h | --help)
            usage
            exit 1
            ;;
        -p | --package)
            if [[ $EUID -ne 0 ]]; then
               echo "ERROR: Installing packages has to be run as root" 
            else
               azure_kinect_install_packages
            fi
            ;;
        -i | --install)
            if [ "$2" != "" ]; then
               if [[ $EUID -ne 0 ]]; then
                  echo "ERROR: Installing packages has to be run as root" 
               else
                  DIR=$2
                  azure_kinect_install
            fi
            else
                echo "ERROR: Option install requires directory e.g.    $0 -i ~/repos"
            fi
            ;;
        -s | --setup)
            if [ "$2" != "" ]; then
              DIR=$2
              azure_kinect_setup
            else
                echo "ERROR: Option setup requires directory e.g.    $0 -i ~/repos"
            fi
            ;;
        -a | --activate)
            if [[ -f "$(pwd)/$AZURE_KINECT_SCRIPT_NAME.sh" ]]; then
                echo "INFO: Sourcing $(pwd)/$AZURE_KINECT_SCRIPT_NAME.sh"
                source "$(pwd)/$AZURE_KINECT_SCRIPT_NAME.sh"
                azure_kinect_activate
            else
                echo "ERROR: Cannot source. Run install first"
            fi
            ;;
        -v | --visionx)
            rebuild_visionx
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
else
  echo "ERROR: No parameter"
  usage
  exit 1
fi
